#include <Llamathrust.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <imgui.h>
#include <glad/glad.h>
#include <thread>
#include <iostream>

// SIZES
#define BACKGROUND_SIZE 5
#define GRID_SPACING 1 // unit_size ==> in meters
#define GRID_SIZE 100 // in units
// COLORS
#define RED glm::vec4(1, 0, 0, 1)
#define GREEN glm::vec4(0, 1, 0, 1)
#define BLUE glm::vec4(0, 0, 1, 1)
#define PINK glm::vec4(1, 0, 1, 1)
#define BLACK glm::vec4(0, 0, 0, 1)
#define TRANSPARENT glm::vec4(0, 0, 0, 0)
#define GRID_COLOR glm::vec4(0.5f, 0.5f, 0.5f, 1.0f)
#define BACKGROUND_COLOR glm::vec4(0.4f, 0.4f, 0.4f, 1.0f)

// Render floor void mist
class BackgroundLayer : public Llamathrust::Layer
{
	Llamathrust::Rendering::RenderDevice* renderDevice = nullptr;

	// Nodes
	Llamathrust::Rendering::Skybox* skybox;
	Llamathrust::Rendering::Camera* camera;
	Llamathrust::Rendering::SceneNode* viewer_node;
public:
	BackgroundLayer()
	{
		using namespace Llamathrust;

		renderDevice = Rendering::RenderDevice::Get();

		camera = m_app.GetCamera();

		viewer_node = m_app.GetViewNode();

		std::vector<std::string> texture_faces =
		{
			// RIGHT
			"../res/textures/CubeMaps/ame_desert/desertsky_rt.tga",
			// LEFT
			"../res/textures/CubeMaps/ame_desert/desertsky_lf.tga",
			// UP
			"../res/textures/CubeMaps/ame_desert/desertsky_up.tga",
			// DOWN
			"../res/textures/CubeMaps/ame_desert/desertsky_dn.tga",
			// BACK
			"../res/textures/CubeMaps/ame_desert/desertsky_bk.tga",
			// FRONT
			"../res/textures/CubeMaps/ame_desert/desertsky_ft.tga",
		};

		Rendering::Texture_Desc cubeMapDesc;
		cubeMapDesc.Format = Rendering::IMAGE_FORMAT::RGB_24;
		cubeMapDesc.MipLevels = false;

		auto cubemap = renderDevice->CreateCubemap(texture_faces, cubeMapDesc);
		cubemap->LoadGPU();

		skybox = new Rendering::Skybox(cubemap);

		m_app.SetSkybox(skybox);
	}

	void OnRender() override
	{
		using namespace Llamathrust::Graphics;

		//m_app.GetVideoDriver()->enableDepthTesting(DEPTH_TESTING::DISABLE);

		//skybox->Draw(camera->GetProjectionMatrix(), viewer_node->GetViewProps()->GetViewMatrix());
	}
};

class GridLayer : public Llamathrust::Layer
{
	Llamathrust::Rendering::RenderDevice* renderDevice = nullptr;
	Llamathrust::Rendering::Scene scene;


public:
	GridLayer() : Llamathrust::Layer("Grid Layer")
	{
		using namespace Llamathrust;

		auto camera = m_app.GetCamera();
		scene.AddCameraSetActive(camera);

		auto viewer_node = m_app.GetViewNode();
		scene.ReplaceViewNodeSetActive(viewer_node, 0);

		renderDevice = Rendering::RenderDevice::Get();

		Rendering::Model* grid;
		// Grid
		{
			std::vector<Rendering::Vertex> vertices;
			std::vector<uint_32> indices;

			Rendering::Vertex vertex;
			vertex.normal = glm::vec3(0, 1, 0);
			vertex.uvcoord = glm::vec2(0, 0);

			// count of lines per axis
			const int lines_count = GRID_SIZE / GRID_SPACING;

			// X LINES
			for (uint_32 i = 0; i <= lines_count; i++)
			{
				// Skip middle line where the axis is to later draw the axis in this position
				if (i == lines_count / 2)
					continue;
				// End point of the line on x_axis
				float x_pos = GRID_SIZE / 2.0f;
				// Position of the line on z_axis (moves every grid_spacing)
				// Begins at the end point of z_axis lines
				float z_pos = GRID_SIZE / 2.0f - i * GRID_SPACING;

				// Create vertices of the line
				// Vert 1
				vertex.position = glm::vec3(x_pos, 0, z_pos);
				vertices.emplace_back(vertex);
				// Vert 2
				vertex.position = glm::vec3(-x_pos, 0, z_pos);
				vertices.emplace_back(vertex);
			}

			// Z LINES
			for (uint_32 i = 0; i <= lines_count; i++)
			{
				// Skip middle line where the axis is to later draw the axis in this position
				if (i == lines_count / 2)
					continue;
				// Create vertices of the line
				// Vert 1
				vertex.position = glm::vec3(GRID_SIZE / 2.0f - i * GRID_SPACING, 0, GRID_SIZE / 2.0f);
				vertices.emplace_back(vertex);
				// Vert 2
				vertex.position = glm::vec3(GRID_SIZE / 2.0f - i * GRID_SPACING, 0, -GRID_SIZE / 2.0f);
				vertices.emplace_back(vertex);
			}

			// Indices
			// Lines count by 4 since each line has 2 indices(vertices) and there are 2 lines group(X & Z)
			uint_32 indices_count = (lines_count) * 4;
			for (uint_32 i = 0; i < indices_count; i++)
			{
				indices.emplace_back(i);
			}

			grid = new Rendering::Model(vertices, indices, Rendering::DRAW_MODE::LINES);
			scene.AddModel(grid, glm::mat4(1.0f), nullptr, "Grid");
		}

		Rendering::Model* axesModel;
		// Axis Lines
		{
			Rendering::Vertex vertex;
			vertex.normal = glm::vec3(0, 1, 0);
			vertex.uvcoord = glm::vec2(0, 0);
			std::vector<Rendering::Vertex> vertices;

			vertex.position = glm::vec3(GRID_SIZE / 2.0f, 0, 0);
			vertices.emplace_back(vertex);

			vertex.position = glm::vec3(-GRID_SIZE / 2.0f, 0, 0);
			vertices.emplace_back(vertex);


			std::vector<uint_32> indices = { 0, 1 };

			Rendering::Mesh* x_axisMesh = renderDevice->CreateMesh(vertices, indices, nullptr, Rendering::DRAW_MODE::LINES);

			for (auto& vert : vertices)
				vert.position = glm::mat3(glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0, 1, 0))) * vert.position;

			Rendering::Mesh* z_axisModel = renderDevice->CreateMesh(vertices, indices, nullptr, Rendering::DRAW_MODE::LINES);

			axesModel = new Rendering::Model({ x_axisMesh, z_axisModel });
			scene.AddModel(axesModel, glm::mat4(1.0f), nullptr, "AxesModel");
		}

		//Materials
		auto gridMaterial = scene.CreateMaterialAndBind(Rendering::MATERIAL_TYPE::UNLIT, grid);
		gridMaterial->SetDiffuse(Graphics::Color(GRID_COLOR));

		auto redMaterial = scene.CreateMaterial(Rendering::MATERIAL_TYPE::UNLIT);
		redMaterial->SetDiffuse(Graphics::Color(RED));

		auto blueMaterial = scene.CreateMaterial(Rendering::MATERIAL_TYPE::UNLIT);
		blueMaterial->SetDiffuse(Graphics::Color(BLUE));

		scene.BindMaterialsToModel(axesModel, { redMaterial, blueMaterial });

		scene.LoadGPU();
	}

	void OnRender() override
	{
		using namespace Llamathrust::Graphics;

		m_app.GetVideoDriver()->enableDepthTesting(DEPTH_TESTING::ENABLE);
		m_app.GetVideoDriver()->enableCullFace(CULL_FACES::BACK_FACE);

		scene.Draw();
	}
};

// This layer is used as the main scene
class PrimaryLayer : public Llamathrust::Layer
{
	Llamathrust::Rendering::RenderDevice* renderDevice = nullptr;

	// View
	Llamathrust::Rendering::Camera* camera;
	Llamathrust::Rendering::ViewProperties* propView;
	// Nodes
	Llamathrust::Rendering::Scene scene;
	Llamathrust::Rendering::SceneNode* viewer_node = nullptr;
	Llamathrust::Rendering::SceneNode* cube_node = nullptr;
	// Models
	Llamathrust::Rendering::Model* model = nullptr;
	bool loaded = false;
	bool gpuInit = false;
	// Logic variables
	// model
	float rotation_val = 0;
	glm::vec3 pick_position;
	bool castRay = false;

	// camera
	const float mouse_sensitivity = 0.2f;
	const float zoomScale = 0.5f;
	float mouseXold = 0;
	float mouseYold = 0;
	float distance = 3.5f;
	bool init = true;
	bool cam_can_move = false;

	// GRID
	glm::mat4 m_Ztransform = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));

public:
	PrimaryLayer() : Llamathrust::Layer("Primary Layer")
	{
		using namespace Llamathrust;

		camera = m_app.GetCamera();
		scene.AddCameraSetActive(camera);

		viewer_node = m_app.GetViewNode();
		glm::vec3& position = *(new glm::vec3(distance, 0, 0));
		Rendering::ViewProperties* viewProp = new Rendering::ViewProperties(position, -20.5f, 318, *(new glm::vec3(0, 1, 0)));
		// Set new ViewProps
		viewer_node->SetAsViewNode(viewProp);
		propView = viewer_node->GetViewProps();
		// Set scene view node
		scene.ReplaceViewNodeSetActive(viewer_node, 0);

		renderDevice = Rendering::RenderDevice::Get();

		std::thread loader(&PrimaryLayer::LoadScene, this);
		loader.detach();
	}

	void LoadScene()
	{
		using namespace Llamathrust;

		LT_TRACE("LOADING...");

		model = m_app.GetAssetManager()->LoadModel("../res/models/wheel.mudm", "wheel");
		if (model)
			cube_node = scene.AddModel(model, glm::mat4(1.0f), nullptr, "CubeNode");

		// Textures
		Rendering::Texture_Desc texture_description;
		texture_description.Format = Rendering::IMAGE_FORMAT::RGB_24;
		texture_description.MipLevels = false;
		Rendering::Texture* texture = renderDevice->CreateTexture2D("../res/textures/container2.png", texture_description);


		if (model)
		{
			auto litMaterialPink = scene.CreateMaterial(Rendering::MATERIAL_TYPE::LIT);
			litMaterialPink->SetDiffuse(Graphics::Color(RED));
			//litMaterialPink->SetDiffuse(texture);
			//litMaterialPink->SetReflection(m_app.GetSkybox()->GetCubemap(), propView);
			//litMaterialPink->SetReflectionFactor(0.2f);
			auto litMaterialGreen = scene.CreateMaterial(Rendering::MATERIAL_TYPE::LIT);
			litMaterialGreen->SetDiffuse(Graphics::Color(GREEN));
			scene.BindMaterialsToModel(model, { litMaterialPink, litMaterialGreen });
		}


		LT_INFO("Scene 1 loaded!");
		loaded = true;
	}

	void OnUpdate(float deltaTime) override
	{
		using namespace Llamathrust::Graphics;
		using namespace Llamathrust::Rendering;

		// Calculate camera position
		float posX = distance * cos(glm::radians(180 - propView->m_yaw)) * cos(glm::radians(propView->m_pitch));
		float posY = distance * sin(glm::radians(180 + propView->m_pitch));
		float posZ = distance * sin(glm::radians(180 + propView->m_yaw)) * cos(glm::radians(propView->m_pitch));

		propView->SetPosition(glm::vec3(posX, posY, posZ));

		if (castRay)
		{
			RayCastHitInfo hit_info;

			if (scene.RayCast(propView->m_position, propView->m_position + (pick_position * 100.0f), hit_info))
			{
				LT_INFO("RAYHIT!: {0}", hit_info.node->GetName());
			}
			castRay = false;
		}
		scene.Update();
	}

	void OnRender() override
	{
		using namespace Llamathrust::Graphics;

		m_app.GetVideoDriver()->enableDepthTesting(DEPTH_TESTING::ENABLE);
		m_app.GetVideoDriver()->enableCullFace(CULL_FACES::BACK_FACE);

		// Wait until scene is loaded
		if (!loaded)
			return;
		// When loaded into the RAM load it into the VRAM
		if (!gpuInit)
		{
			gpuInit = true;
			scene.LoadGPU();
		}
		// Now we can draw
		scene.Draw();
	}

	void OnEvent(Llamathrust::Event& e) override
	{
		using namespace Llamathrust;

		EventDispatcher dispatcher(e);

		dispatcher.Dispatch<KeyPressedEvent>(LT_BIND_EVENT_FN(PrimaryLayer::MoveNodePressed));
		dispatcher.Dispatch<KeyReleasedEvent>(LT_BIND_EVENT_FN(PrimaryLayer::MoveNodeReleased));
		dispatcher.Dispatch<MouseMovedEvent>(LT_BIND_EVENT_FN(PrimaryLayer::MouseMoved));
		dispatcher.Dispatch<MouseButtonPressedEvent>(LT_BIND_EVENT_FN(PrimaryLayer::MouseClick));
		dispatcher.Dispatch<MouseButtonReleasedEvent>(LT_BIND_EVENT_FN(PrimaryLayer::MouseUnClick));
		dispatcher.Dispatch<MouseScrolledEvent>(LT_BIND_EVENT_FN(PrimaryLayer::MouseScroll));
	}

	void OnImGuiRender() override
	{
		ImVec2 size(192, 256);
		ImGui::SetNextWindowSizeConstraints(size, size);
		if (ImGui::Begin("PrimaryLayer"))
		{
#ifdef LT_DEBUG
			ImGui::Text("CameraRotation:\n");
			ImGui::Text("\tPitch: %.4f\n", propView->m_pitch);
			ImGui::Text("\tYaw: %.4f\n", propView->m_yaw);
			ImGui::Separator();
			ImGui::Text("CameraPosition:\n");
			ImGui::Text("\tX Axis: %.4f\n", propView->m_position.x);
			ImGui::Text("\tY Axis: %.4f\n", propView->m_position.y);
			ImGui::Text("\tZ Axis: %.4f\n", propView->m_position.z);
			ImGui::Separator();
			ImGui::Text("Camera distance:");
			ImGui::Text("\tfrom center: %.4f\n", distance);
			ImGui::Separator();
#endif
			if (ImGui::Button("Cook/Save"))
			{
				CookModel();
			}

			if (!loaded)
			{
				ImGui::Separator();
				ImGui::Text("Loading %c", "|/-\\"[(int)(ImGui::GetTime() / 0.05f) & 3]);
			}
		}
		ImGui::End();
	}

private:

	void CookModel()
	{
	}

	bool MoveNodePressed(Llamathrust::KeyPressedEvent& key)
	{
		using namespace Llamathrust;
		switch (key.GetKeyCode())
		{
		case LT_KEY_ESCAPE:
			m_app.Close();
			return true;
		}

		return false;
	}

	bool MoveNodeReleased(Llamathrust::KeyReleasedEvent& key)
	{
		using namespace Llamathrust;
		switch (key.GetKeyCode())
		{
		case LT_KEY_0:
		default:
			break;
		}
		return false;
	}

	bool MouseMoved(Llamathrust::MouseMovedEvent& mouse)
	{
		if (!cam_can_move)
		{
			init = true;
			return false;
		}

		if (init)
		{
			mouseXold = mouse.GetX();
			mouseYold = mouse.GetY();
			init = false;
		}

		float xoffset = mouse.GetX() - mouseXold;
		float yoffset = mouse.GetY() - mouseYold;
		mouseXold = mouse.GetX();
		mouseYold = mouse.GetY();

		xoffset *= mouse_sensitivity;
		yoffset *= -mouse_sensitivity;

		float Yaw = propView->m_yaw;
		float Pitch = propView->m_pitch;

		Yaw += xoffset;
		Pitch += yoffset;

		// Make sure that when pitch is out of bounds, screen doesn't get flipped
		if (Pitch > 89.0f)
			Pitch = 89.0f;
		if (Pitch < -89.0f)
			Pitch = -89.0f;

		if (Yaw > 360)
			Yaw = 0;
		if (Yaw < 0)
			Yaw = 360;
		propView->SetRotation(Pitch, Yaw);

		return true;
	}

	bool MouseScroll(Llamathrust::MouseScrolledEvent& mouse)
	{
		distance -= mouse.GetYOffset() * zoomScale;
		return true;
	}

	bool MouseClick(Llamathrust::MouseButtonPressedEvent& click)
	{
		if (click.GetMouseButton() == Llamathrust::LT_MOUSE_BUTTON_1)
		{
			cam_can_move = true;
			auto mouse_pos = Llamathrust::Input::GetMousePosition();
			scene.GetPickRay(mouse_pos, pick_position);
			castRay = true;
		}

		return true;
	}

	bool MouseUnClick(Llamathrust::MouseButtonReleasedEvent& click)
	{
		cam_can_move = false;
		return true;
	}
};

// This layer is used to do general app operations from ImGui
class GeneralLayer : public Llamathrust::Layer
{
#ifdef LT_DEBUG
	Llamathrust::DrawCallListener* drawCallListener = nullptr;
#endif
	Llamathrust::Rendering::RenderDevice* renderDevice = nullptr;

	Llamathrust::Rendering::Scene scene;
	Llamathrust::Rendering::SceneNode* viewer_node = nullptr;
	Llamathrust::Rendering::ViewProperties* propView = nullptr;
	Llamathrust::Rendering::RenderTarget* guiRenderTarget;

	Llamathrust::Rendering::Scene sceneGUI;
	float screen_edge = 1.78f;
	float distance = 1.0f;
	bool cam_can_move = false;
	bool init = true;
	float mouseXold = 0;
	float mouseYold = 0;
	const float mouse_sensitivity = 0.2f;
public:
	GeneralLayer() : Layer("General Layer")
	{
		using namespace Llamathrust;

		drawCallListener = DrawCallListener::Get();
		renderDevice = Rendering::RenderDevice::Get();


		auto window = m_app.GetWindow();
		auto cam = new Rendering::Camera((float)window->GetWidth(), (float)window->GetHeight());
		// Just a camera since it never moves the view
		sceneGUI.AddCameraSetActive(cam);

		// Textured GUI Plane
		Rendering::Texture2D* texture;
		{
			Rendering::Model* planeModel = m_app.GetAssetManager()->LoadModel("../res/models/plane.mudm", "planeGUI");
			{
				float scale = 0.3f;
				glm::mat4 mat4Trans =
					glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1, 0, 0))
					* glm::scale(glm::mat4(1.0f), glm::vec3(1, 0, 1) * scale);

				auto transform = glm::mat3(mat4Trans);

				for (auto& vert : planeModel->GetMeshes()[0]->GetVertices())
					vert.position = transform * vert.position + glm::vec3(screen_edge - scale, scale - 1.0f, 0);
			}

			sceneGUI.AddModel(planeModel, glm::mat4(1.0f), nullptr, "GUIPlane");
			auto guiMaterial = sceneGUI.CreateMaterialAndBind(Rendering::MATERIAL_TYPE::TEXTURED, planeModel);

			Rendering::Texture_Desc texDesc;
			texDesc.Width = texDesc.Height = 256;
			texDesc.Format = Rendering::IMAGE_FORMAT::RGBA_32;
			texDesc.MipLevels = false;

			texture = renderDevice->CreateTexture2D(nullptr, texDesc);
			// Create empty texture
			texture->SetData(nullptr);
			guiMaterial->SetDiffuse(texture);

			sceneGUI.LoadGPU();
			// Greate the render target for render to texture
			guiRenderTarget = renderDevice->CreateRenderTarget(Rendering::RenderTarget::BUFFER_TYPE::COLOR);
			guiRenderTarget->Bind();
			guiRenderTarget->CreateTargetAndSetTexture(texture);
			guiRenderTarget->Unbind();
		}

		cam = new Rendering::Camera(64.0f, 64.0f);
		scene.AddCameraSetActive(cam);

		glm::vec3& position = *(new glm::vec3(distance, 0, 0));
		propView = new Rendering::ViewProperties(position, -20.5f, 318, *(new glm::vec3(0, 1, 0)));
		// Set new ViewProps
		scene.AddViewNodeSetActive(propView);


		Rendering::Model* axesModel = nullptr;
		// Axis Lines
		{
			Rendering::Vertex vertex;
			vertex.normal = glm::vec3(0, 1, 0);
			vertex.uvcoord = glm::vec2(0, 0);

			std::vector<Rendering::Vertex> vertices;
			vertex.position = glm::vec3(1, 0, 0);
			vertices.emplace_back(vertex);

			std::vector<Rendering::Vertex> verticesY;
			vertex.position = glm::vec3(0, 1, 0);
			verticesY.emplace_back(vertex);

			std::vector<Rendering::Vertex> verticesZ;
			vertex.position = glm::vec3(0, 0, 1);
			verticesZ.emplace_back(vertex);

			vertex.position = glm::vec3(0, 0, 0);
			vertices.emplace_back(vertex);
			verticesY.emplace_back(vertex);
			verticesZ.emplace_back(vertex);

			float loc = 0.45f;
			auto translate = glm::vec3(0, -loc, 0);
			auto transform = glm::translate(glm::mat4(1.0f), translate)
				* glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1) * loc * 2.0f);
			for (auto& vert : vertices)
				//vert.position += translate;
				vert.position = (transform * glm::vec4(vert.position, 1.0f)).xyz;
			for (auto& vert : verticesY)
				vert.position = (transform * glm::vec4(vert.position, 1.0f)).xyz;
			for (auto& vert : verticesZ)
				vert.position = (transform * glm::vec4(vert.position, 1.0f)).xyz;


			std::vector<uint_32> indices = { 0, 1 };

			Rendering::Mesh* x_axisMesh = renderDevice->CreateMesh(vertices, indices, nullptr, Rendering::DRAW_MODE::LINES);
			Rendering::Mesh* y_axisMesh = renderDevice->CreateMesh(verticesY, indices, nullptr, Rendering::DRAW_MODE::LINES);
			Rendering::Mesh* z_axisModel = renderDevice->CreateMesh(verticesZ, indices, nullptr, Rendering::DRAW_MODE::LINES);
			axesModel = new Rendering::Model({ x_axisMesh, y_axisMesh, z_axisModel });
			scene.AddModel(axesModel, glm::mat4(1.0f), nullptr, "GimbalModel");
		}

		//Materials
		auto greenMaterial = scene.CreateMaterial(Rendering::MATERIAL_TYPE::UNLIT);
		greenMaterial->SetDiffuse(Graphics::Color(GREEN));

		auto redMaterial = scene.CreateMaterial(Rendering::MATERIAL_TYPE::UNLIT);
		redMaterial->SetDiffuse(Graphics::Color(RED));

		auto blueMaterial = scene.CreateMaterial(Rendering::MATERIAL_TYPE::UNLIT);
		blueMaterial->SetDiffuse(Graphics::Color(BLUE));

		scene.BindMaterialsToModel(axesModel, { redMaterial, greenMaterial, blueMaterial });
		scene.LoadGPU();
	}

	void OnRender() override
	{
		using namespace Llamathrust;
		using namespace Graphics;

		guiRenderTarget->Bind();

		m_app.GetVideoDriver()->enableDepthTesting(DEPTH_TESTING::ENABLE);
		m_app.GetVideoDriver()->enableCullFace(CULL_FACES::NONE);
		m_app.GetVideoDriver()->setClearColor(Color(TRANSPARENT));
		m_app.GetVideoDriver()->clearScreenBuffer(BUFFERTYPESFLAGS::COLOR_BUFFER | BUFFERTYPESFLAGS::DEPTH_BUFFER);

		Viewport oldViewPort = m_app.GetVideoDriver()->getViewport();
		const Rendering::Texture_Desc* description = guiRenderTarget->GetTexture()->GetDescription();
		int height, width;
		height = description->Height;
		width = description->Width;

		Viewport viewport = { 0, 0, width, height };
		m_app.GetVideoDriver()->setViewport(&viewport);
		glLineWidth(4);

		scene.Draw();
		guiRenderTarget->Unbind();

		glLineWidth(1);
		m_app.GetVideoDriver()->setViewport(&oldViewPort);
		m_app.GetVideoDriver()->enableBlending(true);
		m_app.GetVideoDriver()->enableDepthTesting(DEPTH_TESTING::DISABLE);
		m_app.GetVideoDriver()->setClearColor(Color(BACKGROUND_COLOR));
		sceneGUI.Draw();
		m_app.GetVideoDriver()->enableBlending(false);
	}

	void OnUpdate(float deltaTime) override
	{
		using namespace Llamathrust::Graphics;

		// Calculate camera position
		float posX = distance * cos(glm::radians(180 - propView->m_yaw)) * cos(glm::radians(propView->m_pitch));
		float posY = distance * sin(glm::radians(180 + propView->m_pitch));
		float posZ = distance * sin(glm::radians(180 + propView->m_yaw)) * cos(glm::radians(propView->m_pitch));

		propView->SetPosition(glm::vec3(posX, posY, posZ));
	}

	void OnImGuiRender() override
	{
		ImVec2 size(300, 256);
		ImGui::SetNextWindowSizeConstraints(size, size);

		if (ImGui::Begin("General Info"))
		{
			ImGuiIO& io = ImGui::GetIO();
			ImGui::Text("Average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
#ifdef LT_DEBUG
			ImGui::Text("Shader changes %u", drawCallListener->GetShaderChangesCount());
#endif
			ImGui::Separator();

			ImGui::Text("Exit Application: ESC");
			ImGui::Separator();

			ImGui::Text("Camera Zoom: ");
			ImGui::Text("\tscroll");
			ImGui::Text("Camera Rotation: ");
			ImGui::Text("\tleft mouse + mouse movement");

			ImGui::Separator();

			if (ImGui::Button("Conect to Blender"))
			{
			}
		}
		ImGui::End();
	}

	void OnEvent(Llamathrust::Event& e) override
	{
		using namespace Llamathrust;

		EventDispatcher dispatcher(e);

		dispatcher.Dispatch<WindowResizeEvent>(LT_BIND_EVENT_FN(GeneralLayer::WindowResize));
		dispatcher.Dispatch<MouseMovedEvent>(LT_BIND_EVENT_FN(GeneralLayer::MouseMoved));
		dispatcher.Dispatch<MouseButtonPressedEvent>(LT_BIND_EVENT_FN(GeneralLayer::MouseClick));
		dispatcher.Dispatch<MouseButtonReleasedEvent>(LT_BIND_EVENT_FN(GeneralLayer::MouseUnClick));
	}

	bool WindowResize(Llamathrust::WindowResizeEvent& e)
	{
		sceneGUI.GetActiveCamera()->RecalculateProjectionMatrix((float)e.GetWindowWidth(), (float)e.GetWindowHeight());
		screen_edge = (float)e.GetWindowWidth() / (float)e.GetWindowHeight();
		return true;
	}

	bool MouseClick(Llamathrust::MouseButtonPressedEvent& click)
	{
		if (click.GetMouseButton() == Llamathrust::LT_MOUSE_BUTTON_1)
			cam_can_move = true;
		return false;
	}

	bool MouseUnClick(Llamathrust::MouseButtonReleasedEvent& click)
	{
		cam_can_move = false;
		return false;
	}

	bool MouseMoved(Llamathrust::MouseMovedEvent& mouse)
	{
		if (!cam_can_move)
		{
			init = true;
			return false;
		}

		if (init)
		{
			mouseXold = mouse.GetX();
			mouseYold = mouse.GetY();
			init = false;
		}

		float xoffset = mouse.GetX() - mouseXold;
		float yoffset = mouse.GetY() - mouseYold;
		mouseXold = mouse.GetX();
		mouseYold = mouse.GetY();

		xoffset *= mouse_sensitivity;
		yoffset *= -mouse_sensitivity;

		float Yaw = propView->m_yaw;
		float Pitch = propView->m_pitch;

		Yaw += xoffset;
		Pitch += yoffset;

		// Make sure that when pitch is out of bounds, screen doesn't get flipped
		if (Pitch > 89.0f)
			Pitch = 89.0f;
		if (Pitch < -89.0f)
			Pitch = -89.0f;

		if (Yaw > 360)
			Yaw = 0;
		if (Yaw < 0)
			Yaw = 360;
		propView->SetRotation(Pitch, Yaw);

		return false;
	}
};


// You must extend the 'Application' class of the engine
struct Editor : public Llamathrust::Application {

	Editor() : Llamathrust::Application("Editor", 1280, 720, glm::vec3(BACKGROUND_COLOR))
	{
		LT_INFO("SANDBOX INITIALIZED");
		// NOTE: Order matters
		PushLayer(new BackgroundLayer());
		PushLayer(new GridLayer());
		PushLayer(new PrimaryLayer());
		PushLayer(new GeneralLayer());
	}

	~Editor()
	{

	}
};

// Send the custom 'Sandbox' class to the engine to run it
LLAMATHRUST_APPCLASS(Editor)