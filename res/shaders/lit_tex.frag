#version 430
out vec4 out_color;
in vec2 uv_coord;
in vec3 normal;

uniform sampler2D diffuse;

const vec3 lightDir = vec3(-1, -1, 0);

void main()
{
    float dif_val = max(dot(normal, -normalize(lightDir)), 0.0);

    vec4 light_diffuse = vec4(1, 1, 1, 1);
    vec4 light_ambient = light_diffuse * 0.1f;
    
    
    vec4 diffuseCol = dif_val * light_diffuse;
    
    out_color =  (diffuseCol + light_ambient) * texture(diffuse, uv_coord);
}