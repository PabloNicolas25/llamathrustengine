#version 430
layout(location = 0)in vec3 position;
layout(location = 2)in vec2 v_uvCoord;

out vec2 uv_coord;

uniform mat4 pvm_transform;

void main()
{
    uv_coord = v_uvCoord;
    vec4 pos = vec4(position, 1);
    gl_Position = pvm_transform * pos;
}