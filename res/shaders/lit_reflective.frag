#version 430
out vec4 out_color;
in vec3 in_normal;
in vec3 in_position;
in vec3 in_cameraPos;

uniform vec3 color;
uniform samplerCube skybox;
uniform float reflection_factor;
const vec3 lightDir = vec3(-1, -1, 0);

void main()
{
    float dif_val = max(dot(in_normal, -normalize(lightDir)), 0.0);

    vec4 obj_color = vec4(color, 1);
    
    vec4 light_diffuse = vec4(1, 1, 1, 1);
    vec4 light_ambient = light_diffuse * 0.4f;
    
    vec3 I = normalize(in_position - in_cameraPos);
    vec3 R = reflect(I, normalize(in_normal));
    vec4 reflection = vec4(texture(skybox, R).rgb, 1.0);
    
    vec4 diffuseCol = dif_val * light_diffuse;
    float mix_factor = max(min(reflection_factor, 1.0), 0.0);
    out_color = (diffuseCol + light_ambient) * obj_color;
    out_color = mix(out_color,  reflection, mix_factor);
}