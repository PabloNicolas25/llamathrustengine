#version 430
layout(location = 0)in vec3 v_position;
layout(location = 1)in vec3 v_normal;
layout(location = 2)in vec2 v_uvCoord;

out vec3 normal;
out vec2 uv_coord;

uniform mat4 pvm_transform;

void main()
{
    uv_coord = v_uvCoord;
    normal = mat3(pvm_transform) * v_normal;
    vec4 pos = vec4(v_position, 1);
    gl_Position = pvm_transform * pos;
}