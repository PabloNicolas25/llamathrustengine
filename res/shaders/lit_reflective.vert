#version 430
layout(location = 0)in vec3 v_position;
layout(location = 1)in vec3 v_normal;
layout(location = 2)in vec2 v_uvCoord;

out vec2 uv_coord;
out vec3 in_normal;
out vec3 in_position;
out vec3 in_cameraPos;
uniform mat4 pvm_transform;
uniform mat4 model_transform = mat4(1.0f);
uniform vec3 camPos;

void main()
{
    uv_coord = v_uvCoord;
    in_normal = mat3(pvm_transform) * v_normal;
    in_cameraPos = camPos;

    vec4 pos = vec4(v_position, 1);
    gl_Position = pvm_transform * pos;
    in_position = gl_Position.xyz;
}