#version 430
out vec4 out_color;
in vec2 uv_coord;

uniform sampler2D diffuse;

void main()
{
    out_color = texture(diffuse, uv_coord);
}