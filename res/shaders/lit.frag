#version 430
out vec4 out_color;
in vec3 normal;

const vec3 lightDir = vec3(-1, -1, 0);
uniform vec3 color;

void main()
{
    float dif_val = max(dot(normal, -normalize(lightDir)), 0.0);

    vec4 obj_color = vec4(color, 1);
    
    vec4 light_diffuse = vec4(1, 1, 1, 1) * 0.6;
    vec4 light_ambient = obj_color * 0.4f;
    
    
    vec4 diffuseCol = obj_color * dif_val * light_diffuse;
    
    out_color =  diffuseCol + light_ambient;
}