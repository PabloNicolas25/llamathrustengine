workspace "LlamathrustEngine"
    architecture "x64"
    
    configurations
    {
        "Debug",
        "Release",
        "Dist"
    }
    
    startproject "Sandbox"


outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Inlcude directories relative to root folder (solution directory)
IncludeDir = {}
IncludeDir["GLFW"] = "LlamathrustEngine/vendor/GLFW/include"
IncludeDir["GLAD"] = "LlamathrustEngine/vendor/GLAD/include"
IncludeDir["ImGui"] = "LlamathrustEngine/vendor/ImGui"
IncludeDir["GLM"] = "LlamathrustEngine/vendor/glm"

include "LlamathrustEngine/vendor/GLFW"
include "LlamathrustEngine/vendor/GLAD"
include "LlamathrustEngine/vendor/ImGui"

project "LlamathrustEngine"
    location "LlamathrustEngine"
    kind "StaticLib"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    ignoredefaultlibraries
    {
        "LIBCMT",
    }


    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    pchheader "ltpch.hpp"
    pchsource "LlamathrustEngine/src/ltpch.cpp"

    files
    {
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.hpp",
        "%{prj.name}/src/**.cpp",
        "%{prj.name}/vendor/glm/**.hpp",
        "%{prj.name}/vendor/glm/**.inl"
    }

    defines
    {
        "_CRT_SECURE_NO_WARNINGS",
    }

    includedirs
    {
        "%{prj.name}/src",
        "%{prj.name}/vendor/spdlog/include",
        "%{IncludeDir.GLFW}",
        "%{IncludeDir.GLAD}",
        "%{IncludeDir.ImGui}",
        "%{IncludeDir.GLM}"
    }

    links
    {
        "GLFW",
        "GLAD",
        "ImGui",
        "opengl32.lib"
    }

    filter "system:windows"
        staticruntime "On"
        systemversion "latest"
        
        defines
        {
            "LT_PLATFORM_WINDOWS",
            "GLFW_INCLUDE_NONE",
        }

        links{
            "D3D11",
            "D3DCompiler"
        }
        
    filter "configurations:Debug"
        defines {"LT_DEBUG", "LT_ENABLE_ASSERTS"}
        runtime "Debug"
        symbols "On"

    filter "configurations:Release"
        defines {"LT_RELEASE", "LT_ENABLE_ASSERTS"}
        runtime "Release"
        optimize "On"

    filter "configurations:Dist"
        defines "LT_DIST"
        runtime "Release"
        optimize "On"

project "Sandbox"
    location "Sandbox"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    ignoredefaultlibraries
    {
        "LIBCMT",
    }

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.hpp",
        "%{prj.name}/src/**.cpp"
    }

    includedirs
    {
        "LlamathrustEngine/vendor/spdlog/include",
        "LlamathrustEngine/src",
        "%{IncludeDir.ImGui}",
        "%{IncludeDir.GLM}",
        "%{IncludeDir.GLAD}",
    }

    links
    {
        "LlamathrustEngine",
    }
    
    filter "system:windows"
        systemversion "latest"

        defines
        {
            "LT_PLATFORM_WINDOWS",
        }

    filter "configurations:Debug"
        defines "LT_DEBUG"
        runtime "Debug"
        symbols "On"

    filter "configurations:Release"
        defines "LT_RELEASE"
        runtime "Release"
        optimize "On"

    filter "configurations:Dist"
        defines "LT_DIST"
        runtime "Release"
        optimize "On"