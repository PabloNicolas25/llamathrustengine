#pragma once

#include "core/Core.hpp"
#include "renderer/RenderDevice.hpp"

namespace Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API GLRenderDevice : public RenderDevice
		{
		public:

			Texture2D* CreateTexture2D(const char* in_filePath, Texture_Desc& in_textureDesc) override;

			Cubemap* CreateCubemap(std::vector<std::string>& in_filePaths, Texture_Desc& in_textureDesc) override;

			Mesh* CreateMesh(const std::vector<Vertex>& in_vertices, const std::vector<uint_32>& in_indices,
				DataStructures::AABB *in_aabb, const DRAW_MODE in_mode) override;

			Graphics::Shader* CreateShader(const Graphics::SHADER_TYPE in_type, const char* in_fileName) override;
			
			RenderTarget* CreateRenderTarget(const RenderTarget::BUFFER_TYPE in_type) override;

		protected:
			Graphics::ShaderProgram* CreateShaderProgram(const char* in_vertexName, const char* in_pixelName) override;

			Graphics::ShaderProgram* CreateShaderProgram(Graphics::Shader* in_vertexShader, Graphics::Shader* in_pixelShader) override;
		};
	}
}