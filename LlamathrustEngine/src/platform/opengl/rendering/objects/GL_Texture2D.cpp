#include "ltpch.hpp"
#include "GL_Texture2D.hpp"
#include <glad/glad.h>

namespace Llamathrust::Rendering
{
	GLenum GetGLformat(const IMAGE_FORMAT in_format)
	{
		switch (in_format)
		{
		case IMAGE_FORMAT::RGB_24:
			return GL_RGB;
		case IMAGE_FORMAT::RGBA_32:
		default:
			return GL_RGBA;
		}
	}

	GLenum GetGLinternal(const IMAGE_FORMAT in_format)
	{
		switch (in_format)
		{
		case IMAGE_FORMAT::RGB_24:
			return GL_RGB8;
		case IMAGE_FORMAT::RGBA_32:
		default:
			return GL_RGBA8;
		}
	}

	GLenum GetGLTarget(const TEXTURE_TARGET in_target)
	{
		switch (in_target)
		{
		case TEXTURE_TARGET::CUBEMAP_FACE_POSITIVE_X:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
		case TEXTURE_TARGET::CUBEMAP_FACE_NEGATIVE_X:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
		case TEXTURE_TARGET::CUBEMAP_FACE_POSITIVE_Y:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
		case TEXTURE_TARGET::CUBEMAP_FACE_NEGATIVE_Y:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
		case TEXTURE_TARGET::CUBEMAP_FACE_POSITIVE_Z:
			return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
		case TEXTURE_TARGET::CUBEMAP_FACE_NEGATIVE_Z:
			return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
		case TEXTURE_TARGET::TEXTURE_2D:
		default:
			return GL_TEXTURE_2D;
		}
	}

	GLTexture2D::GLTexture2D(Texture_Desc* in_description) : Texture2D(in_description)
	{
	}

	void* GLTexture2D::Get()
	{
		return reinterpret_cast<void*>(m_id);
	}

	void GLTexture2D::Use(uint_32 in_textureUnit)
	{
		glActiveTexture(GL_TEXTURE0 + in_textureUnit);
		glBindTexture(GL_TEXTURE_2D, m_id);
	}

	void GLTexture2D::SetData(byte* in_byteArray)
	{
		m_data = in_byteArray;
	}

	void GLTexture2D::UploadData()
	{
		GLenum mem_format = GetGLformat(m_description->Format);
		GLenum inter_format = GetGLinternal(m_description->Format);
		glTexImage2D(
			GetGLTarget(m_description->Target),
			0,
			inter_format,
			m_description->Width,
			m_description->Height,
			0,
			mem_format,
			GL_UNSIGNED_BYTE,
			m_data);
	}

	void GLTexture2D::LoadGPU()
	{
		if (loaded) return;

		glGenTextures(1, &m_id);
		glBindTexture(GL_TEXTURE_2D, m_id);
		// Border
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// Filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		if (m_description->MipLevels)
			glGenerateMipmap(GL_TEXTURE_2D);

		UploadData();
		delete m_data;
	}
}