#pragma once
#include "core/Core.hpp"
#include "renderer/Objects/RenderTarget.hpp"


namespace Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API GLRenderTarget : public RenderTarget
		{
			uint_32 m_targetBuffer;
			GLenum  m_type;

		public:
			GLRenderTarget(RenderTarget::BUFFER_TYPE in_type);
			~GLRenderTarget();

			void ReCreate(RenderTarget::BUFFER_TYPE in_type) override;
			void CreateTargetAndSetTexture(Texture2D* in_texture) override;
			void Bind() override;
			void Unbind() override;
		};
	}
}