#pragma once
#include "core/Core.hpp"
#include "renderer/Objects/Cubemap.hpp"

namespace Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API GL_Cubemap : public Cubemap
		{
		protected:
			uint_32 m_id;

		public:
			GL_Cubemap(const std::initializer_list<CubemapFace>& in_faces);
			~GL_Cubemap();

			/* Get platform texture object */
			void* Get() override;

			/* Use or Bind this Texture object */
			void Use(uint_32 in_textureUnit = 0) override;

			/* Assign data to this Texture object */
			void SetData(byte* in_byteArray) override {}

			/* Upload data to the GPU */
			void UploadData() override;

			/* Generate Texture and UploadData */
			void LoadGPU() override;
		};
	}
}