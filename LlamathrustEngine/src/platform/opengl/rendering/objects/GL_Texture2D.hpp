#pragma once
#include "core/Core.hpp"
#include "renderer/Objects/Texture2D.hpp"

namespace Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API GLTexture2D : public Texture2D
		{
			uint_32 m_id;
		public:
			GLTexture2D(Texture_Desc* in_description);


			void* Get() override;

			void Use(uint_32 in_textureUnit = 0) override;

			void SetData(byte* in_byteArray) override;

			void UploadData() override;

			void LoadGPU() override;
		};
	}
}
