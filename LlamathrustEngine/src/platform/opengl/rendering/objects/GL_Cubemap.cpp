#include "ltpch.hpp"
#include "GL_Cubemap.hpp"

namespace Llamathrust::Rendering
{
	GL_Cubemap::GL_Cubemap(const std::initializer_list<CubemapFace>& in_faces)
		: Cubemap(in_faces)
	{
	}
	GL_Cubemap::~GL_Cubemap()
	{
		glDeleteTextures(1, &m_id);
	}
	void* GL_Cubemap::Get()
	{
		return reinterpret_cast<void*>(m_id);
	}
	void GL_Cubemap::Use(uint_32 in_textureUnit)
	{
		glActiveTexture(GL_TEXTURE0 + in_textureUnit);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);
	}
	void GL_Cubemap::UploadData()
	{
		for (int i = 0; i < 6; i++)
			m_data[i]->UploadData();
	}
	void GL_Cubemap::LoadGPU()
	{
		glGenTextures(1, &m_id);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_id);

		m_description->Height = m_description->Width = m_data[0]->GetDescription()->Height;
		m_description->Format = m_data[0]->GetDescription()->Format;

		UploadData();

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}
}