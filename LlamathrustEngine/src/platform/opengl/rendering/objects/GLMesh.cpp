#include "ltpch.hpp"
#include "GLMesh.hpp"
#include "platform/opengl/graphics/GL_Buffer.hpp"


namespace Llamathrust::Rendering
{
	static void SetMatrices(glm::mat4* in_matrices, size_t in_matricesCount)
	{
		// Get the buffer size
		size_t matrices_array_size_b = 16 * sizeof(float) * in_matricesCount;
		int buffer_size;
		glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &buffer_size);

		// If the buffer is bigger than the matrix vector then we can update a subset of it
		if (buffer_size > matrices_array_size_b)
		{
			glBufferSubData(GL_ARRAY_BUFFER, 0, matrices_array_size_b, in_matrices);
		}
		// else we need to recreate the buffer to a bigger size (it request more mem in the gpu)
		else
		{
			glBufferData(GL_ARRAY_BUFFER, in_matricesCount, (void*)in_matrices, GL_STATIC_DRAW);
		}
	}

	static GLenum GetDrawMode(DRAW_MODE in_mode)
	{
		switch (in_mode)
		{
		case LINES:
			return GL_LINES;
		case TRIANGLES:
		default:
			return GL_TRIANGLES;
		}
	}

	GLMesh::GLMesh(const std::vector<Vertex>& in_vertices, const std::vector<uint_32>& in_indices,
		DataStructures::AABB *in_aabb, const DRAW_MODE in_mode) : Mesh(in_vertices, in_indices, in_aabb, in_mode),
		m_glMode(GetDrawMode(in_mode))
	{
	}

	GLMesh::~GLMesh()
	{
		UnloadGPU();
	}

	void GLMesh::LoadGPU() 
	{
		using namespace Graphics;

		BufferLayout layout = {
			{SHADER_DATA_TYPE::Float3, "v_position"},
			{SHADER_DATA_TYPE::Float3, "v_normal"},
			{SHADER_DATA_TYPE::Float2, "v_uvCoord"},
		};

		// Generate the vertex array (all the info about the mesh is in this memory)
		glGenVertexArrays(1, &m_vao);
		glBindVertexArray(m_vao);

		m_vertexBuffer = new GLVertexBuffer(&m_vertices.data()->position.x, (uint_32)m_vertices.size() * 8, layout);

		m_indexBuffer = new GLIndexBuffer(m_indices.data(), (uint_32)m_indices.size());
	}

	void GLMesh::UnloadGPU() const
	{
		glDeleteVertexArrays(1, &m_vao);
	}

	void GLMesh::Bind() const
	{
		glBindVertexArray(m_vao);
	}

	void GLMesh::Draw(uint_32 instance_count) const
	{
		glDrawElements(m_glMode, (GLsizei) m_indices.size(), GL_UNSIGNED_INT, NULL);
	}
}
