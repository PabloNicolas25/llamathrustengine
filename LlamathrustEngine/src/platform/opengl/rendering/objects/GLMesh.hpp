#pragma once


#include "core/Core.hpp"
#include "renderer/Objects/Mesh.hpp"
#include <glm/glm.hpp>
#include <glad/glad.h>
#include <vector>

namespace  Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API GLMesh : public Mesh
		{
			GLuint m_vao;
			GLenum m_glMode;
		public:
			GLMesh(const std::vector<Vertex>& in_vertices,
				const std::vector<uint_32>& in_indices,
				DataStructures::AABB *in_aabb, const DRAW_MODE in_mode);
			~GLMesh();

			// Send the mesh into the GPU
			void LoadGPU() override;

			// Delete the mesh from the GPU
			void UnloadGPU() const override;

			// Must be called before 'Draw'
			void Bind() const override;

			// Draw instaces_count instances of this mesh
			void Draw(uint_32 instance_count = 1) const override;
		};
	}
}
