#include "ltpch.hpp"
#include "GLRenderTarget.hpp"
namespace Llamathrust::Rendering
{
	GLRenderTarget::GLRenderTarget(RenderTarget::BUFFER_TYPE in_type) : RenderTarget(in_type)
	{
		ReCreate(in_type);
	}
	GLRenderTarget::~GLRenderTarget()
	{
		glDeleteFramebuffers(1, &m_targetBuffer);
	}


	void GLRenderTarget::ReCreate(RenderTarget::BUFFER_TYPE in_type)
	{
		switch (in_type)
		{
		case Llamathrust::Rendering::RenderTarget::DEPTH:
			break;
		case Llamathrust::Rendering::RenderTarget::STENCIL:
			break;
		case Llamathrust::Rendering::RenderTarget::COLOR:
		default:
			m_type = GL_COLOR_ATTACHMENT0;
		}

		glGenFramebuffers(1, &m_targetBuffer);
	}

	void GLRenderTarget::CreateTargetAndSetTexture(Texture2D* in_texture)
	{
		m_texture = in_texture;
		glFramebufferTexture2D(GL_FRAMEBUFFER, m_type, GL_TEXTURE_2D, (uint_32)m_texture->Get(), 0);

		// Check if the framebuffer created successfuly
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			LT_ERROR("BUFFER_TARGET NOT CREATED!!");
	}
	
	
	void GLRenderTarget::Bind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_targetBuffer);
	}
	void GLRenderTarget::Unbind()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, NULL);
	}
}
