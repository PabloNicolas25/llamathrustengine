#include "ltpch.hpp"
#include "GLRenderDevice.hpp"
#include "platform/opengl/rendering/objects/GL_Cubemap.hpp"
#include "platform/opengl/rendering/objects/GLMesh.hpp"
#include "platform/opengl/rendering/objects/GLRenderTarget.hpp"
#include "platform/opengl/graphics/GL_Shader.hpp"
#include "platform/opengl/graphics/GL_ShaderProgram.hpp"
#include "core/loaders/images/stb_image.h"
#include "objects/GL_Texture2D.hpp"


namespace Llamathrust::Rendering
{
	Texture2D* GLRenderDevice::CreateTexture2D(const char* in_filePath, Texture_Desc& in_textureDesc)
	{
		auto description = new Texture_Desc(in_textureDesc);
		auto tex = new GLTexture2D(description);
		if (in_filePath)
		{
			int channels;
			switch (in_textureDesc.Format)
			{
			case IMAGE_FORMAT::RGB_24:
				channels = 3;
				break;
			case IMAGE_FORMAT::RGBA_32:
			default:
				channels = 4;
				break;
			}
			byte* data = stbi_load(in_filePath, &description->Width, &description->Height, nullptr, channels);
			if (!data)
			{
				LT_WARN("Image {0} not loaded!", in_filePath);
				return tex;
			}
			tex->SetData(data);
		}
		return tex;
	}

	Cubemap* GLRenderDevice::CreateCubemap(std::vector<std::string>& in_filePaths, Texture_Desc& in_textureDesc)
	{
		Rendering::CubemapFace facePosX = {
			Rendering::TEXTURE_TARGET::CUBEMAP_FACE_POSITIVE_X,
			CreateTexture2D(in_filePaths[0].c_str(), in_textureDesc)
		};
		Rendering::CubemapFace faceNegX = {
			Rendering::TEXTURE_TARGET::CUBEMAP_FACE_NEGATIVE_X,
			CreateTexture2D(in_filePaths[1].c_str(), in_textureDesc)
		};
		Rendering::CubemapFace facePosY = {
			Rendering::TEXTURE_TARGET::CUBEMAP_FACE_POSITIVE_Y,
			CreateTexture2D(in_filePaths[2].c_str(), in_textureDesc)
		};
		Rendering::CubemapFace faceNegY = {
			Rendering::TEXTURE_TARGET::CUBEMAP_FACE_NEGATIVE_Y,
			CreateTexture2D(in_filePaths[3].c_str(), in_textureDesc)
		};
		Rendering::CubemapFace facePosZ = {
			Rendering::TEXTURE_TARGET::CUBEMAP_FACE_POSITIVE_Z,
			CreateTexture2D(in_filePaths[4].c_str(), in_textureDesc)
		};
		Rendering::CubemapFace faceNegZ = {
			Rendering::TEXTURE_TARGET::CUBEMAP_FACE_NEGATIVE_Z,
			CreateTexture2D(in_filePaths[5].c_str(), in_textureDesc)
		};

		return new GL_Cubemap({ facePosX, faceNegX, facePosY, faceNegY, facePosZ, faceNegZ });
	}

	Mesh * GLRenderDevice::CreateMesh(const std::vector<Vertex>& in_vertices,
		const std::vector<uint_32>& in_indices, DataStructures::AABB *in_aabb, const DRAW_MODE in_mode)
	{
		return new GLMesh(in_vertices, in_indices, in_aabb, in_mode);
	}

	Graphics::Shader* GLRenderDevice::CreateShader(const Graphics::SHADER_TYPE in_type, const char* in_fileName)
	{
		return new Graphics::GLShader(in_fileName, in_type);
	}

	RenderTarget* GLRenderDevice::CreateRenderTarget(const RenderTarget::BUFFER_TYPE in_type)
	{
		return new GLRenderTarget(in_type);
	}

	Graphics::ShaderProgram* GLRenderDevice::CreateShaderProgram(const char* in_vertexName, const char* in_pixelName)
	{
		auto vertexShader = CreateShader(Graphics::SHADER_TYPE::VERTEX_SHADER, in_vertexName);
		auto pixelShader = CreateShader(Graphics::SHADER_TYPE::PIXEL_SHADER, in_pixelName);
		return CreateShaderProgram(vertexShader, pixelShader);
	}

	Graphics::ShaderProgram* GLRenderDevice::CreateShaderProgram(Graphics::Shader* in_vertexShader, Graphics::Shader* in_pixelShader)
	{
		return new Graphics::GLShaderProgram(reinterpret_cast<Graphics::GLShader*>(in_vertexShader), reinterpret_cast<Graphics::GLShader*>(in_pixelShader));
	}
}
