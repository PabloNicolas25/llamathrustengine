#pragma once

#include "core/Core.hpp"
#include "core/graphics/Shader.hpp"

namespace Llamathrust
{
	namespace Graphics
	{

		class LLAMATHRUST_API GLShader : public Shader
		{
			uint_32 m_id;

		public:
			GLShader(const char* in_fileName, const SHADER_TYPE in_type);
			~GLShader();

			void* Get() override;
		};
	}
}