#pragma once
#include "core/Core.hpp"
#include <vector>
#include <glm/glm.hpp>
#include "core/graphics/ShaderProgram.hpp"
#include "GL_Shader.hpp"

namespace Llamathrust
{
	namespace Graphics
	{
		class LLAMATHRUST_API GLShaderProgram : public ShaderProgram
		{
			uint_32 m_programID;
			GLShader* m_vertexShader;
			GLShader* m_pixelShader;

		public:
			GLShaderProgram(GLShader* in_vertexShader, GLShader* in_pixelShader);
			~GLShaderProgram();
			
			void Create() override;
			void Use() override;

			void SetInt(const char* in_uniformName, const int in_value) override;
			void SetFloat(const char* in_uniformName, const float in_value) override;
			void SetVec3(const char* in_uniformName, const glm::vec3& in_vec) override;
			void SetVec4(const char* in_uniformName, const glm::vec4& in_vec) override;
			void SetMat4(const char* in_uniformName, const glm::mat4& in_mat) override;
			void SetMat4vec(const char* in_uniformName, const std::vector<glm::mat4>& in_matVec) override;

			uint_32 Get() { return m_programID; }
		};
	}
}