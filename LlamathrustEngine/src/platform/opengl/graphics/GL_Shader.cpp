#include "ltpch.hpp"
#include "GL_Shader.hpp"
#include <glad/glad.h>
#include <fstream>

static void shaderCompileStatus(uint_32 shader)
{
	int  success;
	char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shader, 512, 0, infoLog);
		LT_ERROR("ERROR: Shader compilation failed.\n {0} \n", infoLog);
	}
}

static std::string getShaderSrc(const char* fileName)
{
	std::ifstream file;
	file.open(fileName);

	std::string output;
	std::string line;

	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
	{
		LT_ERROR("Unable to load shader: {0}\n", fileName);
	}

	return output;
}

static GLenum getShaderType(const Llamathrust::Graphics::SHADER_TYPE in_type)
{
	switch (in_type)
	{
	case Llamathrust::Graphics::SHADER_TYPE::PIXEL_SHADER:
		return GL_FRAGMENT_SHADER;
	case Llamathrust::Graphics::SHADER_TYPE::VERTEX_SHADER:
	default:
		return GL_VERTEX_SHADER;
	}
}

namespace Llamathrust::Graphics
{
	GLShader::GLShader(const char* in_fileName, const SHADER_TYPE in_type) : Shader(in_type)
	{
		std::string path;
		
		switch (in_type)
		{
		case SHADER_TYPE::PIXEL_SHADER:
			path = std::string(in_fileName) + ".frag"; break;
		case SHADER_TYPE::VERTEX_SHADER:
			path = std::string(in_fileName) + ".vert"; break;
		}

		// Get the shader sources and compile them
		auto stringSource = getShaderSrc(path.c_str());
		auto shaderSource = stringSource.c_str();
		m_id = glCreateShader(getShaderType(in_type));
		glShaderSource(m_id, 1, &shaderSource, NULL);
		glCompileShader(m_id);
		shaderCompileStatus(m_id);
	}

	GLShader::~GLShader()
	{
		glDeleteShader(m_id);
	}

	void* GLShader::Get()
	{
		return reinterpret_cast<void*>(m_id);
	}
}
