#include "ltpch.hpp"
#include "GL_ShaderProgram.hpp"
#include <glad/glad.h>

namespace Llamathrust::Graphics
{
	GLShaderProgram::GLShaderProgram(GLShader* in_vertexShader, GLShader* in_pixelShader)
		: m_vertexShader(in_vertexShader), m_pixelShader(in_pixelShader)
	{
		
	}

	GLShaderProgram::~GLShaderProgram()
	{
		glDeleteProgram(m_programID);
	}

	void GLShaderProgram::Create() {
		m_programID = glCreateProgram();

		glAttachShader(m_programID, (uint_32)m_vertexShader->Get());
		glAttachShader(m_programID, (uint_32)m_pixelShader->Get());

		glLinkProgram(m_programID);

		// check for linking errors
		int  success;
		char infoLog[512];
		glGetProgramiv(m_programID, GL_LINK_STATUS, &success);
		if (!success) {
			glGetProgramInfoLog(m_programID, 512, 0, infoLog);
			LT_ERROR("ERROR: Shader program linking failed.\n{0}\n", infoLog);
		}

		glDetachShader(m_programID, (uint_32)m_vertexShader->Get());
		glDetachShader(m_programID, (uint_32)m_pixelShader->Get());
	}
	void GLShaderProgram::Use()
	{
		glUseProgram(m_programID);
	}
	
	void GLShaderProgram::SetInt(const char* in_uniformName, const int in_value)
	{
		uint_32 location = glGetUniformLocation(m_programID, in_uniformName);
		glUniform1i(location, in_value);
	}
	void GLShaderProgram::SetFloat(const char* in_uniformName, const float in_value)
	{
		uint_32 location = glGetUniformLocation(m_programID, in_uniformName);
		glUniform1f(location, in_value);
	}
	void GLShaderProgram::SetVec3(const char* in_uniformName, const glm::vec3& in_vec)
	{
		uint_32 location = glGetUniformLocation(m_programID, in_uniformName);
		glUniform3f(location, in_vec[0], in_vec[1], in_vec[2]);
	}
	void GLShaderProgram::SetVec4(const char* in_uniformName, const glm::vec4& in_vec)
	{
		uint_32 location = glGetUniformLocation(m_programID, in_uniformName);
		glUniform4f(location, in_vec[0], in_vec[1], in_vec[2], in_vec[3]);
	}
	void GLShaderProgram::SetMat4(const char* in_uniformName, const glm::mat4& in_mat)
	{
		uint_32 location = glGetUniformLocation(m_programID, in_uniformName);
		glUniformMatrix4fv(location, 1, GL_FALSE, &in_mat[0][0]);
	}
	void GLShaderProgram::SetMat4vec(const char* in_uniformName, const std::vector<glm::mat4>& in_matVec)
	{
		uint_32 location = glGetUniformLocation(m_programID, in_uniformName);
		glUniformMatrix4fv(location, (GLsizei) in_matVec.size(), GL_FALSE, &in_matVec[0][0][0]);
	}
}
