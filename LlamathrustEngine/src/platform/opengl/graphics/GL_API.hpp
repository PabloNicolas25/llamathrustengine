#pragma once

#include "core/Core.hpp"
#include "core/graphics/GraphicsAPI.hpp"
#include <glad/glad.h>

namespace Llamathrust
{
	namespace Graphics
	{
		class LLAMATHRUST_API OpenGLAPI : public RenderAPI
		{
		public:
			bool setUpAPI(int in_width, int in_height) override;

			void setViewport(Viewport* in_viewport) override;

			void clearScreenBuffer(const BUFFERTYPESFLAGS in_buffersFlags) override;

			void setClearColor(const Color& in_color) override;

			void enableDepthTesting(const bool in_enable) override;

			void enableCullFace(const CULL_FACES in_face) override;

			void enableWireframeMode(const bool in_wireframeMode) override;

			void enableBlending(const bool in_enable) override;
		};
	}
}