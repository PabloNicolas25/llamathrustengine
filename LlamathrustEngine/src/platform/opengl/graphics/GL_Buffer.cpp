#include "ltpch.hpp"
#include "GL_Buffer.hpp"

namespace Llamathrust::Graphics
{
	static GLenum GLComponentType(SHADER_DATA_TYPE in_type)
	{
		switch (in_type)
		{
		case Llamathrust::Graphics::Float:
		case Llamathrust::Graphics::Float2:
		case Llamathrust::Graphics::Float3:
		case Llamathrust::Graphics::Float4:
		case Llamathrust::Graphics::Mat3:
		case Llamathrust::Graphics::Mat4:
			return GL_FLOAT;
		case Llamathrust::Graphics::Int:
		case Llamathrust::Graphics::Int2:
		case Llamathrust::Graphics::Int3:
		case Llamathrust::Graphics::Int4:
			return GL_INT;
		case Llamathrust::Graphics::Bool:
			return GL_BOOL;
		}
		LT_CORE_ASSERT(false, "Unknown type!");
		return 0;
	}
	//////////////////////////////////////
	//	INDEX BUFFER
	//////////////////////////////////////

	GLIndexBuffer::GLIndexBuffer(const uint_32* in_data, const uint_32 in_count)
	{
		glGenBuffers(1, &m_buffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer);

		glBufferData(GL_ELEMENT_ARRAY_BUFFER, in_count * sizeof(uint_32), in_data, GL_STATIC_DRAW);
	}
	GLIndexBuffer::~GLIndexBuffer()
	{
		glDeleteBuffers(1, &m_buffer);
	}
	void GLIndexBuffer::Bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer);
	}

	//////////////////////////////////////
	//	VERTEX BUFFER
	//////////////////////////////////////

	GLVertexBuffer::GLVertexBuffer(const float* in_data, const uint_32 in_count, const BufferLayout& in_layout)
	{
		glGenBuffers(1, &m_buffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_buffer);

		glBufferData(GL_ARRAY_BUFFER, in_count * sizeof(float), in_data, GL_STATIC_DRAW);

		uint_32 index = 0;
		for (const auto& element : in_layout)
		{
			glEnableVertexAttribArray(index);
			glVertexAttribPointer(index,
				element.GetComponentCount(),
				GLComponentType(element.type),
				element.normalized,
				(GLsizei) in_layout.GetStride(),
				(void*)element.offset);
			index++;
		}
	}
	GLVertexBuffer::~GLVertexBuffer()
	{
		glDeleteBuffers(1, &m_buffer);
	}
	void GLVertexBuffer::Bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
	}
}
