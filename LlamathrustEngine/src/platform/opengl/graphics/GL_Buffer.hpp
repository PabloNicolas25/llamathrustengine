#pragma once

#include "core/Core.hpp"
#include "core/graphics/GBuffer.hpp"
#include <glad/glad.h>

namespace Llamathrust
{
	namespace Graphics
	{
		class LLAMATHRUST_API GLIndexBuffer : public IndexBuffer
		{
			GLuint m_buffer;

		public:
			GLIndexBuffer(const uint_32* in_data, const uint_32 in_count);
			~GLIndexBuffer();

			void Bind() const override;
		};

		class LLAMATHRUST_API GLVertexBuffer : public VertexBuffer
		{
			GLuint m_buffer;

		public:
			GLVertexBuffer(const float* in_data, const uint_32 in_count, const BufferLayout& in_layout);
			~GLVertexBuffer();

			void Bind() const override;
		};
	}
}