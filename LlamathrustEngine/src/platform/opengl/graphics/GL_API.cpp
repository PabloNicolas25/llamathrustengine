#include "ltpch.hpp"
#include "GL_API.hpp"
#include "../rendering/GLRenderDevice.hpp"
#include <GLFW/glfw3.h>

static void OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity,
	GLsizei length, const GLchar *message, const void *data)
{
	if (type == GL_DEBUG_TYPE_OTHER)
		return;

	std::string callback_string = "\n---------------------opengl-callback-start------------\n";
	callback_string += "message: {0}\ntype: ";

	switch (type) {
	case GL_DEBUG_TYPE_ERROR:
		callback_string += "ERROR\n";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		callback_string += "DEPRECATED_BEHAVIOR\n";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		callback_string += "UNDEFINED_BEHAVIOR\n";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		callback_string += "PORTABILITY\n";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		callback_string += "PERFORMANCE\n";
		break;
	case GL_DEBUG_TYPE_OTHER:
		callback_string += "OTHER\n";
		break;
	}

	callback_string += "id: {1}\n";
	callback_string += "severity: ";

	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:
		callback_string += "LOW";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		callback_string += "MEDIUM";
		break;
	case GL_DEBUG_SEVERITY_HIGH:
		callback_string += "HIGH";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		callback_string += "NOTE";
		break;
	}
	callback_string += "\n---------------------opengl-callback-end--------------\n";

	LT_CORE_WARN(callback_string.c_str(), (const char*)message, id);
}


namespace Llamathrust::Graphics
{
	bool OpenGLAPI::setUpAPI(int in_width, int in_height)
	{
		// Init GLAD (opengl functions)
		int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
		LT_CORE_ASSERT(status, "Failed to intialize GLAD!");
		LT_CORE_INFO("GLAD INTIALIZED!");

		if (status)
			new Rendering::GLRenderDevice();

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#ifdef LT_DEBUG
		// set opengldebugmessage callback
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(OpenGLDebugCallback, NULL);
		LT_CORE_INFO("Context Info:");
		LT_CORE_INFO("\tGraphics Card: {0} {1}", glGetString(GL_VENDOR), glGetString(GL_RENDERER));
		LT_CORE_INFO("\tOpenGL Version: {0}", glGetString(GL_VERSION));
#endif
		Viewport viewport = { 0, 0, in_width, in_height };
		setViewport(&viewport);

		return true;
	}
	
	void OpenGLAPI::setViewport(Viewport * in_viewport)
	{
		m_viewport = *in_viewport;
		glViewport(
			(GLint)in_viewport->TopLeftX,
			(GLint)in_viewport->TopLeftY,
			(GLsizei)in_viewport->Width,
			(GLsizei)in_viewport->Height);
	}
	
	void OpenGLAPI::clearScreenBuffer(const BUFFERTYPESFLAGS in_buffersFlags)
	{
		//TODO improve
		GLint mask = 0;
		if (in_buffersFlags & BUFFERTYPESFLAGS::COLOR_BUFFER)
			mask |= GL_COLOR_BUFFER_BIT;
		if (in_buffersFlags & BUFFERTYPESFLAGS::DEPTH_BUFFER)
			mask |= GL_DEPTH_BUFFER_BIT;
		if (in_buffersFlags & BUFFERTYPESFLAGS::STENCIL_BUFFER)
			mask |= GL_STENCIL_BUFFER_BIT;
		glClear(mask);
	}
	
	void OpenGLAPI::setClearColor(const Color & in_color)
	{
		m_backgroundColor = in_color;
		glClearColor(in_color.red, in_color.green, in_color.blue, in_color.alpha);
	}
	
	void OpenGLAPI::enableDepthTesting(const bool in_enable)
	{
		if (in_enable)
		{
			glEnable(GL_DEPTH_TEST);
		}
		else
		{
			glDisable(GL_DEPTH_TEST);
		}
	}
	
	void OpenGLAPI::enableCullFace(const CULL_FACES in_face)
	{
		if (in_face == CULL_FACES::NONE)
			glDisable(GL_CULL_FACE);
		else
		{
			glEnable(GL_CULL_FACE);
			if (in_face == CULL_FACES::BACK_FACE)
				glCullFace(GL_BACK);
			else
				glCullFace(GL_FRONT);
		}
	}
	void OpenGLAPI::enableWireframeMode(const bool in_wireframeMode)
	{
		if (in_wireframeMode)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	void OpenGLAPI::enableBlending(const bool in_enable)
	{
		switch (in_enable)
		{
		case true:
			glEnable(GL_BLEND);
			break;
		case false:
			glDisable(GL_BLEND);
			break;
		}
	}
}