#pragma once
#include "core/Window.hpp"
#include <GLFW/glfw3.h>

namespace Llamathrust
{
	/*
		Windows window implementation
	*/
	class WindowsWindow : public Window
	{
		/*
			Window data as UserData pointer for glfw
		*/
		struct WindowData
		{
			std::string Title;
			unsigned int Width, Height;
			bool VSync;

			EventCallbackFn EventCallback;
		};
		
		WindowData m_Data;
		GLFWwindow* m_Window;

		// helper method for intializing
		virtual void Init(const WindowProps& props);
		// helper method for destroying
		virtual void Shutdown();
	public:
		WindowsWindow(const WindowProps& props);
		virtual ~WindowsWindow();

		void OnUpdate() override;

		inline unsigned int GetWidth() const override { return m_Data.Width; }
		inline unsigned int GetHeight() const override { return m_Data.Height; }

		// Window attributes
		inline void SetEventCallback(const EventCallbackFn& callback) override { m_Data.EventCallback = callback; }
		inline void* GetNativeWindow() const override { return m_Window; }
		void SetVSync(bool enabled) override;
		bool IsVSync() const override;
	};
}

