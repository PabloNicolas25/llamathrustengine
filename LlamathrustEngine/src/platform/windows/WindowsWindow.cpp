#include "ltpch.hpp"
#include "WindowsWindow.hpp"
#include "core/Log.hpp"
#include "core/events/ApplicationEvent.hpp"
#include "core/events/KeyEvent.hpp"
#include "core/events/MouseEvent.hpp"
#include "platform/opengl/graphics/GL_API.hpp"
#include <glad/glad.h>
#ifdef LT_PLATFORM_WINDOWS
//#include "platform/directx/graphics/DirectXAPI.hpp"
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>
#endif

namespace Llamathrust
{

	static bool s_GLFWInitialized = false;

	static void GLFWErrorCallback(int error, const char* description)
	{
		LT_CORE_ERROR("GLFW Error ({0}): {1}", error, description);
	}


	Window* Window::Create(const WindowProps& props)
	{
		return new WindowsWindow(props);
	}

	WindowsWindow::WindowsWindow(const WindowProps& props)
	{
		Init(props);
	}

	WindowsWindow::~WindowsWindow()
	{
		Shutdown();
	}

	void WindowsWindow::Init(const WindowProps & props)
	{
		m_Data.Title  = props.Title;
		m_Data.Height = props.Height;
		m_Data.Width  = props.Width;

		LT_CORE_INFO("Creating window {0} ({1}, {2})", props.Title, props.Width, props.Height);

		// If first time a window is requested init glfw and set error callback
		if (!s_GLFWInitialized)
		{
			// TODO: glfwTerminate on system shutdown
			int success = glfwInit();
			LT_CORE_ASSERT(success, "Could not intialize GLFW!");
			glfwSetErrorCallback(GLFWErrorCallback);
			s_GLFWInitialized = true;
		}

#ifdef LT_DEBUG
	#ifdef LT_OPENGL
		m_api = RENDER_DRIVER::OPENGL4;
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	#endif
#endif

#ifdef LT_DIRECTX
		m_api = RENDER_DRIVER::DIRECTX11;
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
#endif

		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
		glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
		glfwWindowHint(GLFW_FOCUSED, GLFW_TRUE);
		// Create window
		m_Window = glfwCreateWindow((int)props.Width, (int)props.Height, m_Data.Title.c_str(), nullptr, nullptr);

#ifdef LT_OPENGL
		// Make the current window the current opengl context
		glfwMakeContextCurrent(m_Window);
		m_videoDriver = new Graphics::OpenGLAPI();
#endif

#ifdef LT_DIRECTX
	#ifdef LT_PLATFORM_WINDOWS
		// Win32 window handler (yeeey glfw did it for us)
		HWND window;
		window = glfwGetWin32Window(m_Window);
		m_videoDriver = new Graphics::DirectXAPI(window);
	#else
	#error "DirectX no supported in this platform!"
	#endif
#endif

		bool success = (m_videoDriver != nullptr) ? m_videoDriver->setUpAPI(props.Width, props.Height) : false;
		if (!success)
			exit(-1);
		
		// Pass the user data to glfw
		glfwSetWindowUserPointer(m_Window, &m_Data);
		SetVSync(false);

		// Set GLFW callbacks
		glfwSetWindowSizeCallback(m_Window, [](GLFWwindow* window, int width, int height)
		{
			WindowData& data = *(WindowData*) glfwGetWindowUserPointer(window);
			data.Width = width;
			data.Height = height;

			WindowResizeEvent e(width, height);
			data.EventCallback(e);
		});

		glfwSetWindowCloseCallback(m_Window, [](GLFWwindow* window)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			WindowCloseEvent e;
			data.EventCallback(e);
		});


		glfwSetKeyCallback(m_Window, [](GLFWwindow* window, int key, int scancode, int action, int modifier)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			switch (action)
			{
				case GLFW_PRESS:
				{
					KeyPressedEvent e(key, 0);
					data.EventCallback(e);
					break;
				}
				case GLFW_REPEAT:
				{
					KeyPressedEvent e(key, 1);
					data.EventCallback(e);
					break;
				}
				case GLFW_RELEASE:
				{
					KeyReleasedEvent e(key);
					data.EventCallback(e);
					break;
				}
			}
		});

		glfwSetCharCallback(m_Window, [](GLFWwindow* window, unsigned int character)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			KeyTypedEvent e(character);
			data.EventCallback(e);
		});

		glfwSetMouseButtonCallback(m_Window, [](GLFWwindow* window, int button, int action, int modifier)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			switch (action)
			{
				case GLFW_PRESS:
				{
					MouseButtonPressedEvent e(button, 0);
					data.EventCallback(e);
					break;
				}
				case GLFW_REPEAT:
				{
					MouseButtonPressedEvent e(button, 1);
					data.EventCallback(e);
					break;
				}
				case GLFW_RELEASE:
				{
					MouseButtonReleasedEvent e(button);
					data.EventCallback(e);
					break;
				}
			}
		});

		glfwSetScrollCallback(m_Window, [](GLFWwindow* window, double xOffset, double yOffset)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			MouseScrolledEvent event((float)xOffset, (float)yOffset);
			data.EventCallback(event);
		});

		glfwSetCursorPosCallback(m_Window, [](GLFWwindow* window, double xPos, double yPos)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			MouseMovedEvent event((float)xPos, (float)yPos);
			data.EventCallback(event);
		});
	}

	void WindowsWindow::Shutdown()
	{
		glfwDestroyWindow(m_Window);
	}

	void WindowsWindow::OnUpdate()
	{
#ifdef LT_PLATFORM_WINDOWS
		/*
		if (m_api == RENDER_DRIVER::DIRECTX11)
			static_cast<Graphics::DirectXAPI*>(m_videoDriver)->m_swapChain->Present(0, NULL);
		else
		*/
#endif
			glfwSwapBuffers(m_Window);
		glfwPollEvents();
	}

	void WindowsWindow::SetVSync(bool enabled)
	{
		if (enabled)
			glfwSwapInterval(1);
		else
			glfwSwapInterval(0);

		m_Data.VSync = enabled;
	}

	bool WindowsWindow::IsVSync() const
	{
		return m_Data.VSync;
	}
}