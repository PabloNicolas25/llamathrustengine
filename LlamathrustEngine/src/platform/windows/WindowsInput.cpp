#include "ltpch.hpp"
#include "WindowsInput.hpp"

#include "core/Application.hpp"
#include <GLFW/glfw3.h>

namespace Llamathrust
{
	Input* Input::s_Instance = new WindowsInput();

	bool WindowsInput::IsKeyPressedImpl(int keycode)
	{
		auto window = (GLFWwindow*) Application::Get().GetWindow()->GetNativeWindow();
		auto state = glfwGetKey(window, keycode);

		return state == GLFW_PRESS || state == GLFW_REPEAT;
	}
	
	bool WindowsInput::IsMouseButtonPressedImpl(int button)
	{
		auto window = (GLFWwindow*)Application::Get().GetWindow()->GetNativeWindow();
		auto state = glfwGetMouseButton(window, button);
		return state == GLFW_PRESS;
	}

	float WindowsInput::GetMouseXImpl()
	{
		auto window = (GLFWwindow*)Application::Get().GetWindow()->GetNativeWindow();
		double xPos, yPos;
		glfwGetCursorPos(window, &xPos, &yPos);
		return (float) xPos;
	}

	float WindowsInput::GetMouseYImpl()
	{
		auto window = (GLFWwindow*)Application::Get().GetWindow()->GetNativeWindow();
		double xPos, yPos;
		glfwGetCursorPos(window, &xPos, &yPos);
		return (float) yPos;
	}
	glm::vec2 WindowsInput::GetMousePositionImpl()
	{
		auto window = (GLFWwindow*)Application::Get().GetWindow()->GetNativeWindow();
		double xPos, yPos;
		glfwGetCursorPos(window, &xPos, &yPos);
		return glm::vec2(xPos, yPos);
	}
}
