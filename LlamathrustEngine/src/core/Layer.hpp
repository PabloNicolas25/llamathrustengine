#pragma once
#include "core/Core.hpp"
#include "core/events/Event.hpp"
#include "core/graphics/GraphicsAPI.hpp"
#include <string>


namespace Llamathrust
{
	class LLAMATHRUST_API Application;

	class LLAMATHRUST_API Layer
	{
	public:
		Layer(const std::string& name = "Layer");
		virtual ~Layer() = default;

		virtual void OnAttach() {}
		virtual void OnDetach() {}
		virtual void OnUpdate(float deltaTime) {}
		virtual void OnRender() {}
		virtual void OnImGuiRender() {}
		virtual void OnEvent(Event& event) {}

		inline const std::string& GetName() const { return m_DebugName; }
	protected:
		Application& m_app;
#pragma warning(suppress: 4251)
		std::string m_DebugName;
	};
}