#pragma once
#include "Application.hpp"
#include "Log.hpp"


#ifdef LT_PLATFORM_WINDOWS

extern Llamathrust::Application* Llamathrust::CreateApplication();

int main(int argc, char** argv)
{
	// Dont forget to initalize the class!!!
	Llamathrust::Log::Init();

	// Initialize the application
	auto app = Llamathrust::CreateApplication();
	// Run the app
	app->Run();
	// Free memory
	delete app;

	return EXIT_SUCCESS;
}

#endif


/*
 *	Macro to make the api easier
 *	it makes the implementation of the 'Llamathrust::CreateApplication' function automatic
 */
#define LLAMATHRUST_APPCLASS(Class) Llamathrust::Application* Llamathrust::CreateApplication() { \
	return new Class();\
}