#pragma once
#include "Core.hpp"
#include <memory>
#include <spdlog/spdlog.h>

namespace Llamathrust {
	/*
	Multithreaded logger
	*/
#pragma warning(push)
#pragma warning(disable: 4251)
	class LLAMATHRUST_API Log
	{
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
	public:
		static void Init();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
	};
#pragma warning(pop)
}


// Core log macros
#define LT_CORE_TRACE(...)	::Llamathrust::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define LT_CORE_INFO(...)	::Llamathrust::Log::GetCoreLogger()->info(__VA_ARGS__)
#define LT_CORE_WARN(...)	::Llamathrust::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define LT_CORE_ERROR(...)	::Llamathrust::Log::GetCoreLogger()->error(__VA_ARGS__)
#define LT_CORE_FATAL(...)	::Llamathrust::Log::GetCoreLogger()->fatal(__VA_ARGS__)

// Client log macros
#define LT_TRACE(...)		::Llamathrust::Log::GetClientLogger()->trace(__VA_ARGS__)
#define LT_INFO(...)		::Llamathrust::Log::GetClientLogger()->info(__VA_ARGS__)
#define LT_WARN(...)		::Llamathrust::Log::GetClientLogger()->warn(__VA_ARGS__)
#define LT_ERROR(...)		::Llamathrust::Log::GetClientLogger()->error(__VA_ARGS__)
#define LT_FATAL(...)		::Llamathrust::Log::GetClientLogger()->fatal(__VA_ARGS__)