#include "ltpch.hpp"
#include "DrawCallListener.hpp"

namespace Llamathrust
{
#ifdef LT_DEBUG
	DrawCallListener* DrawCallListener::m_instance = new DrawCallListener();
#else
	DrawCallListener* DrawCallListener::m_instance = nullptr;
#endif

	DrawCallListener* DrawCallListener::Get()
	{
		return m_instance;
	}

	void DrawCallListener::Notify()
	{
		m_count++;
	}
	void DrawCallListener::Reset()
	{
		m_count = 0;
	}
	uint_32 DrawCallListener::GetShaderChangesCount()
	{
		return m_count;
	}
}