#pragma once

#include "core/Core.hpp"
#include "Listener.hpp"

namespace Llamathrust
{
	class LLAMATHRUST_API DrawCallListener : public Listener
	{
		uint_32 m_count = 0;
	public:
		static DrawCallListener* Get();

		void Notify() override;

		void Reset() override;

		uint_32 GetShaderChangesCount();
	
	protected:
		DrawCallListener() = default;
		static DrawCallListener* m_instance;
	};
}
