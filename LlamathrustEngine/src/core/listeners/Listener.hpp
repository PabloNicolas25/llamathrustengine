#pragma once

#include "core/Core.hpp"

namespace Llamathrust
{
	class LLAMATHRUST_API Listener
	{
	public:
		virtual void Notify() = 0;
		virtual void Reset() = 0;
	};
}

