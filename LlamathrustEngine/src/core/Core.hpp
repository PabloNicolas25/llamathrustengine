#pragma once

#define LT_OPENGL
//#define LT_DIRECTX

#ifdef LT_PLATFORM_WINDOWS
	#ifdef LT_DYNAMIC_LINK
		#ifdef LT_BUILD_DLL
			#define LLAMATHRUST_API __declspec(dllexport)
		#else
			#define LLAMATHRUST_API __declspec(dllimport)
		#endif
	#else
		#define LLAMATHRUST_API
	#endif
#else
	#error Llamathrust only spport Windows!
#endif // LT_PLATFORM_WINDOWS

#ifdef  LT_ENABLE_ASSERTS
	#define LT_ASSERT(x, ...) {\
		if(!(x)) {\
			LT_ERROR("Assertion Failed: {0}", __VA_ARGS__);\
			__debugbreak();\
	} }
	
	#define LT_CORE_ASSERT(x, ...) {\
		 if(!(x)) {\
			LT_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__);\
			__debugbreak();\
	} }
#else
	#define LT_ASSERT(x, ...)
	#define LT_CORE_ASSERT(x, ...)
#endif


#define BIT(x) (1 << x)

#define LT_BIND_EVENT_FN(x) std::bind(&x, this, std::placeholders::_1)

#define LT_OR_OPERATOR_OVERLOAD(x)\
inline const x operator | (const x lhs, const x rhs)\
{\
using T = std::underlying_type_t <x>;\
return static_cast<x>(static_cast<T>(lhs) | static_cast<T>(rhs));\
}\
inline const x& operator |= (x& lhs, const x rhs)\
{\
	lhs = lhs | rhs;\
	return lhs;\
}\

// TYPEDEFs
typedef unsigned char byte;
typedef byte uint_8;
typedef unsigned int uint_32;
typedef unsigned long long int uint_64;
typedef uint_64 lt_size;