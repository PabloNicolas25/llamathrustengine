#pragma once
#include <vector>
#define GLM_ENABLE_EXPERIMENTAL
#include <GLM/glm.hpp>
#include <GLM/gtx/quaternion.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include "mud_importer.hpp"

#define MAX_INDICES 384000 //384K * 4B = 512KB :: 384K = 128K Tris
#define MAX_VERTS MAX_INDICES * 3 // This means every face has own vertices

namespace MUDLoader
{

	//###################################################################
	//				MATH DEFINES
	//###################################################################
	typedef glm::mat4 mat4;
	typedef glm::vec2 vec2;
	typedef glm::vec3 vec3;
	typedef glm::vec4 vec4;
	typedef glm::uvec4 vec4u;
	typedef glm::quat quat;
	typedef float decimal;
	typedef unsigned char byte;

	//###################################################################
	//				BINARY FILE ENUM
	//###################################################################

	enum MUD_FILE_MASKS
	{
		HAS_AABB = 1,
		HAS_UVS = 1 << 1,
		HAS_NORMALS = 1 << 2
	};

	
	//###################################################################
	//				SKELETON
	//###################################################################

	template<class A, class B, class C>
	struct tuple
	{
		A parentID;
		B first;
		C second;
	};

	#define MAX_VERTEX_BONES 4	// Max number of bones per vertex
	#define PRECISION 12
	
	struct Bone
	{
		int id;
		Bone* parent;
		mat4 bindOffset;	// Original offset relative to the parent
		mat4 inverseBindOffset;
		std::vector<Bone*> children;
		const char* debugName;
	};

	//###################################################################
	//				VERTEX, MESH AND MODEL
	//###################################################################

	struct Vertex
	{
		vec3 pos;
		vec3 normal = vec3(0);
		vec2 uvCoord = vec2(0);
		vec4u indices = vec4u(0); //Bones indices. Up to 4 bones
		vec4 weights = vec4(0);	//Bones weight. 1 per bone (4 total)
	};

	struct AABB
	{
		vec3 max_extent = vec3(0, 0, 0);
		vec3 min_extent = vec3(0, 0, 0);

		vec3 m_max_extent = vec3(0, 0, 0);
		vec3 m_min_extent = vec3(0, 0, 0);
	};

	struct Mesh
	{
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		AABB aabb;
		bool hasAABB = false;
	};

	struct Model
	{
		std::vector<Mesh> meshes;
		Bone* skeleton = nullptr;
		std::vector<tuple<int, mat4*, mat4*>> bindTransforms;
	};

	// Load from ASCII file
	void LoadASCII(const char* in_filePath, Model** out_model);

	// Load from binary file
	void LoadBinary(const char* in_filePath, Model** out_model);


	// Load any file (ASCII or Bin)
	void Load(const char* in_filePath, Model** out_model);

	// Save the model in binary file
	void CookBinary(const char* in_filePath, const char* in_bytes);
}