#include "ltpch.hpp"
#include "tinyxml2.h"
#include "mud_importer.hpp"
#include <fstream>	// Files
#include <sstream>	// String Stream
#include <iostream> // Input Output
#include <algorithm> // Sort

#define CHECK_ERROR(x)\
if (x.bad())\
{\
std::cout << "READ ERROR! "<< std::endl;\
return true;\
}

#define CHECK_INCOMPLETE(x, y)\
if (x.eof())\
{\
	std::cout << "FILE " <<  y << " INCOMPLETE" << std::endl;\
	return true;\
}





//###################################################################
//				MATH FUNCTIONS
//###################################################################
static void quaternionToMatrix(MUDLoader::quat& q, MUDLoader::mat4& mat)
{
	mat = glm::toMat4(q);
}

static void translationToMatrix(MUDLoader::vec3& vec, MUDLoader::mat4& mat)
{
	using namespace MUDLoader;
	mat4 location;
	location = glm::translate(glm::mat4(1.0f), vec);
	mat = location * mat;
}

static MUDLoader::mat4 inverseMat4(const MUDLoader::mat4& matrix)
{
	return glm::inverse(matrix);
}

//###################################################################
//				HELPERS
//###################################################################
static void helperStrToVec3(const char* charArray, MUDLoader::vec3& vec)
{
	std::string string(charArray);
	
	size_t first = string.find_first_of(", ");

	auto sub = string.substr(0, PRECISION);

	// X
	{
		size_t first = sub.find_first_of(", ");
		if (first != std::string::npos)
			sub = sub.replace(first, std::string::npos, "");
		vec.x = (MUDLoader::decimal) atof(sub.c_str());
	}

	// Y
	{
		first = string.find(", ", first) + 2;
		sub = string.substr(first, PRECISION);

		size_t first = sub.find_first_of(", ");
		if (first != std::string::npos)
			sub = sub.replace(first, std::string::npos, "");

		vec.y = (MUDLoader::decimal) atof(sub.c_str());
	}

	// Z
	{
		first = string.find(", ", first) + 2;
		sub = string.substr(first, PRECISION);

		vec.z = (MUDLoader::decimal) atof(sub.c_str());
	}
}

static void helperStrToQuat(const char* charArray, MUDLoader::quat& vec)
{
	std::string string(charArray);

	size_t first = string.find_first_of(", ");

	auto sub = string.substr(0, PRECISION);

	// X
	{
		size_t first = sub.find_first_of(", ");
		if (first != std::string::npos)
			sub = sub.replace(first, std::string::npos, "");
		vec.x = (MUDLoader::decimal) atof(sub.c_str());
	}

	// Y
	{
		first = string.find(", ", first) + 2;
		sub = string.substr(first, PRECISION);
		
		size_t first = sub.find_first_of(", ");
		if (first != std::string::npos)
			sub = sub.replace(first, std::string::npos, "");
		
		vec.y = (MUDLoader::decimal) atof(sub.c_str());
	}

	// Z
	{
		first = string.find(", ", first) + 2;
		sub = string.substr(first, PRECISION);
		
		
		size_t first = sub.find_first_of(", ");
		if (first != std::string::npos)
			sub = sub.replace(first, std::string::npos, "");

		vec.z = (MUDLoader::decimal) atof(sub.c_str());
	}

	// W
	{
		first = string.find(", ", first) + 2;
		sub = string.substr(first, PRECISION);

		vec.w = (MUDLoader::decimal) atof(sub.c_str());
	}
}

static MUDLoader::mat4 helperBuildInverseBindAbsolute(MUDLoader::Bone* bone)
{
	using namespace MUDLoader;

	int i = 0;
	mat4 transform = bone->bindOffset;

	for (Bone* parent = bone->parent; parent != nullptr; parent = parent->parent)
	{
		transform = parent->bindOffset * transform;
		i++;
	}

	return inverseMat4(transform);
}

static void helperBoneBuild(MUDLoader::Bone& bone, tinyxml2::XMLElement* boneNode)
{
	using namespace MUDLoader;

	auto translation = boneNode->FindAttribute("translation")->Value();
	auto rotation = boneNode->FindAttribute("rotation")->Value();
	auto name = boneNode->FindAttribute("name")->Value();
	auto id = boneNode->FindAttribute("id")->Value();
	bone.debugName = name;
	bone.id = atoi(id);

	vec3 translationVec = vec3();
	helperStrToVec3(translation, translationVec);

	quat rotationQuat;
	helperStrToQuat(rotation, rotationQuat);

	quaternionToMatrix(rotationQuat, bone.bindOffset);
	translationToMatrix(translationVec, bone.bindOffset);

	bone.inverseBindOffset = helperBuildInverseBindAbsolute(&bone);
}



void MUDLoader::Load(const char* in_filePath, Model** out_model)
{
	std::string string(in_filePath);
	if ('b' == string[string.size() - 1])
		LoadBinary(in_filePath, out_model);
	else
		LoadASCII(in_filePath, out_model);
}



void skeletonBuild(tinyxml2::XMLElement* node, MUDLoader::Bone* parent, std::vector<MUDLoader::Bone*>& array)
{
	for (auto siblingNode = node; siblingNode != nullptr; siblingNode = siblingNode->NextSiblingElement("bone"))
	{
		MUDLoader::Bone* boneTmp = new MUDLoader::Bone();
		
		// double linking
		parent->children.emplace_back(boneTmp);
		boneTmp->parent = parent;
		
		// bone data
		helperBoneBuild(*boneTmp, siblingNode);
		
		// Matrix array
		array.emplace_back(boneTmp);

		skeletonBuild(siblingNode->FirstChildElement("bone"), boneTmp, array);
	}
}

void MUDLoader::LoadASCII(const char * filePath, Model** model)
{
	tinyxml2::XMLDocument doc;
	auto loaded = doc.LoadFile(filePath);

	if (loaded == tinyxml2::XMLError::XML_ERROR_FILE_NOT_FOUND)
	{
		std::cout << "FILE NOT FOUND: " << filePath << std::endl;
		return;
	}

	tinyxml2::XMLElement* modelNode = doc.FirstChildElement("model");

	if (!modelNode)
	{
		std::cout << "FILE NOT A MUD MODEL: " << filePath << std::endl;
		return;
	}

	std::vector<Mesh> meshes;

	//Warnings
	bool normalWarn = false;
	bool uvWarn = false;

	// Meshes of the model
	for (auto meshNode = modelNode->FirstChildElement("mesh");
		meshNode != nullptr;
		meshNode = meshNode->NextSiblingElement("mesh"))
	{
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;

		// Mesh vertices
		for (auto vertexNode = meshNode->FirstChildElement("vertex");
			vertexNode != nullptr;
			vertexNode = vertexNode->NextSiblingElement("vertex"))
		{
			Vertex vertex;

			// Position
			auto text = vertexNode->FirstChildElement("position")->FirstAttribute()->Value();
			{
				std::stringstream string(text);
				string >> vertex.pos.x;
				string >> vertex.pos.y;
				string >> vertex.pos.z;
			}

			// Normal
			auto normalNode = vertexNode->FirstChildElement("normal");
			if (!normalNode)
			{
				if (!normalWarn)
				{
					LT_CORE_WARN("Model: {0}:\tHas no normals", filePath);
					normalWarn = !normalWarn;
				}
			}
			else
			{
				text = normalNode->FirstAttribute()->Value();
				{
					std::stringstream string(text);
					string >> vertex.normal.x;
					string >> vertex.normal.y;
					string >> vertex.normal.z;
				}
			}
			// UV Coord
			auto uvNode = vertexNode->FirstChildElement("uvcoord");
			if (!uvNode)
			{
				if (!uvWarn)
				{
					LT_CORE_WARN("Model: {0}:\tHas no UVs", filePath);
					uvWarn = !uvWarn;
				}
			}
			else
			{
				text = uvNode->FirstAttribute()->Value();
				{
					std::stringstream string(text);
					string >> vertex.uvCoord.x;
					string >> vertex.uvCoord.y;
				}
			}
			// Bone Indices
			auto hasIndices = vertexNode->FirstChildElement("indices");
			if (hasIndices)
			{
				text = vertexNode->FirstChildElement("indices")->FirstAttribute()->Value();
				{
					std::stringstream string(text);
					string >> vertex.indices.x;
					string >> vertex.indices.y;
					string >> vertex.indices.z;
					string >> vertex.indices.w;
				}
				// Bone Weights
				text = vertexNode->FirstChildElement("weights")->FirstAttribute()->Value();
				{
					std::stringstream string(text);
					string >> vertex.weights.x;
					string >> vertex.weights.y;
					string >> vertex.weights.z;
					string >> vertex.weights.w;
				}
			}

			vertices.emplace_back(vertex);
		}

		// Vertex indices
		tinyxml2::XMLElement* indicesNode = meshNode->FirstChildElement("indices");
		unsigned int count = std::strtoul(indicesNode->FirstAttribute()->Value(), nullptr, 0);
		auto text = indicesNode->FindAttribute("values")->Value();
		std::stringstream string(text);
		unsigned int index;
		for (size_t i = 0; i < count; i++)
		{
			string >> index;
			indices.emplace_back(index);
		}

		// Mesh AABB
		AABB aabb;
		tinyxml2::XMLElement* aabbNode = meshNode->FirstChildElement("aabb");
		if (aabbNode)
		{
			auto maxExtStr = aabbNode->FindAttribute("max_extent")->Value();
			std::stringstream maxStream(maxExtStr);
			maxStream >> aabb.max_extent.x;
			maxStream >> aabb.max_extent.y;
			maxStream >> aabb.max_extent.z;
			
			auto minExtStr = aabbNode->FindAttribute("min_extent")->Value();
			std::stringstream minStream(minExtStr);
			minStream >> aabb.min_extent.x;
			minStream >> aabb.min_extent.y;
			minStream >> aabb.min_extent.z;

			aabb.m_min_extent = aabb.min_extent;
			aabb.m_max_extent = aabb.max_extent;
		}
		else
			LT_WARN("Model {0} mesh {1} has not aabb", filePath, meshNode->FindAttribute("name")->Value());
		
		meshes.emplace_back(Mesh({ vertices, indices, aabb, aabbNode != nullptr }));
	}

	// Skeleton of the model
	bool init = true;
	tinyxml2::XMLElement* skeletonNode = modelNode->FirstChildElement("skeleton");
	
	Bone* skeleton = nullptr;
	std::vector<tuple<int, mat4*, mat4*>> transformsArray;

	if (skeletonNode)
	{
		// ID sorted bone array
		std::vector<Bone*> bonesArray;

		// CREATE the root bone (skeleton must have a unique root bone!)
		skeleton = new Bone();
		auto rootNode = skeletonNode->FirstChildElement("bone");
		helperBoneBuild(*skeleton, rootNode);
		bonesArray.emplace_back(skeleton);
		
		// BUILD skeleton and bone array (should be sorted)
		skeletonBuild(rootNode->FirstChildElement("bone"), skeleton, bonesArray);

		// SORT in case
		std::sort(bonesArray.rbegin(), bonesArray.rend(), [](Bone* bone1, Bone* bone2)
		{
			return bone1->id > bone2->id;
		});

		// BUILD transforms array
		for (auto bone : bonesArray)
		{
			int id;
			if (bone->parent)
				id = bone->parent->id;
			else
				id = -1;
			tuple<int, mat4*, mat4*> pair = { id, &bone->bindOffset, &bone->inverseBindOffset };
			transformsArray.emplace_back(pair);
		}
	}

	*model = new Model({ meshes, skeleton, transformsArray });
}


static void FullVertex(std::ifstream &in_file, std::vector<MUDLoader::Vertex>& out_vertices)
{
	using namespace MUDLoader;

	while (!in_file.bad() && !in_file.eof())
	{
		Vertex vert;
		in_file.read(reinterpret_cast<char*>(&vert), sizeof(Vertex));
		out_vertices.emplace_back(vert);
	}
}

static void PosVertex(std::ifstream& in_file, std::vector<MUDLoader::Vertex>& out_vertices)
{
	using namespace MUDLoader;

	while (in_file.good())
	{
		Vertex vert;
		in_file.read(reinterpret_cast<char*>(&vert.pos), sizeof(vec3));
		out_vertices.emplace_back(vert);
	}
}

static void NormalOnlyVertex(std::ifstream& in_file, std::vector<MUDLoader::Vertex>& out_vertices)
{
	using namespace MUDLoader;

	while (in_file.good())
	{
		Vertex vert;
		in_file.read(reinterpret_cast<char*>(&vert), sizeof(vec3) * 2);
		out_vertices.emplace_back(vert);
	}
}

static void UVonlyVertex(std::ifstream& in_file, std::vector<MUDLoader::Vertex>& out_vertices)
{
	using namespace MUDLoader;

	while (in_file.good())
	{
		vec3 pos;
		vec2 uv;
		in_file.read(reinterpret_cast<char*>(&pos), sizeof(vec3));
		in_file.read(reinterpret_cast<char*>(&uv), sizeof(vec2));
		Vertex vert;
		vert.pos = pos;
		vert.uvCoord = uv;
		out_vertices.emplace_back(vert);
	}
}

static bool ReadMesh(std::ifstream& in_file, const char *in_filePath, MUDLoader::Mesh& out_mesh)
{
	using namespace MUDLoader;

	// Get header byte and set the flags
	char header = 0;
	in_file >> header;
	
	CHECK_ERROR(in_file);
	CHECK_INCOMPLETE(in_file, in_filePath);
	

	// Set flags
	bool hasAABB = header & MUD_FILE_MASKS::HAS_AABB;
	bool hasUVs = header & MUD_FILE_MASKS::HAS_UVS;
	bool hasNormals = header & MUD_FILE_MASKS::HAS_NORMALS;

	// Print the flags
	std::cout << "FILE MASK: " << in_filePath << "\n";
	std::cout << "\tHAS AABB: " << hasAABB << "\n";
	std::cout << "\tHAS UVs: " << hasUVs << "\n";
	std::cout << "\tHAS NORMALS: " << hasNormals << "\n";
	std::cout << std::endl;

	// Get the verticex count and the indices count
	unsigned int vertexCount = 0;
	unsigned int indexCount = 0;

	in_file >> vertexCount;
	in_file >> indexCount;

	CHECK_ERROR(in_file);
	CHECK_INCOMPLETE(in_file, in_filePath);

	// If has 0 vertices or 0 indices is not a valid mesh
	if (vertexCount <= 0 || indexCount <= 0)
	{
		std::cout << "FILE " << in_filePath << " MESH NO VALID: IndexCount or VertexCount is 0" << std::endl;
		return true;
	}

	if (vertexCount > MAX_VERTS || indexCount > MAX_INDICES)
	{
		std::cout << "FILE " << in_filePath << " MESH TOO BIG!" << std::endl;
		return true;
	}

	// If has AABB read the aabb
	AABB aabb;
	if (hasAABB)
	{
		vec3 maxExtent;
		vec3 minExtent;

		in_file.read(reinterpret_cast<char*>(&maxExtent), sizeof(vec3));
		in_file.read(reinterpret_cast<char*>(&minExtent), sizeof(vec3));

		CHECK_ERROR(in_file);
		CHECK_INCOMPLETE(in_file, in_filePath);

		aabb.min_extent = minExtent;
		aabb.max_extent = maxExtent;
		aabb.m_max_extent = maxExtent;
		aabb.m_min_extent = minExtent;
	}

	// Read Indices
	const size_t bufferSize = sizeof(unsigned int) * indexCount;
	unsigned int* bufferBytes = new unsigned int[indexCount];

	in_file.read(reinterpret_cast<char*>(bufferBytes), bufferSize);
	CHECK_ERROR(in_file);
	CHECK_INCOMPLETE(in_file, in_filePath);

	std::vector<unsigned int> indices(bufferBytes, bufferBytes + indexCount);
	delete[] bufferBytes;

	// Read vertices
	std::vector<Vertex> vertices;
	vertices.reserve(vertexCount);

	if (hasNormals && hasUVs)
		FullVertex(in_file, vertices);
	else
		if (!hasNormals & !hasUVs)
			PosVertex(in_file, vertices);
		else
			if (!hasNormals)
				UVonlyVertex(in_file, vertices);
			else
				NormalOnlyVertex(in_file, vertices);

	CHECK_ERROR(in_file);
	out_mesh = Mesh({ vertices, indices, aabb, hasAABB });
	return 0;
}

// BINARY VERSION
void MUDLoader::LoadBinary(const char* in_filePath, Model** model)
{
	bool error = false;
	std::ifstream file;
	file.open(in_filePath, std::ios::binary | std::ios::in);

	if (!file.is_open())
	{
		std::cout << "FILE NOT FOUND : " << in_filePath << std::endl;
		return;
	}

	char header = 0;
	file >> header;
	if (!file.good())
	{
		std::cout << "FILE EMPTY: " << in_filePath << std::endl;
		return;
	}

	if (header & 1)
		std::cout << "MESH COUNT GREATER THAN 1" << std::endl;
	// Get the mesh count
	unsigned int meshCount = 0;
	file >> meshCount;

	if (!file.good())
	{
		std::cout << "FILE INVALID MUD BINARY: " << in_filePath << std::endl;
		return;
	}

	std::cout << "FILE MESH COUNT = " << meshCount << std::endl;

	*model = new Model();
	// Get every mesh
	for (unsigned int i = 0; i < meshCount; i++)
	{
		Mesh mesh;
		if (ReadMesh(file, in_filePath, mesh))
		{
			error = true;
			break;
		}
		(*model)->meshes.emplace_back(mesh);
	}
	if (error)
		std::cout << "FILE LOADED ERROR! " << in_filePath << std::endl;
	else
		std::cout << "FILE LOADED SUCCESFULY! " << in_filePath << std::endl;
	file.close();
}


void MUDLoader::CookBinary(const char* in_filePath, const char* in_bytes)
{
	if (!in_bytes)
	{
		std::cout << "COOKING::ERROR: Not model to cook!" << std::endl;
		return;
	}

	std::ofstream file;
	file.open(in_filePath, std::ios::binary | std::ios::out | std::ios::trunc);
	if (!file.good())
		std::cout << "COOKING::Could not open the file!" << std::endl;
	
	file << in_bytes;
	

	if (file.bad())
		std::cout << "COOKING::ERROR!" << std::endl;
	else
		std::cout << "COOKING::Success!" << std::endl;
	
	file.close();
}
