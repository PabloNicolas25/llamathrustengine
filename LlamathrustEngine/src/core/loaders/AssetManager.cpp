#include "ltpch.hpp"
#include "AssetManager.hpp"
#include "./mud_file/mud_importer.hpp"
#include "renderer/Objects/Model.hpp"

namespace Llamathrust
{
	AssetManager* AssetManager::s_instance = new AssetManager();

	Rendering::Model* AssetManager::LoadModel(const char* in_fileName, const char* in_name)
	{
		auto it = m_loadedFiles.find(in_fileName);
		// If not found load from disk
		if (it == m_loadedFiles.end())
		{
			MUDLoader::Model* disk_model = nullptr;
			MUDLoader::Load(in_fileName, &disk_model);

			// To prevent exceptions later we return if we didnt load the model
			if (!disk_model)
				return nullptr;
			
			Rendering::Model* model = new Rendering::Model(disk_model);
			m_models.emplace(in_name, model);
			return model;
		}

		auto loaded_name = std::string(it->second);
		if (loaded_name != std::string(in_name))
			LT_CORE_WARN("Asset Manager: Model {0} loaded with name {1}", in_name, loaded_name);
		
		return GetModel(it->second);
	}
	Rendering::Model* AssetManager::GetModel(const char* in_name) const
	{
		auto it = m_models.find(in_name);

		if (it == m_models.end())
			throw new AssetNotLoaded(in_name);
		return it->second;
	}
}
