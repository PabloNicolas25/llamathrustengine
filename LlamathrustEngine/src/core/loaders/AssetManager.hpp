#pragma once
#include "core/Core.hpp"
#include "renderer/Objects/Texture2D.hpp"
#include <map>
#include <exception>

namespace Llamathrust
{
	class App;

	namespace Rendering
	{
		class Model;
	}

	class LLAMATHRUST_API AssetManager
	{
		static AssetManager* s_instance;
	protected:
		friend class App;

		AssetManager() 
		{
			s_instance = this;
		}
		~AssetManager() = default;
	public:
		// Get the class instance (Singleton)
		static AssetManager* Get() {
			return s_instance;
		}

		/* Load a model from disk.
		*	If already exist get the model in memory.
		*	If already loaded but in_name is different print warning
		*/
		Rendering::Model* LoadModel(const char* in_fileName, const char* in_name);

		// Get the model in memory. If not loaded rise ASSET_NOT_LOADED exception
		Rendering::Model* GetModel(const char* in_name) const;

	private:
		// set of file path successfuly loaded and names
		std::unordered_map<const char*, const char*> m_loadedFiles;
		// map of model's name and model
		std::unordered_map<const char*, Rendering::Model*> m_models;
	};

	

	struct AssetNotLoaded : public std::exception {
		AssetNotLoaded()
		{
			m_name = "NOT_NAME_PROVIDED";
		}

		AssetNotLoaded(const char* in_assetName)
		{
			m_name;
		}

		const char* what() const throw () {
			return (std::string("Asset ") + std::string(m_name) + std::string(" not loaded!")).c_str();
		}
	private:
		const char* m_name;
	};
}