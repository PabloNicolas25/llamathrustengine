#include "ltpch.hpp"
#include "Application.hpp"
#include "input/Input.hpp"
#include "input/InputCodes.hpp"
#include "imgui/ImGuiLayer.hpp"

#include <GLFW/glfw3.h>
#include <glad/glad.h>


namespace Llamathrust 
{
	Application* Application::s_Instance = nullptr;

	Application::Application(const char* title = "Sandbox", int width = 1280, int height = 720, glm::vec3& in_backColor = glm::vec3())
	{
		// SetUp the singleton
		s_Instance = this;
		
		// for now it only uses one window per app
		// we create the window
		WindowProps windowProperties = { title, (unsigned int)width, (unsigned int)height };
		m_Window = Window::Create(windowProperties);
		m_videoDriver = m_Window->GetVideoDriver();

		// We set the method "OnEvent" as an event callback
		m_Window->SetEventCallback(LT_BIND_EVENT_FN(Application::OnEvent));

		// Push the overlay ImGui layer (for debugging purpouses)
		m_ImGuiLayer = new ImGuiLayer();
		PushOverlay(m_ImGuiLayer);

		m_videoDriver->setClearColor({in_backColor.r, in_backColor.g, in_backColor.b, 1});

		m_camera = new Rendering::Camera(70.0f, 1000.0f, 0.01f, (float) m_Window->GetWidth() / m_Window->GetHeight());
		m_viewNode = new Rendering::SceneNode(glm::mat4(1.0f));
		Rendering::ViewProperties* viewProp = new Rendering::ViewProperties(glm::vec3(0, 0, 0), 0, 0, *(new glm::vec3(0, 1, 0)));
		m_viewNode->SetAsViewNode(viewProp);
	}

	Application::~Application()
	{
		delete m_videoDriver;
	}

	void Application::Run()
	{
		/* The render layers should be the ones who clear the viewport
		* so we do this one time before all things just to give
		* a welcome screen
		*/
		using namespace Graphics;

		while (m_Running)
		{
			ImGuiIO& io = ImGui::GetIO();
			float delta = io.Framerate / 1000.0f;

#ifdef LT_DEBUG
			DrawCallListener::Get()->Reset();
#endif

			m_videoDriver->clearScreenBuffer(
				BUFFERTYPESFLAGS::COLOR_BUFFER |
				BUFFERTYPESFLAGS::DEPTH_BUFFER);

			// Draw or update data from first layer to the last
			for (auto layer : m_layerStack)
			{
				layer->OnUpdate(delta);
				layer->OnRender();
			}

			// Begin ImGui Rendering
			m_ImGuiLayer->Begin();
			// Render every overlay layer
			for (auto layer : m_layerStack)
				layer->OnImGuiRender();
			// End ImGui Rendering
			m_ImGuiLayer->End();


			m_Window->OnUpdate();
		}
	}

	void Application::OnEvent(Event & e)
	{
		// Create a dispatcher
		EventDispatcher dispatcher(e);
		// Dispatch the event
		dispatcher.Dispatch<WindowCloseEvent>(LT_BIND_EVENT_FN(Application::OnWindowClose));
		dispatcher.Dispatch<WindowResizeEvent>(LT_BIND_EVENT_FN(Application::OnWindowResize));

		// handle the events from last layer to first
		for (auto it = m_layerStack.end(); it != m_layerStack.begin();)
		{
			// decrease the iterator and then reference and call OnEvent
			(*--it)->OnEvent(e);
			// if it is handled dont pass it further
			if (e.Handled)
				break;
		}
	}

	void Application::PushLayer(Layer * layer)
	{
		m_layerStack.PushLayer(layer);
		layer->OnAttach();
	}

	void Application::PushOverlay(Layer * layer)
	{
		m_layerStack.PushOverlay(layer);
		layer->OnAttach();
	}
	
	bool Application::OnWindowClose(WindowCloseEvent & e)
	{
		m_Running = false;
		return true;
	}

	bool Application::OnWindowResize(WindowResizeEvent& e)
	{
		Graphics::Viewport viewport = { 0, 0, e.GetWindowWidth(), e.GetWindowHeight() };
		m_videoDriver->setViewport(&viewport);
		m_camera->RecalculateProjectionMatrix(
			m_camera->GetFieldOfView(),
			m_camera->GetMaxDistance(),
			m_camera->GetMinDistance(),
			(float)e.GetWindowWidth() / (float)e.GetWindowHeight());
		return false;
	}
}
