#pragma once
#include "../Core.hpp"

namespace Llamathrust
{
	/*
		Static singleton interface for input. This means there is one input system at a time.
		Since only one window has the focus this is not a problem.
	*/
	class LLAMATHRUST_API Input
	{
		static Input* s_Instance;
	
	public:
		inline static bool IsKeyPressed(int keycode) {
			return s_Instance->IsKeyPressedImpl(keycode);
		}
	
		inline static bool IsMouseButtonPressed(int button) {
			return s_Instance->IsMouseButtonPressedImpl(button);
		}

		inline static float GetMouseX() {
			return s_Instance->GetMouseXImpl();
		}

		inline static float GetMouseY() {
			return s_Instance->GetMouseYImpl();
		}

		inline static glm::vec2 GetMousePosition()
		{
			return s_Instance->GetMousePositionImpl();
		}
	protected:
		virtual bool IsKeyPressedImpl(int keycode) = 0;
	
		virtual bool IsMouseButtonPressedImpl(int button) = 0;

		virtual float GetMouseXImpl() = 0;
		virtual float GetMouseYImpl() = 0;
		virtual glm::vec2 GetMousePositionImpl() = 0;
	};
}