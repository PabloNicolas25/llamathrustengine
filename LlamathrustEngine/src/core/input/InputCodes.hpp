#pragma once

// CODES FROM GLFW 3

namespace Llamathrust
{
	enum KEYCODES : unsigned int
	{
		LT_KEY_SPACE         =    32,
		LT_KEY_APOSTROPHE    =    39,  /* ' */
		LT_KEY_COMMA         =    44,  /* , */
		LT_KEY_MINUS         =    45,  /* - */
		LT_KEY_PERIOD        =    46,  /* . */
		LT_KEY_SLASH         =    47,  /* / */
		LT_KEY_0             =    48,
		LT_KEY_1             =    49,
		LT_KEY_2             =    50,
		LT_KEY_3             =    51,
		LT_KEY_4             =    52,
		LT_KEY_5             =    53,
		LT_KEY_6             =    54,
		LT_KEY_7             =    55,
		LT_KEY_8             =    56,
		LT_KEY_9             =    57,
		LT_KEY_SEMICOLON     =    59,  /* ; */
		LT_KEY_EQUAL         =    61,  /* = */
		LT_KEY_A             =    65,
		LT_KEY_B             =    66,
		LT_KEY_C             =    67,
		LT_KEY_D             =    68,
		LT_KEY_E             =    69,
		LT_KEY_F             =    70,
		LT_KEY_G             =    71,
		LT_KEY_H             =    72,
		LT_KEY_I             =    73,
		LT_KEY_J             =    74,
		LT_KEY_K             =    75,
		LT_KEY_L             =    76,
		LT_KEY_M             =    77,
		LT_KEY_N             =    78,
		LT_KEY_O             =    79,
		LT_KEY_P             =    80,
		LT_KEY_Q             =    81,
		LT_KEY_R             =    82,
		LT_KEY_S             =    83,
		LT_KEY_T             =    84,
		LT_KEY_U             =    85,
		LT_KEY_V             =    86,
		LT_KEY_W             =    87,
		LT_KEY_X             =    88,
		LT_KEY_Y             =    89,
		LT_KEY_Z             =    90,
		LT_KEY_LEFT_BRACKET  =    91,  /* [ */
		LT_KEY_BACKSLASH     =    92,  /* \ */
		LT_KEY_RIGHT_BRACKET =    93,  /* ] */
		LT_KEY_GRAVE_ACCENT  =    96,  /* ` */
		LT_KEY_WORLD_1       =    161, /* non-US #1 */
		LT_KEY_WORLD_2       =    162, /* non-US #2 */
		
		/* Function keys */	 
		LT_KEY_ESCAPE        =    256,
		LT_KEY_ENTER         =    257,
		LT_KEY_TAB           =    258,
		LT_KEY_BACKSPACE     =    259,
		LT_KEY_INSERT        =    260,
		LT_KEY_DELETE        =    261,
		LT_KEY_RIGHT         =    262,
		LT_KEY_LEFT          =    263,
		LT_KEY_DOWN          =    264,
		LT_KEY_UP            =    265,
		LT_KEY_PAGE_UP       =    266,
		LT_KEY_PAGE_DOWN     =    267,
		LT_KEY_HOME          =    268,
		LT_KEY_END           =    269,
		LT_KEY_CAPS_LOCK     =    280,
		LT_KEY_SCROLL_LOCK   =    281,
		LT_KEY_NUM_LOCK      =    282,
		LT_KEY_PRINT_SCREEN  =    283,
		LT_KEY_PAUSE         =    284,
		LT_KEY_F1            =    290,
		LT_KEY_F2            =    291,
		LT_KEY_F3            =    292,
		LT_KEY_F4            =    293,
		LT_KEY_F5            =    294,
		LT_KEY_F6            =    295,
		LT_KEY_F7            =    296,
		LT_KEY_F8            =    297,
		LT_KEY_F9            =    298,
		LT_KEY_F10           =    299,
		LT_KEY_F11           =    300,
		LT_KEY_F12           =    301,
		LT_KEY_F13           =    302,
		LT_KEY_F14           =    303,
		LT_KEY_F15           =    304,
		LT_KEY_F16           =    305,
		LT_KEY_F17           =    306,
		LT_KEY_F18           =    307,
		LT_KEY_F19           =    308,
		LT_KEY_F20           =    309,
		LT_KEY_F21           =    310,
		LT_KEY_F22           =    311,
		LT_KEY_F23           =    312,
		LT_KEY_F24           =    313,
		LT_KEY_F25           =    314,
		LT_KEY_KP_0          =    320,
		LT_KEY_KP_1          =    321,
		LT_KEY_KP_2          =    322,
		LT_KEY_KP_3          =    323,
		LT_KEY_KP_4          =    324,
		LT_KEY_KP_5          =    325,
		LT_KEY_KP_6          =    326,
		LT_KEY_KP_7          =    327,
		LT_KEY_KP_8          =    328,
		LT_KEY_KP_9          =    329,
		LT_KEY_KP_DECIMAL    =    330,
		LT_KEY_KP_DIVIDE     =    331,
		LT_KEY_KP_MULTIPLY   =    332,
		LT_KEY_KP_SUBTRACT   =    333,
		LT_KEY_KP_ADD        =    334,
		LT_KEY_KP_ENTER      =    335,
		LT_KEY_KP_EQUAL      =    336,
		LT_KEY_LEFT_SHIFT    =    340,
		LT_KEY_LEFT_CONTROL  =    341,
		LT_KEY_LEFT_ALT      =    342,
		LT_KEY_LEFT_SUPER    =    343,
		LT_KEY_RIGHT_SHIFT   =    344,
		LT_KEY_RIGHT_CONTROL =    345,
		LT_KEY_RIGHT_ALT     =    346,
		LT_KEY_RIGHT_SUPER   =    347,
		LT_KEY_MENU          =    348,
	};

	enum MOUSEBUTTONCODES : unsigned char
	{
		LT_MOUSE_BUTTON_1       =  0,
		LT_MOUSE_BUTTON_2       =  1,
		LT_MOUSE_BUTTON_3       =  2,
		LT_MOUSE_BUTTON_4       =  3,
		LT_MOUSE_BUTTON_5       =  4,
		LT_MOUSE_BUTTON_6       =  5,
		LT_MOUSE_BUTTON_7       =  6,
		LT_MOUSE_BUTTON_8       =  7,
		LT_MOUSE_BUTTON_LAST    =  LT_MOUSE_BUTTON_8,
		LT_MOUSE_BUTTON_LEFT    =  LT_MOUSE_BUTTON_1,
		LT_MOUSE_BUTTON_RIGHT   =  LT_MOUSE_BUTTON_2,
		LT_MOUSE_BUTTON_MIDDLE  =  LT_MOUSE_BUTTON_3,
	};
}