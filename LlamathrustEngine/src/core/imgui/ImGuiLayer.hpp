#pragma once
#include "core/Core.hpp"
#include "core/Layer.hpp"
#include "core/events/KeyEvent.hpp"
#include "core/events/MouseEvent.hpp"
#include "core/events/ApplicationEvent.hpp"

#define IMGUI_IMPL_API
#include <imgui.h>

namespace Llamathrust
{
	class LLAMATHRUST_API ImGuiLayer : public Layer
	{
		double g_Time = 1 / 60;
	
	public:
		ImGuiLayer();
		~ImGuiLayer() = default;

		void OnAttach() override;
		void OnDetach() override;

		void OnImGuiRender() override;

		void Begin();
		void End();

		// Pointer to the main and only ImGui context
		ImGuiContext* m_context;
	};
}