#pragma once
#include <core/Core.hpp>
#include "Layer.hpp"

#include <vector>


namespace Llamathrust
{
	class LLAMATHRUST_API LayerStack
	{
	public:
		LayerStack();
		~LayerStack();

		void PushLayer(Layer* layer);
		void PushOverlay(Layer* overlay);
		void PopLayer(Layer* layer);
		void PopOverlay(Layer* overlay);

		std::vector<Layer*>::iterator begin() { return m_Layers.begin(); }
		std::vector<Layer*>::iterator end() { return m_Layers.end(); }
	private:
#pragma warning(suppress: 4251)
		std::vector<Layer*> m_Layers;
		uint_32 m_LayerInsertIndex = 0;
	};
}
