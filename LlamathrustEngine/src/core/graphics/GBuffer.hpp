#pragma once

#include "core/Core.hpp"

namespace Llamathrust
{
	namespace Graphics
	{
		enum LLAMATHRUST_API SHADER_DATA_TYPE
		{
			Float = 0,	Float2, Float3, Float4,
			Int,	Int2,	Int3,	Int4,
			Mat3,	Mat4,
			Bool
		};

		static uint_32 GetElementSize(SHADER_DATA_TYPE in_type)
		{
			switch (in_type)
			{
			case SHADER_DATA_TYPE::Float:
			case SHADER_DATA_TYPE::Int:
				return 4;
			case SHADER_DATA_TYPE::Float2:
			case SHADER_DATA_TYPE::Int2:
				return 4 * 2;
			case SHADER_DATA_TYPE::Float3:
			case SHADER_DATA_TYPE::Int3:
				return 4 * 3;
			case SHADER_DATA_TYPE::Float4:
			case SHADER_DATA_TYPE::Int4:
				return 4 * 4;
			case SHADER_DATA_TYPE::Mat3:
				return 4 * 3 * 3;
			case SHADER_DATA_TYPE::Mat4:
				return 4 * 4 * 4;
			case SHADER_DATA_TYPE::Bool:
				return 1;
			}
			LT_CORE_ASSERT(false, "Unknown type")
			return 0;
		}

		struct LLAMATHRUST_API BufferElement
		{
			std::string name;
			uint_32 size;
			uint_32 offset;
			SHADER_DATA_TYPE type;
			bool normalized;

			BufferElement(SHADER_DATA_TYPE in_type, const std::string& in_name, const bool in_normalized = false)
				: name(in_name), type(in_type), size(GetElementSize(in_type)), offset(0), normalized(in_normalized)
			{}

			uint_32 GetComponentCount() const
			{
				switch (type)
				{
				case SHADER_DATA_TYPE::Float:
				case SHADER_DATA_TYPE::Int:
				case SHADER_DATA_TYPE::Bool:
					return 1;
				case SHADER_DATA_TYPE::Float2:
				case SHADER_DATA_TYPE::Int2:
					return 2;
				case SHADER_DATA_TYPE::Float3:
				case SHADER_DATA_TYPE::Int3:
					return 3;
				case SHADER_DATA_TYPE::Float4:
				case SHADER_DATA_TYPE::Int4:
					return 4;
				case SHADER_DATA_TYPE::Mat3:
					return 3 * 3;
				case SHADER_DATA_TYPE::Mat4:
					return 4 * 4;
				}
				LT_CORE_ASSERT(false, "Unknown type");
				return 0;
			}
		};

		class LLAMATHRUST_API BufferLayout
		{
			std::vector<BufferElement> m_elements;
			uint_32 m_stride = 0;

		public:
			BufferLayout(const std::initializer_list<BufferElement>& in_elements)
			 :m_elements(in_elements)
			{
				CalculateOffsetAndStride();
			}

			inline uint_32 GetStride() const { return m_stride; }

			inline const std::vector<BufferElement>& GetElements() const { return m_elements; }

			std::vector<BufferElement>::iterator begin() { return m_elements.begin(); }
			std::vector<BufferElement>::iterator end() { return m_elements.end(); }

			std::vector<BufferElement>::const_iterator begin() const { return m_elements.begin(); }
			std::vector<BufferElement>::const_iterator end() const { return m_elements.end(); }
		private:
			void CalculateOffsetAndStride()
			{
				uint_32 offset = 0;
				m_stride = 0;
				for (auto& element : m_elements)
				{
					element.offset = offset;
					offset += element.size;
					m_stride += element.size;
				}
			}
		};

		class LLAMATHRUST_API VertexBuffer
		{
		public:
			virtual ~VertexBuffer() = default;
			virtual void Bind() const = 0;
		};

		class LLAMATHRUST_API IndexBuffer
		{
		public:
			virtual ~IndexBuffer() = default;

			virtual void Bind() const = 0;
		};
	}
}