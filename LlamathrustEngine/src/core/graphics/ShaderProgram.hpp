#pragma once
#include "core/Core.hpp"
#include <vector>
#include <glm/glm.hpp>

namespace Llamathrust
{
	namespace Graphics
	{
		struct LLAMATHRUST_API ShaderProgram
		{
			// Create the GPU program
			virtual void Create() = 0;
			virtual void Use() = 0;
			virtual void SetInt(const char* in_uniformName, const int in_value) = 0;
			virtual void SetFloat(const char* in_uniformName, const float in_value) = 0;
			virtual void SetVec3(const char* in_uniformName, const glm::vec3& in_vec) = 0;
			virtual void SetVec4(const char* in_uniformName, const glm::vec4& in_vec) = 0;
			virtual void SetMat4(const char* in_uniformName, const glm::mat4& in_mat) = 0;
			virtual void SetMat4vec(const char* in_uniformName, const std::vector<glm::mat4>& in_matVec) = 0;
		};
	}
}