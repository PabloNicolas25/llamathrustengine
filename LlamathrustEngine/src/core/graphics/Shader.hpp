#pragma once

#include "core/Core.hpp"


namespace Llamathrust
{
	namespace Graphics
	{
		enum LLAMATHRUST_API SHADER_TYPE
		{
			VERTEX_SHADER,
			PIXEL_SHADER
		};

		class LLAMATHRUST_API Shader
		{
		protected:
			const SHADER_TYPE m_type;
		public:
			Shader(const SHADER_TYPE in_type) : m_type(in_type) {}
			virtual void* Get() = 0;
		};
	}
}
