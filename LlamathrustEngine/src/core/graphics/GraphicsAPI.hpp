/*
AUTHOR: Narvaja, Pablo Nicolas
LICENSE: MIT

In this file we declare the API pure virtual struct which
is used to abstract the graphics APIs
*/

#pragma once
#include "core/Core.hpp"
#include <glm/glm.hpp>
//#include "renderer/Objects/RenderTarget.hpp"

namespace Llamathrust
{
	namespace Graphics
	{
		enum LLAMATHRUST_API BUFFERTYPESFLAGS : int
		{
			DEPTH_BUFFER = 1,
			COLOR_BUFFER = 1 << 1,
			STENCIL_BUFFER = 1 << 2
		};
		LT_OR_OPERATOR_OVERLOAD(BUFFERTYPESFLAGS);

		enum LLAMATHRUST_API CULL_FACES
		{
			NONE,
			BACK_FACE,
			FRONT_FACE,
		};

		enum LLAMATHRUST_API DEPTH_TESTING
		{
			ENABLE = true,
			DISABLE = false
		};

		struct LLAMATHRUST_API Viewport
		{
			int TopLeftX = 0;
			int TopLeftY = 0;
			int Width = 640;
			int Height = 480;
		};

		struct LLAMATHRUST_API Color
		{
			union {
				struct {
					float red;
					float green;
					float blue;
					float alpha;
				};
				glm::vec4 vector;
			};

			Color() : red(0), green(0), blue(0), alpha(1) {}

			Color(float in_red, float in_green, float in_blue, float in_alpha)
				: red(in_red), green(in_green), blue(in_blue), alpha(in_alpha)
			{}

			Color(glm::vec4& in_color) : vector(in_color) {}
		};

		struct LLAMATHRUST_API RenderAPI
		{
			RenderAPI()
			{
				s_instance = this;
			}

			virtual bool setUpAPI(int in_width, int in_height) = 0;

			virtual void setViewport(Viewport* in_viewport) = 0;

			virtual void clearScreenBuffer(const BUFFERTYPESFLAGS in_buffersFlags) = 0;

			virtual void setClearColor(const Color& in_color) = 0;

			virtual void enableDepthTesting(const bool in_enable) = 0;

			virtual void enableCullFace(const CULL_FACES in_face) = 0;

			virtual void enableWireframeMode(const bool in_wireframeMode) = 0;

			virtual void enableBlending(const bool in_enable) = 0;
			//virtual void setRenderTarget(Rendering::RenderTarget& in_target) = 0;

			Viewport& getViewport() { return m_viewport; }

			static RenderAPI* Get() { return s_instance; }

		protected:
			static RenderAPI* s_instance;
			Viewport m_viewport = Viewport();
			Color m_backgroundColor = { 0, 0, 0, 1 };
		};
	}
}