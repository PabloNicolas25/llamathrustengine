#pragma once

#include "ltpch.hpp"

#include "Core.hpp"
#include "events/Event.hpp"
#include "core/graphics/GraphicsAPI.hpp"

namespace Llamathrust {

	enum RENDER_DRIVER
	{
		NONE,
#ifdef LT_PLATFORM_WINDOWS
		DIRECTX11,
#endif
		OPENGL4
	};

	// Window config structure
	struct WindowProps
	{
		std::string Title;
		unsigned int Width;
		unsigned int Height;

		WindowProps(const std::string& title = "Llamathrust Engine",
			unsigned int width = 1280,
			unsigned int height = 720)
			: Title(title), Width(width), Height(height)
		{
		}
	};

	// Interface representing a desktop system based Window
	class LLAMATHRUST_API Window
	{
	protected:
		RENDER_DRIVER m_api;
		// Owned by Application
		Graphics::RenderAPI* m_videoDriver = nullptr;
	public:
		using EventCallbackFn = std::function<void(Event&)>;

		virtual ~Window() {}

		virtual void OnUpdate() = 0;

		virtual unsigned int GetWidth() const = 0;
		virtual unsigned int GetHeight() const = 0;

		// Window attributes
		virtual void SetEventCallback(const EventCallbackFn& callback) = 0;
		virtual void SetVSync(bool enabled) = 0;
		virtual bool IsVSync() const = 0;

		virtual void* GetNativeWindow() const = 0;
		
		Graphics::RenderAPI* GetVideoDriver() {
			return m_videoDriver;
		}

		static Window* Create(const WindowProps& props = WindowProps());
	};

}