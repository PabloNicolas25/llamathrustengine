#include "ltpch.hpp"
#include "AABB_tree.hpp"
#include "renderer/RenderDevice.hpp"

namespace Llamathrust::DataStructures
{
	AABBTree::AABBTree(const float in_margin, const uint_32 in_aproximateSize)
		: m_marginVector(in_margin)
	{
		if (in_aproximateSize)
			m_invalidNodes.reserve(in_aproximateSize);
	}
	
	void AABBTree::AddAABB(AABB* in_dataAABB, const void* in_user)
	{
		LT_CORE_ASSERT(in_dataAABB != nullptr, "You are trying to add null AABB data!");

		// If the root already exists then create the
		// leaf node to be added and look for the right place.
		if (m_root)
		{
			auto node = new AABBTreeNode();
			node->dataAABB = in_dataAABB;
			node->user = in_user;
			AddNode(node);
		}
		else
		{
			m_root = new AABBTreeNode();
			m_root->dataAABB = in_dataAABB;
			m_root->user = in_user;
		}
	}

	void AABBTree::Update()
	{
		if(!m_root)
			return;

		// since there is only one node there is no performance drawback
		// by updating it all the time
		if (m_root->IsLeaf())
		{
			m_root->CalculateAABB(m_marginVector);
		}
		else
		{
			// clear the list of nodes to update
			m_invalidNodes.clear();
			// get the nodes to update
			GetInvalidNodes(m_root);

			// reinsert the nodes in the list
			for (AABBTreeNode* node : m_invalidNodes)
			{
				// node grandparent
				AABBTreeNode* parent = node->parent;
				AABBTreeNode* sibling = (parent->right == node) ? parent->left : parent->right;

				// if there's a grandparent
				if (parent->parent)
				{
					sibling->parent = parent->parent;
					((parent->parent->left == parent) ? parent->parent->left : parent->parent->right) = sibling;
				}
				// if no grandparent make sibling root
				else
				{
					m_root = sibling;
					sibling->parent = nullptr;
				}

				delete parent;
				parent = nullptr;
				node->parent = nullptr;
				// reinsert the node
				node->CalculateAABB(m_marginVector);
				AddNode(node, &m_root);
			}
		}
	}

	bool AABBTree::RayCast(const glm::vec3& in_from, const glm::vec3& in_direction,
		const float in_distance, glm::vec3& out_position, void** out_user) const
	{
		// If the tree is empty there is not collision
		if (!m_root) return false;

		// Else we create the ray
		Ray ray(in_from, in_direction);

		// And perform the raycast
		return RayCastTree(ray, *m_root, out_user);
	}

	void AABBTree::Draw(const AABBTreeNode* in_node) const
	{
		using namespace Rendering;
		if (!m_root)
			return;

		if (!in_node)
			Draw(m_root);
		
		else
		{
			if (in_node->IsLeaf())
			{
				RenderDevice::Get()->DrawAABB(*in_node->dataAABB);//, Graphics::Color(1, 1, 0, 1));
			}
			else
			{
				Draw(in_node->right);
				Draw(in_node->left);
				RenderDevice::Get()->DrawAABB(*in_node->fatAABB);//, Graphics::Color(0, 1, 0, 1));
			}
		}
	}

	void AABBTree::AddNode(AABBTreeNode* in_node, AABBTreeNode** in_parent)
	{
		LT_CORE_ASSERT(in_node != nullptr, "You are trying to add an empty node!");

		// save the handle to the parent node to add it
		// to the new branch node
		AABBTreeNode* parentNode = *in_parent;

		//If the parent is leaf, create new branch node
		// and make it the new parent of both nodes (node and parent)
		if (parentNode->IsLeaf())
		{
			AABBTreeNode* newBranchNode = new AABBTreeNode();
			// set parent of new node same as oldparent parent
			newBranchNode->parent = parentNode->parent;

			// set children of new node and set its parent to new node
			newBranchNode->SetBranch(in_node, parentNode);

			// set the grandpa pointer to new branchNode
			*in_parent = newBranchNode;
		}
		// if parent is a branch evaluate into what child do insertion
		else
		{
			AABB* AABB_R = parentNode->right->fatAABB;
			AABB* AABB_L = parentNode->left->fatAABB;

			// DeltaVolume of inserting the AABB: Volume after insertion - volume befor insertion
			float volumeR = AABB::Union(*AABB_R, *in_node->fatAABB).GetVolume() - AABB_R->GetVolume();
			float volumeL = AABB::Union(*AABB_L, *in_node->fatAABB).GetVolume() - AABB_L->GetVolume();

			// if volume in right is bigger than volume in left
			// then left is better candidate than right
			if (volumeR > volumeL)
				AddNode(in_node, &parentNode->left);
			else
				AddNode(in_node, &parentNode->right);
		}

		// calc the aabb containing the children including the ones up it the tree
		(*in_parent)->CalculateAABB(m_marginVector);
	}
	
	void AABBTree::GetInvalidNodes(AABBTreeNode* in_node)
	{
		// if the node is a leaf then test for update
		if (in_node->IsLeaf())
		{
			// if the dataAabb isn't completly inside the fatAabb
			// then needs update
			if (!in_node->fatAABB->Contains(in_node->dataAABB))
				m_invalidNodes.push_back(in_node);
			return;
		}
		GetInvalidNodes(in_node->left);
		GetInvalidNodes(in_node->right);
	}

	bool AABBTree::RayCastTree(const Ray& in_ray, const AABBTreeNode& in_node, void** out_user) const
	{
		// If the node is a leaf intersect the ray with its aabb
		if (in_node.IsLeaf())
		{
			// If it intersects
			if (in_node.dataAABB->RayIntersect(in_ray))
			{
				// set the user of the node
				*out_user = (void*)in_node.user;
				// and return true
				return true;
			}
		}
		// If the node is a branch
		else
		{
			bool right = RayCastTree(in_ray, *in_node.right, out_user);
			if (right)
			{
				return true;
			}

			bool left = RayCastTree(in_ray, *in_node.left, out_user);
			if (left)
			{
				return true;
			}
		}
		return false;
	}
}
