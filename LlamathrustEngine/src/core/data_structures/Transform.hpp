#pragma once
#include "core/Core.hpp"
#include <glm/glm.hpp>

namespace Llamathrust
{
	namespace DataStructures
	{
		class LLAMATHRUST_API Transform
		{
			glm::mat4 m_mat;

		public:

			glm::vec3 GetPosition();
			void SetPosition(glm::vec3& in_pos);

			glm::mat4 GetMatrix() const
			{
				return m_mat;
			}

			glm::mat4& GetMatrix()
			{
				return m_mat;
			}
		};
	}
}