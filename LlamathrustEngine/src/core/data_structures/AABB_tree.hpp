#pragma once
#include "core/Core.hpp"
#include "AABB.hpp"

namespace Llamathrust
{
	namespace DataStructures
	{
		struct AABBTreeNode
		{
			AABBTreeNode* parent = nullptr;
			AABBTreeNode* right = nullptr;
			AABBTreeNode* left = nullptr;
			AABB* fatAABB = nullptr;
			AABB* dataAABB = nullptr;
			// Usually the scene node
			const void* user = nullptr;

			inline bool IsLeaf() const { return !right && !left; }

			void SetBranch(AABBTreeNode* in_left, AABBTreeNode* in_right)
			{
				in_left->parent = in_right->parent = this;
				right = in_right;
				left = in_left;
			}

			void SetLeaf(AABB* in_dataAABB)
			{
				LT_CORE_ASSERT(in_dataAABB != nullptr, "SetLeaf() AABB null!");

				fatAABB = in_dataAABB;
				right = left = nullptr;
			}

			void CalculateAABB(const glm::vec3& in_marginVec)
			{
				if (!IsLeaf())
				{
					auto rightData = (right->dataAABB) ? *right->dataAABB : *right->fatAABB;
					auto leftData = (left->dataAABB) ? *left->dataAABB : *left->fatAABB;
					fatAABB = new AABB(AABB::Union(rightData, leftData));
				}
				else
				{
					fatAABB = new AABB(*dataAABB);
				}
				fatAABB->GetMaxExtent() += in_marginVec;
				fatAABB->GetMinExtent() -= in_marginVec;
			}
		};

		class LLAMATHRUST_API AABBTree
		{
			glm::vec3 m_marginVector;
			AABBTreeNode* m_root =  nullptr;
			uint_32 m_nodeCount = 0;
			std::vector<AABBTreeNode*> m_invalidNodes;

		public:
			AABBTree(const float in_margin = 0.0f, const uint_32 in_aproximateSize = 0);
			~AABBTree() = default;

			// Add the aabb as a leaf-node in the tree
			void AddAABB(AABB* in_dataAABB, const void* in_user = nullptr);

			// Update the tree
			void Update();

			/* Cast ray through out the tree and return true if intersects a node
			 * in_from: point from where the ray is casted
			 * in_direction: direction towards the ray is casted
			 * in_distance: max length of the ray
			 * out_position: hit position
			 */
			bool RayCast(const glm::vec3& in_from, const glm::vec3& in_direction,
				const float in_distance, glm::vec3& out_position, void** out_user) const;

			// Draw the tree in screen
			void Draw(const AABBTreeNode* in_node = nullptr) const;

		private:
			// Run the tree recursively and adds the node in the best position
			void AddNode(AABBTreeNode* in_node, AABBTreeNode** in_parent = nullptr);
			// Get the nodes that needs to be updated
			void GetInvalidNodes(AABBTreeNode* in_node);
			// RayCast through the tree, return out_user which is the node user
			bool RayCastTree(const Ray& in_ray, const AABBTreeNode& in_node, void** out_user = nullptr) const;
		};
	}
}