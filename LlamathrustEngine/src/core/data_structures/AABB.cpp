#include "ltpch.hpp"
#include "AABB.hpp"
#include "renderer/RenderDevice.hpp"

namespace Llamathrust::DataStructures
{
	

	

	void AABB::Update(const glm::vec3& in_pos)
	{
		auto delta_pos = in_pos - GetPosition();
		max_extent.xyz = m_max_extent.xyz + delta_pos;
		min_extent.xyz = m_min_extent.xyz + delta_pos;
	}

	bool AABB::Contains(const AABB* in_aabb) const
	{
		auto maxExtentsProposition =
			max_extent[0] > in_aabb->max_extent[0] &&
			max_extent[1] > in_aabb->max_extent[1] &&
			max_extent[2] > in_aabb->max_extent[2];

		auto minExtentsProposition =
			min_extent[0] < in_aabb->min_extent[0] &&
			min_extent[1] < in_aabb->min_extent[1] &&
			min_extent[2] < in_aabb->min_extent[2];

		return maxExtentsProposition && minExtentsProposition;
	}

	bool AABB::Contains(const glm::vec3& in_point) const
	{
		bool on_xAxis = max_extent.x >= in_point.x && min_extent.x <= in_point.x;
		bool on_yAxis = max_extent.y >= in_point.y && min_extent.y <= in_point.y;
		bool on_zAxis = max_extent.z >= in_point.z && min_extent.z <= in_point.z;
		return on_xAxis && on_yAxis && on_zAxis;
	}

	bool AABB::RayIntersect(const Ray& in_ray) const //, float t)
	{
		// This is actually correct, even though it appears not to handle edge cases
		// (ray.n.{x,y,z} == 0).  It works because the infinities that result from
		// dividing by zero will still behave correctly in the comparisons.  Rays
		// which are parallel to an axis and outside the box will have tmin == inf
		// or tmax == -inf, while rays inside the box will have tmin and tmax
		// unchanged.

		float tx1 = (min_extent.x - in_ray.origin.x) * in_ray.inv_dig_direction.x;
		float tx2 = (max_extent.x - in_ray.origin.x) * in_ray.inv_dig_direction.x;

		float tmin = std::min(tx1, tx2);
		float tmax = std::max(tx1, tx2);

		float ty1 = (min_extent.y - in_ray.origin.y) * in_ray.inv_dig_direction.y;
		float ty2 = (max_extent.y - in_ray.origin.y) * in_ray.inv_dig_direction.y;

		tmin = std::max(tmin, std::min(ty1, ty2));
		tmax = std::min(tmax, std::max(ty1, ty2));

		float tz1 = (min_extent.z - in_ray.origin.z) * in_ray.inv_dig_direction.z;
		float tz2 = (max_extent.z - in_ray.origin.z) * in_ray.inv_dig_direction.z;

		tmin = std::max(tmin, std::min(tz1, tz2));
		tmax = std::min(tmax, std::max(tz1, tz2));

		return tmax >= std::max(0.0f, tmin);// && tmin < t;
	}

	AABB AABB::Union(const AABB& in_right, const AABB& in_left)
	{
		AABB result;
		// Max Extent
		auto right_max = in_right.max_extent;
		auto left_max = in_left.max_extent;
		result.m_max_extent.x = (right_max.x > left_max.x) ? right_max.x : left_max.x;
		result.m_max_extent.y = (right_max.y > left_max.y) ? right_max.y : left_max.y;
		result.m_max_extent.z = (right_max.z > left_max.z) ? right_max.z : left_max.z;
		// Min Extent
		auto right_min = in_right.min_extent;
		auto left_min = in_left.min_extent;
		result.m_min_extent.x = (right_min.x < left_min.x) ? right_min.x : left_min.x;
		result.m_min_extent.y = (right_min.y < left_min.y) ? right_min.y : left_min.y;
		result.m_min_extent.z = (right_min.z < left_min.z) ? right_min.z : left_min.z;
		// Locals and Globals are the same
		result.min_extent = result.m_min_extent;
		result.max_extent = result.m_max_extent;
		return result;
	}
}
