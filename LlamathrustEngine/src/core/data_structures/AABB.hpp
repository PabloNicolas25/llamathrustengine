#pragma once
#include "core/Core.hpp"
#include <glm/glm.hpp>

namespace Llamathrust
{
	namespace DataStructures
	{
		struct LLAMATHRUST_API Ray
		{
			glm::vec3 origin;
			glm::vec3 inv_dig_direction;

			Ray(const glm::vec3& in_origin, const glm::vec3& in_dir)
				: origin(in_origin)
			{
				inv_dig_direction.x = 1.0f / in_dir.x;
				inv_dig_direction.y = 1.0f / in_dir.y;
				inv_dig_direction.z = 1.0f / in_dir.z;
			}
		};

		struct LLAMATHRUST_API AABB
		{

			AABB() = default;
			AABB(const glm::vec3& in_maxExtent, const glm::vec3& in_minExent)
				: m_max_extent(in_maxExtent), max_extent(in_maxExtent),
				min_extent(in_minExent), m_min_extent(in_minExent) {}

			inline glm::vec3 GetMaxExtent() const { return max_extent; }
			inline glm::vec3 GetMinExtent() const { return min_extent; }
			inline glm::vec3& GetMaxExtent() { return max_extent; }
			inline glm::vec3& GetMinExtent() { return min_extent; }
			inline glm::vec3 GetMaxExtentLocal() const { return m_max_extent; }
			inline glm::vec3 GetMinExtentLocal() const { return m_min_extent; }
			inline glm::vec3& GetMaxExtentLocal() { return m_max_extent; }
			inline glm::vec3& GetMinExtentLocal() { return m_min_extent; }
			// Get the volume of the AABB
			inline float AABB::GetVolume() const
			{
				auto diff = max_extent - min_extent;
				return diff.x * diff.y * diff.z;
			}
			// Get position, the center of AABB
			inline glm::vec3& AABB::GetPosition() const
			{
				return min_extent + (max_extent - min_extent) * 0.5f;
			}

			void Update(const glm::vec3& in_pos);
			
			// Returns true if the in_aabb is inside this aabb
			bool Contains(const AABB* in_aabb) const;
			// Returns true if the point is inside this aabb
			bool Contains(const glm::vec3& in_point) const;
			// Return true if the ray intersects this aabb
			bool RayIntersect(const Ray& in_ray) const;//, float t);

			static AABB Union(const AABB& in_right, const AABB& in_left);

		private:
			// Global extents
			glm::vec3 max_extent;
			glm::vec3 min_extent;

			// local extents seted once
			glm::vec3 m_max_extent;
			glm::vec3 m_min_extent;
		};
	}
}