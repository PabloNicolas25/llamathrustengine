#pragma once
#include "core/Core.hpp"
#include <unordered_set>
#include <GLM/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Llamathrust
{
	namespace DataStructures
	{
		template<class T>
		class LLAMATHRUST_API ktree;

		template<class T>
		class Node
		{
			friend class ktree<T>;

		protected:
#pragma warning(push)
#pragma warning(disable: 4251)
			glm::mat4 local_transform;
			glm::mat4 world_transform;

			Node* parent;
			std::unordered_set<Node*> children;
#pragma warning(pop)
		protected:
			Node(const glm::mat4& in_transform, Node* in_parent = nullptr)
				: local_transform(in_transform), parent(in_parent), world_transform(in_transform)
			{
				if (parent)
				{
					parent->children.emplace(this);
				}
			}
		public:
			inline glm::mat4 GetWorldTransform() const {
				return world_transform;
			}

			inline const glm::mat4* GetWorldTransformPtr() const {
				return &world_transform;
			}

			inline glm::mat4& GetLocalTransform() {
				return local_transform;
			}
		};

		template<class T>
		class LLAMATHRUST_API ktree
		{
		protected:
			T* root;

		public:
			ktree(const glm::vec3* scene_position = nullptr, glm::mat4* scene_rotation = nullptr)
			{
				glm::mat4 global_center;

				// Location
				global_center = (scene_position) ? glm::translate(glm::mat4(1.0f), *scene_position) : glm::mat4(1.0f);

				// Rotaiton
				if (scene_rotation)
					global_center = global_center * (*scene_rotation);

				// Root node alloc
				root = static_cast<T*>(new Node<T>(global_center));
			}


			// Add a new Node to the Tree and return such node.
			// transform: if parent the transform is local to it.
			T * ktree::AddNode(const glm::mat4 & in_transform, T * in_parent)
			{
				Node<T>* parent = (!in_parent) ? root : in_parent;
				
				T* node_model = new T(in_transform, nullptr, parent);

				computeWorldMatrix(node_model, node_model->parent->world_transform);

				return node_model;
			}


			// Set Parent to Node
			void SetParent(T* in_node, T* in_parent)
			{
				// if node has parent get node out of parent's children
				if (in_node->parent)
				{
					auto parent = in_node->parent;
					parent->children.erase(in_node);
				}

				// set double link
				in_node->parent = in_parent;
				in_parent->children.emplace(in_node);

				computeWorldMatrix(in_node, in_parent->world_transform);
			}

			// Delete the node in the scene
			// returns false if not found
			bool DeleteNode(T* in_node)
			{
				// delete from children list
				bool erased = in_node->parent->children.erase(in_node);
				// free memory
				delete in_node;
				// anullate pointer
				in_node = nullptr;

				return erased;
			}

			// Update input node's children
			virtual void Update(T* in_node = nullptr)
			{
				in_node = (in_node) ? in_node : root;

				if (in_node->parent)
					computeWorldMatrix(in_node, in_node->parent->world_transform);
				else
					computeWorldMatrix(in_node, glm::mat4(1.0f));
			}

		private:

			void computeWorldMatrix(T* in_node, glm::mat4& in_parentWorldMatrix)
			{
				glm::mat4 world_transform = in_parentWorldMatrix * in_node->local_transform;
				in_node->world_transform = world_transform;

				for (auto child : in_node->children)
				{
					computeWorldMatrix(static_cast<T*>(child), world_transform);
				}
			}

		};
	}
}