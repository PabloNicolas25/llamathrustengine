#pragma once
#include "Core.hpp"
#include "Window.hpp"
#include "core/loaders/AssetManager.hpp"
#include "core/LayerStack.hpp"
#include "core/events/ApplicationEvent.hpp"
#include "core/imgui/ImGuiLayer.hpp"
#include "renderer/Objects/Camera.hpp"
#include "renderer/Objects/Scene.hpp"
#include "renderer/Objects/Skybox.hpp"
#include <glm/glm.hpp>

namespace Llamathrust {

	/*
	This is the main app class.
	It is a singleton and is the owner of all the systems in the engine
	*/
	class LLAMATHRUST_API Application
	{

		static Application* s_Instance;

	public:
		Application(const char* title, int width, int height, glm::vec3& in_backColor);
		virtual ~Application();

		// Engine/App main loop
		void Run();

		// Resolves the events (events callback)
		void OnEvent(Event& e);

		// Returns the window reference
		inline Window* GetWindow() { return m_Window; }

		// Get the singleton instance
		inline static Application& Get() { return *s_Instance; }

		// Get video driver const pointer
		inline Graphics::RenderAPI* const GetVideoDriver() { return m_videoDriver; }

		// Get the app asset manager (singleton)
		Llamathrust::AssetManager* GetAssetManager() const { return Llamathrust::AssetManager::Get(); }

		// Close App
		inline void Close() { m_Running = false; }

		inline ImGuiContext* GetImGuiContext() { return m_ImGuiLayer->m_context; }

		// Get the application camera
		inline Llamathrust::Rendering::Camera* GetCamera() { return m_camera; }

		// Get the application view node
		inline Llamathrust::Rendering::SceneNode* GetViewNode() { return m_viewNode; }

		// Get the application Skybox
		inline Llamathrust::Rendering::Skybox* GetSkybox() { return m_skybox; }

		// Set the application Skybox
		inline void SetSkybox(Llamathrust::Rendering::Skybox* in_skybox) { m_skybox = in_skybox; }

		void PushLayer(Layer* layer);
		void PushOverlay(Layer* layer);
		
	private:

		// Unique pointer to the window
#pragma warning(suppress: 4251)
		Llamathrust::Window* m_Window;
		Graphics::RenderAPI* m_videoDriver = nullptr;
		// True if running / centinel of the main loop
		bool m_Running = true;

		// ImGuiLayer owned by the layerstack
		ImGuiLayer* m_ImGuiLayer;
		// Layer stack of the app
		LayerStack m_layerStack;

		Llamathrust::Rendering::Camera* m_camera;
		Llamathrust::Rendering::SceneNode* m_viewNode;
		Llamathrust::Rendering::Skybox* m_skybox;

		// Callback on window close (Called by OnEvent)
		bool OnWindowClose(WindowCloseEvent& e);
		// Callback on window resize
		bool Application::OnWindowResize(WindowResizeEvent& e);
	};

	// To be defined in client
	Application* CreateApplication();

}

