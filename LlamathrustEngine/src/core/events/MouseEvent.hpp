#pragma once

#include "..\Core.hpp"
#include "Event.hpp"

namespace Llamathrust
{
	class LLAMATHRUST_API MouseMovedEvent : public Event
	{
		float m_MouseX;
		float m_MouseY;

	public:
		MouseMovedEvent(float in_x, float in_y)
			: m_MouseX(in_x), m_MouseY(in_y)
		{}

		inline float GetX() const { return m_MouseX; }
		inline float GetY() const { return m_MouseY; }
		inline glm::vec2 GetPosition() const { return glm::vec2(m_MouseX, m_MouseY); }

		EVENT_CLASS_TYPE_FUNCS(MouseMoved)
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
	};

	class LLAMATHRUST_API MouseScrolledEvent : public Event
	{
		float m_XOffset;
		float m_YOffset;

	public:
		MouseScrolledEvent(float xOffset, float yOffset)
			: m_XOffset(xOffset), m_YOffset(yOffset) {}

		inline float GetXOffset() const { return m_XOffset; }
		inline float GetYOffset() const { return m_YOffset; }

		EVENT_CLASS_TYPE_FUNCS(MouseScrolled)
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
	};

	class LLAMATHRUST_API MouseButtonEvent : public Event
	{
	protected:
		int m_Button;

		MouseButtonEvent(int button) : m_Button(button) {}

	public:
		inline int GetMouseButton() const { return m_Button; }

		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
	};

	class LLAMATHRUST_API MouseButtonPressedEvent : public MouseButtonEvent
	{
		int m_RepeatCount;

	public:
		MouseButtonPressedEvent(int button, int repeatCount)
			: MouseButtonEvent(button), m_RepeatCount(repeatCount) {}

		inline int GetRepeatCount() const { return m_RepeatCount; }

		EVENT_CLASS_TYPE_FUNCS(MouseButtonPressed)
	};

	class LLAMATHRUST_API MouseButtonReleasedEvent : public MouseButtonEvent
	{
	public:
		MouseButtonReleasedEvent(int button)
			: MouseButtonEvent(button) {}


		EVENT_CLASS_TYPE_FUNCS(MouseButtonReleased)
	};
}