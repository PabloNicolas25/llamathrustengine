#pragma once

#include "..\Core.hpp"
#include "Event.hpp"

namespace Llamathrust
{
	class LLAMATHRUST_API KeyEvent : public Event
	{
	protected:
		int m_KeyCode;

		KeyEvent(int keycode) : m_KeyCode(keycode) {}

	public:
		inline int GetKeyCode() const { return m_KeyCode; }

		EVENT_CLASS_CATEGORY(EventCategoryKeyboard | EventCategoryInput)
	};

	class LLAMATHRUST_API KeyPressedEvent : public KeyEvent
	{
		int m_RepeatCount;

	public:
		KeyPressedEvent(int keycode, int repeatCount)
			: KeyEvent(keycode), m_RepeatCount(repeatCount) {}
		
		inline int GetRepeatCount() const { return m_RepeatCount; }

		EVENT_CLASS_TYPE_FUNCS(KeyPressed)
	};

	class LLAMATHRUST_API KeyReleasedEvent : public KeyEvent
	{
	public:
		KeyReleasedEvent(int keycode)
			: KeyEvent(keycode) {}


		EVENT_CLASS_TYPE_FUNCS(KeyReleased)
	};

	class LLAMATHRUST_API KeyTypedEvent : public KeyEvent
	{
	public:
		KeyTypedEvent(int keycode)
			: KeyEvent(keycode) {}

		EVENT_CLASS_TYPE_FUNCS(KeyTyped)
	};
}