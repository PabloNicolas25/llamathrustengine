#pragma once

#include "..\Core.hpp"
#include "Event.hpp"

namespace Llamathrust
{
	class LLAMATHRUST_API WindowCloseEvent : public Event
	{
	public:
		WindowCloseEvent() {}

		EVENT_CLASS_TYPE_FUNCS(WindowClose)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};

	class LLAMATHRUST_API WindowResizeEvent : public Event
	{
		int m_Width;
		int m_Height;
	public:
		WindowResizeEvent(int width, int height)
		: m_Width(width), m_Height(height) {}

		inline int GetWindowHeight() const { return m_Height; }
		inline int GetWindowWidth() const { return m_Width; }

		EVENT_CLASS_TYPE_FUNCS(WindowResize)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};
}