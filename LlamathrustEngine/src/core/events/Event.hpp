#pragma once

#include "..\core.hpp"
#include <string>
#include <functional>

namespace Llamathrust
{
	// Scoped enum. Must be accessed as 'EventType::event'
	// Types of events
	enum class EventType
	{
		None = 0,
		WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
		AppTick, AppUpdate, AppRender,
		KeyPressed, KeyReleased, KeyTyped,
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
	};

	// The macro bit is defined as 'BIT(x) (1 << x)'
	// The category of each event
	enum EventCategory
	{
		None = 0,
		EventCategoryApplication	= BIT(0),
		EventCategoryInput			= BIT(1),
		EventCategoryKeyboard		= BIT(2),
		EventCategoryMouse			= BIT(3),
		EventCategoryMouseButton	= BIT(4)
	};

	// Since this functions are needed for every event to know the info at runtime
	// We use this macro to avoid writing repeating code
	// The '#' operator enclose the result in quotes creating a string literal adding the backlashes where necesary,
	// The '##' operator: This operation is called "concatenation" or "token pasting". Only tokens that form a valid token together may be pasted.
#define EVENT_CLASS_TYPE_FUNCS(type)\
	static EventType GetStaticEventType() { return EventType::##type; } \
	virtual EventType GetEventType() const override{ return GetStaticEventType(); } \
	virtual const char* GetName() const override { return #type; }

#define EVENT_CLASS_CATEGORY(category) virtual int GetCategoryFlags() const override { return category; }

	// Base interface of an event
	struct LLAMATHRUST_API Event
	{
		bool Handled = false;
		virtual EventType GetEventType() const = 0;
		virtual const char* GetName() const = 0;
		virtual int GetCategoryFlags() const = 0;
		virtual std::string ToString() const { return GetName(); }

		inline bool IsInCategory(EventCategory category)
		{
			return GetCategoryFlags() & category;
		}
	};

	/*
		The event dispatcher is the class that makes the call to the callback
		when the event is retrieve from the system.
		Think of it as a helper callback caller.
	*/
	class EventDispatcher
	{
		// returns a boolean and has a T& param
		template<typename T>
		using EventFn = std::function<bool(T&)>;
	
		// event to dispatch
		Event& m_event;
	public:
		EventDispatcher(Event& event) : m_event(event) {}

		template<typename T>
		bool Dispatch(EventFn<T> func)
		{
			// if the event is of the type of the typename then is handled
			if (m_event.GetEventType() == T::GetStaticEventType())
			{
				// pass a reference of the event as a T-type event and return if handled
				m_event.Handled = func(*(T*)&m_event);
				return true;
			}
			return false;
		}
	};

}