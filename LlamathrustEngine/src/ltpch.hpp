#pragma once

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <functional>
#include <thread>
#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>
#include <glad/glad.h>
#include "core/Log.hpp"

#ifdef LT_PLATFORM_WINDOWS
	#include <Windows.h>
	#include <d3d11.h>
#endif