#pragma once

#include "core/Core.hpp"
#include "core/graphics/Graphics.hpp"
#include "Objects/Camera.hpp"
#include "Objects/Model.hpp"
#include "Objects/Texture2D.hpp"
#include "Objects/Mesh.hpp"
#include "Objects/Material.hpp"
#include "Objects/RenderTarget.hpp"
#include "Objects/Cubemap.hpp"
#include <vector>
#include <map>
#include <set>

namespace Llamathrust
{
	namespace DataStructures
	{
		struct AABB;
	}

	namespace Rendering
	{
		class LLAMATHRUST_API RenderDevice
		{

			friend struct Graphics::RenderAPI;

			static RenderDevice* s_instance;
		protected:
			std::vector<Graphics::ShaderProgram*> m_shaders;

			std::vector<Mesh*> m_debugMeshes;
			Material* m_debugMat = nullptr;
		public:
			enum DebugColors : uint_32
			{
				TREE_BRANCH,
				TREE_LEAF,
				HIT_POINTS,
				RAY
			};

		public:
			static RenderDevice* Get() { return s_instance; }

			// Height and Width is ignored since the loader fill them for you
			virtual Texture2D* CreateTexture2D(const char* in_filePath, Texture_Desc& in_textureDesc) = 0;
			
			virtual Cubemap* CreateCubemap(std::vector<std::string>& in_filePaths, Texture_Desc& in_textureDesc) = 0;

			virtual Mesh* CreateMesh(const std::vector<Vertex>& in_vertices, const std::vector<uint_32>& in_indices,
				DataStructures::AABB* in_aabb = nullptr, const DRAW_MODE in_mode = TRIANGLES) = 0;

			virtual Graphics::Shader* CreateShader(const Graphics::SHADER_TYPE in_type, const char* in_fileName) = 0;

			virtual RenderTarget* CreateRenderTarget(const RenderTarget::BUFFER_TYPE in_type) = 0;

			void Draw(const glm::mat4& PVmat, 
				std::vector<std::vector<std::pair<Material*, std::vector<Mesh*>* > >* >& in_materialsAndModels,
				const std::vector<Mesh*>& in_models);

			Material* CreateMaterial(const MATERIAL_TYPE in_type);


			// Call in main thread only (opengl context owner)
			void GenerateShaders(const std::set<MATERIAL_TYPE>& in_types);

			void DrawAABB(const DataStructures::AABB& in_aabb);
			
			virtual Graphics::ShaderProgram* CreateShaderProgram(const char* in_vertexName, const char* in_pixelName) = 0;
		
		protected:
			RenderDevice();


			virtual Graphics::ShaderProgram* CreateShaderProgram(Graphics::Shader* in_vertexShader, Graphics::Shader* in_pixelShader) = 0;
		};
	}
}
