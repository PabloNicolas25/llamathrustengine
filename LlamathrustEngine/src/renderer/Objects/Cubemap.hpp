#pragma once
#include "core/Core.hpp"
#include "Texture2D.hpp"
#include <initializer_list>

namespace Llamathrust
{
	namespace Rendering
	{
		struct LLAMATHRUST_API CubemapFace
		{
			using CUBEMAP_FACE = TEXTURE_TARGET;
			CUBEMAP_FACE face;
			Texture2D* texture;
		};

		class LLAMATHRUST_API Cubemap : public Texture
		{
		protected:
			Texture2D* m_data[6];
		public:
			Cubemap(const std::initializer_list<CubemapFace>& in_faces);
			~Cubemap();

			/* Get platform texture object */
			virtual void* Get() override = 0;

			/* Use or Bind this Texture object */
			virtual void Use(uint_32 in_textureUnit = 0) override = 0;

			/* Assign data to this Texture object */
			virtual void SetData(byte* in_byteArray) override = 0;

			/* Upload data to the GPU */
			virtual void UploadData() override = 0;

			/* Generate Texture and UploadData */
			virtual void LoadGPU() override = 0;
		};
	}
}
