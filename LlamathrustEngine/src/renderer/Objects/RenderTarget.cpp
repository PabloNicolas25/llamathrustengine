#include "ltpch.hpp"
#include "RenderTarget.hpp"

namespace Llamathrust::Rendering
{
	RenderTarget::RenderTarget(RenderTarget::BUFFER_TYPE in_type)
		: m_type(in_type) {}

	RenderTarget::~RenderTarget()
	{
		delete m_texture;
	}
	Texture2D* RenderTarget::GetTexture()
	{
		return m_texture;
	}
}
