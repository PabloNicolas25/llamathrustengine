#include "ltpch.hpp"
#include "Material.hpp"

namespace Llamathrust::Rendering
{
	Material::Material(Graphics::ShaderProgram** in_shaderProgram, const MATERIAL_TYPE in_type)
		: m_type(in_type), m_program(in_shaderProgram), m_diffuseColor(Graphics::Color(0.8f, 0.8f, 0.8f, 1.0f)),
		m_diffuseTex(nullptr), m_skyboxTex(nullptr), m_viewProps(nullptr)
	{
		m_drawCallListener = DrawCallListener::Get();
	}

	void Material::Use()
	{
		(*m_program)->Use();
		m_drawCallListener->Notify();
	}

	void Material::Draw()
	{
		if (m_type & TEXTURED)
			m_diffuseTex->Use(0);
		else
			(*m_program)->SetVec3("color", m_diffuseColor.vector);
		if (m_type & REFLECTIVE)
		{
			(*m_program)->SetFloat("reflection_factor", m_reflection_factor);
			m_skyboxTex->Use(1);
			(*m_program)->SetVec3("camPos", m_viewProps->m_position);
		}
	}

	void Material::SetReflection(Cubemap* in_cubemap, ViewProperties* in_viewMat)
	{
		LT_CORE_ASSERT(in_viewMat != nullptr, "view matrix is null!");
		m_skyboxTex = in_cubemap;
		m_viewProps = in_viewMat;
	}
}
