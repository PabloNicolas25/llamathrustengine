#pragma once
#include "core/Core.hpp"
#include "core/graphics/GBuffer.hpp"
#include "core/data_structures/AABB.hpp"
#include <glm/glm.hpp>
#include <vector>

namespace  Llamathrust
{
	namespace Rendering
	{
		enum LLAMATHRUST_API DRAW_MODE
		{
			TRIANGLES,
			LINES
		};
#pragma warning(push)
#pragma warning(disable: 4251)
		struct LLAMATHRUST_API Vertex
		{
			glm::vec3 position;
			glm::vec3 normal;
			glm::vec2 uvcoord;

			Vertex() = default;
			Vertex(const glm::vec3& in_pos) : position(in_pos), normal(0.0f, 0.0f, 0.0f), uvcoord(0.0f, 0.0f) {}
			Vertex(const glm::vec3& in_pos, const glm::vec3& in_normal) : position(in_pos), normal(in_normal), uvcoord(0.0f, 0.0f) {}
			Vertex(const glm::vec3& in_pos, const glm::vec2& in_uvCoord) : position(in_pos), normal(0.0f, 0.0f, 0.0f), uvcoord(in_uvCoord) {}
		};

		class LLAMATHRUST_API Mesh
		{
		protected:
			// Buffers
			std::vector<Vertex> m_vertices;
			std::vector<uint_32> m_indices;
			DataStructures::AABB *m_aabb;
			// GPU
			DRAW_MODE m_mode;
			Graphics::VertexBuffer *m_vertexBuffer = nullptr;
			Graphics::IndexBuffer *m_indexBuffer = nullptr;
		public:
			// aabb pointer is owned by Mesh
			Mesh(const std::vector<Vertex>& in_vertices, const std::vector<uint_32>& in_indices,
				DataStructures::AABB* in_aabb, const DRAW_MODE in_mode)
				: m_mode(in_mode), m_aabb(in_aabb), m_vertices(in_vertices), m_indices(in_indices) {}

			~Mesh()
			{
				delete m_indexBuffer;
				delete m_vertexBuffer;
				delete m_aabb;
			}

			// Send the mesh into the GPU
			virtual void LoadGPU() = 0;

			// Delete the mesh from the GPU
			virtual void UnloadGPU() const = 0;

			// Must be called before 'Draw'
			virtual void Bind() const = 0;

			// Draw instaces_count instances of this mesh
			virtual void Draw(uint_32 instance_count = 1) const = 0;

			// Get the AABB pointer
			DataStructures::AABB* GetAABB() { return m_aabb; }
			// Get the AABB pointer
			const DataStructures::AABB* GetAABB() const { return m_aabb; }

			bool HasAABB() { return m_aabb != nullptr; }

			std::vector<Vertex>& GetVertices() { return m_vertices; }
			const std::vector<Vertex>& GetVertices() const { return m_vertices; }
			const std::vector<uint_32>& GetIndices() { return m_indices; }
		};
#pragma warning(pop)
	}
}
