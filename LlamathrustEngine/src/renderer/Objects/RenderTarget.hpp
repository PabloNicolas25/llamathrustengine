#pragma once
#include "core/Core.hpp"
#include "Texture2D.hpp"

namespace Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API RenderTarget
		{
		public:
			enum BUFFER_TYPE : byte
			{
				COLOR = 1,
				DEPTH = COLOR << 1,
				STENCIL = DEPTH << 1
			};
		protected:
			RenderTarget::BUFFER_TYPE m_type;
			// This texture should be intialized by the
			// derived class
			Texture2D* m_texture;
		public:
			RenderTarget(RenderTarget::BUFFER_TYPE in_type);
			virtual ~RenderTarget();
			
			virtual void ReCreate(RenderTarget::BUFFER_TYPE in_type) = 0;
			virtual void CreateTargetAndSetTexture(Texture2D* in_texture) = 0;
			virtual void Bind() = 0;
			virtual void Unbind() = 0;
			
			RenderTarget::BUFFER_TYPE GetType() const { return m_type; }

			Texture2D* GetTexture();
		};
	}
}