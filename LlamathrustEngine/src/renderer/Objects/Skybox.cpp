#include "ltpch.hpp"
#include "Skybox.hpp"
#include "renderer/RenderDevice.hpp"
#include "core/Application.hpp"

Llamathrust::Rendering::Skybox::Skybox(Llamathrust::Rendering::Cubemap* in_cube)
	: m_cubemap(in_cube)
{
	m_shader = RenderDevice::Get()->CreateShaderProgram("../res/shaders/skybox", "../res/shaders/skybox");
	m_shader->Create();
	m_shader->Use();
	m_shader->SetInt("diffuse", 0);

	m_model = Application::Get().GetAssetManager()->LoadModel("../res/models/cube.mudm", "skybox");
	m_model->LoadGPU();
}

Llamathrust::Rendering::Skybox::~Skybox()
{
	delete m_cubemap;
	delete m_shader;
	delete m_model;
}

void Llamathrust::Rendering::Skybox::Draw(const glm::mat4& in_projection, const glm::mat4& in_view)
{
	m_shader->Use();
	m_shader->SetMat4("projection", in_projection);
	m_shader->SetMat4("view", in_view);
	m_cubemap->Use();
	m_model->Draw();
}

Llamathrust::Rendering::Cubemap* Llamathrust::Rendering::Skybox::GetCubemap()
{
	return m_cubemap;
}
