#pragma once

#include "core/Core.hpp"


namespace Llamathrust
{
	namespace Rendering
	{
		enum LLAMATHRUST_API IMAGE_FORMAT
		{
			RGB_24,
			RGBA_32,
		};

		enum LLAMATHRUST_API TEXTURE_TARGET : int
		{
			TEXTURE_2D,
			CUBEMAP_FACE_POSITIVE_X,
			CUBEMAP_FACE_NEGATIVE_X,
			CUBEMAP_FACE_POSITIVE_Y,
			CUBEMAP_FACE_NEGATIVE_Y,
			CUBEMAP_FACE_POSITIVE_Z,
			CUBEMAP_FACE_NEGATIVE_Z,
			CUBEMAP
		};

		struct LLAMATHRUST_API Texture_Desc
		{
			// Width of the image
			int Width;
			// Height of the image
			int Height;
			// If true then mipmaps are generated for this texture
			bool MipLevels;
			// Format of the image
			IMAGE_FORMAT Format;
			// Texture target (Texture2D, CubeMapFace)
			TEXTURE_TARGET Target;
		};

		class LLAMATHRUST_API Texture
		{
		protected:
			Texture_Desc* m_description;
			bool loaded = false;
		public:
			Texture(Texture_Desc* in_description)
			 : m_description(in_description) {}

			/* Get platform texture object */
			virtual void* Get() = 0;

			/* Use or Bind this Texture object */
			virtual void Use(uint_32 in_textureUnit = 0) = 0;

			/* Assign data to this Texture object */
			virtual void SetData(byte* in_byteArray) = 0;

			/* Upload data to the GPU */
			virtual void UploadData() = 0;

			/* Generate Texture and UploadData */
			virtual void LoadGPU() = 0;

			const Texture_Desc* GetDescription() const {
				return m_description;
			}

			Texture_Desc* GetDescription() {
				return m_description;
			}
		};
	}
}