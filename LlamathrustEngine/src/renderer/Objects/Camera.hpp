#pragma once
#include "core/Core.hpp"
#include <glm/glm.hpp>

namespace Llamathrust
{
	namespace Rendering
	{
		// This struct is for generating the view matrix
		// So it could be in any object POV
		class LLAMATHRUST_API ViewProperties
		{
#pragma warning(push)
#pragma warning(disable: 4251)
			// Referenc attr
			glm::vec3& m_worldUp;

			// Matrix stored to save racalculate it
			glm::mat4 m_viewMatrix;

		public:
			// Orientation attr
			float m_yaw;
			float m_pitch;

			// Reference to the node position
			glm::vec3& m_position;

			// Axes attr
			glm::vec3 m_front;
			glm::vec3 m_right;
			glm::vec3 m_up;
#pragma warning(pop)


		public:
			ViewProperties(glm::vec3& in_position = glm::vec3(0, 0, 0), float in_pitch = 0, float in_yaw = 0, glm::vec3& in_worldUp = glm::vec3(0, 1, 0));

			glm::mat4 GetViewMatrix() const;
			

			void SetRotation(float in_pitch, float in_yaw);

			void SetPosition(glm::vec3& in_position);

		private:
			// Calculates the vectors from the updated Euler Angles
			void updateCameraVectors();
		};

		class LLAMATHRUST_API Camera
		{
#pragma warning(suppress: 4251)
			union
			{
				glm::mat4 m_projectionMatrix;
				glm::mat4 m_projectionMatrixOrtho;
			};
			float m_fov;
			float m_aspect;
			float m_maxDistance;
			float m_minDistance;

			bool ortho = false;

		public:
			Camera(float in_fov, float in_maxDist, float in_minDist, float in_aspect = 16.0f / 9.0f);
			Camera(float in_width, float in_height);

			glm::mat4& GetProjectionMatrix();
			

			glm::mat4& RecalculateProjectionMatrix(float in_fov, float in_maxDist, float in_minDist, float in_aspect = 16.0f / 9.0f);
			glm::mat4& RecalculateProjectionMatrix(float in_width, float in_height);

			float GetFieldOfView();

			float GetMaxDistance();

			float GetMinDistance();

			float GetAspectRatio();
		};

	}
}