#pragma once
#include <core/Core.hpp>
#include <glm/glm.hpp>
#include "core/data_structures/k_tree.hpp"
#include "core/data_structures/AABB.hpp"
#include "core/data_structures/AABB_tree.hpp"
#include "Model.hpp"
#include "Camera.hpp"
#include "../RenderDevice.hpp"
#include "Material.hpp"
#include <set>

namespace Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API ViewProperties;

#pragma warning(push)
#pragma warning(disable: 4251) // This suppress the warning of dll export for struct members
		class LLAMATHRUST_API SceneNode : public DataStructures::Node<SceneNode>
		{
			// Usually a model contained by this node
			void* m_user;

			// Name of the node
			const char* m_name;

			// Not null if it is a POV node
			ViewProperties* m_POVprops = nullptr;
#pragma warning(pop)
		public:
			// If set POV properties they pass the ownership to the node!!
			SceneNode(const glm::mat4& in_transform,
				void* in_user = nullptr, Node* in_parent = nullptr,
				ViewProperties* in_POVprops = nullptr);
			~SceneNode();

		public:

			void SetUser(void* in_user);

			ViewProperties* GetViewProps();

			// This pass the ViewProperties ownership to the node!!
			void SetAsViewNode(ViewProperties* in_viewProps);

			void UnsetAsViewNode();

			bool IsViewNode() const;

			void SetName(const char* in_name) { m_name = in_name; }
			const char* GetName() { return m_name; }
		};


		class LLAMATHRUST_API SceneGraph : public DataStructures::ktree<SceneNode>
		{
		public:
			SceneGraph();

			// Add a new Node to the Tree and return such node.
			// transform: if parent the transform is local to it.
			SceneNode* AddNode(const glm::mat4& in_transform, void* in_user = nullptr, SceneNode* in_parent = nullptr);
		};

		struct LLAMATHRUST_API RayCastHitInfo
		{
			glm::vec3 hit_position;
			glm::vec3 hit_normal;
			SceneNode* node = nullptr;
		};

		class LLAMATHRUST_API Scene
		{
			// SceneGraph (scene hierarchy)
			SceneGraph m_sceneGraph;
			// Tree used for ray casting
			DataStructures::AABBTree m_aabbTree;

			// Materials used in the scene grouped by type
			typedef std::vector<Model*> Models;
			typedef std::vector<Mesh*> Meshes;
			typedef std::pair < Material*, Meshes* > Bind;
			typedef std::vector<Bind> MaterialGroup;
			// MaterialType::COUNT Material groups
			std::vector<MaterialGroup*> m_materials;
			std::set<MATERIAL_TYPE> m_types;

			//Models without materials
			Meshes m_noMaterialModels;
			
			// Models used in this scene
			Models m_models;

			// Projection Matrices
			std::vector<Camera*> m_cameras;
			
			// View nodes in the scene (look matrices)
			std::vector<SceneNode*> m_viewNodes;
			
			
			// Active scene view node
			SceneNode* m_activeViewNode = nullptr;
			
			// Active camera
			Camera* m_activeCamera;

			RenderDevice* m_videoDriver = nullptr;

		public:
			Scene(Camera* in_camera = nullptr, ViewProperties* in_defaultView = nullptr);
			~Scene() = default;

			// Call this method to update hierarchy node
			// leave parameter null to update whole scene (not recomended)
			// Called multiple times
			void Update(SceneNode* in_node = nullptr);
			
			// Draw this scene
			void Draw();

			// Load this scene into the GPU
			void LoadGPU();

			Camera* GetActiveCamera() { return m_activeCamera; }
			SceneNode* GetActiveView() { return m_activeViewNode; }
			
			void AddCamera(Camera* in_camera) { m_cameras.emplace_back(in_camera); }
			void AddCameraSetActive(Camera* in_camera) { AddCamera(in_camera); m_activeCamera = in_camera; }
			
			SceneNode* AddViewNode(ViewProperties* in_viewProps);
			SceneNode* AddViewNodeSetActive(ViewProperties* in_viewProps) { return m_activeViewNode = AddViewNode(in_viewProps); }
			
			void ReplaceViewNode(SceneNode* in_viewNode, uint_32 in_index);
			void ReplaceViewNodeSetActive(SceneNode* in_viewNode, uint_32 in_index);
			
			SceneNode* AddModel(Model* in_model, const glm::mat4& in_transform, SceneNode* parent = nullptr, const char* in_node_name = nullptr);
			SceneNode* AddEmpty(const glm::mat4& in_transform, SceneNode* parent = nullptr);

			Material* CreateMaterial(const MATERIAL_TYPE in_type);
			
			/* Creates a material of type in_type and binds it to in_model
			*	If model is nullptr or not in scene returns nullptr
			*/
			Material* CreateMaterialAndBind(const MATERIAL_TYPE in_type, Model* in_model);
			void BindMaterialToModel(Material* in_material, Model* in_model);
			void BindMaterialsToModel(Model* in_model, std::initializer_list<Material*> in_materials);
			void BindMaterialToMesh(Material* in_material, Mesh* in_mesh);
			
			/* Get the vector to where the mouse is pointing to in the scene
			*	OutInfo: 3D vector
			*	Hint: Used for ray::in_to
			*/
			void GetPickRay(glm::vec2& in_mousePosition, glm::vec3& out_pickRay);

			/* Cast a ray through the scene from in_from to in_to
			*	Return true if hit any node.
			*	OutInfo: RayCastHitInfo object
			*/
			bool RayCast(glm::vec3& in_from, glm::vec3& in_to, RayCastHitInfo& out_hitInfo);
		};
	}
}