#pragma once

#include "core/Core.hpp"
#include "Texture.hpp"

namespace Llamathrust
{
	namespace Rendering
	{		
		class LLAMATHRUST_API Texture2D : public Texture
		{
		protected:
			byte* m_data = nullptr;
		public:
			Texture2D(Texture_Desc* in_description) : Texture(in_description) {}

			/* Get platform texture object */
			virtual void* Get() override = 0;

			/* Use or Bind this Texture object */
			virtual void Use(uint_32 in_textureUnit = 0) override = 0;

			/* Assign data to this Texture object */
			virtual void SetData(byte* in_byteArray) override = 0;

			/* Upload data to the GPU */
			virtual void UploadData() override = 0;

			/* Generate Texture and UploadData */
			virtual void LoadGPU() override = 0;
		};
	}
}