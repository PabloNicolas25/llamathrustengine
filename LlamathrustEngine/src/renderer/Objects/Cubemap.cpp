#include "ltpch.hpp"
#include "Cubemap.hpp"
namespace Llamathrust::Rendering
{
	Cubemap::Cubemap(const std::initializer_list<CubemapFace>& in_faces)
		: Texture(new Texture_Desc())
	{
		m_description->MipLevels = 0;
		m_description->Target = TEXTURE_TARGET::CUBEMAP;

		for (auto& face : in_faces)
		{
			face.texture->GetDescription()->Target = face.face;
			m_data[face.face - 1] = face.texture;
		}
	}
	Cubemap::~Cubemap()
	{
		if (m_data)
		{
			for (int i = 0; i < 6; i++)
				if (m_data[i])
					delete m_data[i];
			delete[] m_data;
		}
	}
}
