#include "ltpch.hpp"
#include "Model.hpp"
#include "../RenderDevice.hpp"
#include "core/data_structures/AABB.hpp"

namespace Llamathrust::Rendering
{
	Model::Model(MUDLoader::Model* in_mudModel)
		: m_aabb(nullptr)
	{
		for (auto mesh : in_mudModel->meshes)
		{
			std::vector<Vertex> vertices;
			std::vector<uint_32> indices;
			
			for (auto vertex : mesh.vertices)
			{
				Vertex lt_vertex;
				
				lt_vertex.position = vertex.pos;
				lt_vertex.normal= vertex.normal;
				lt_vertex.uvcoord = vertex.uvCoord;

				vertices.emplace_back(lt_vertex);
			}

			for (auto index : mesh.indices)
			{
				indices.emplace_back(index);
			}
			
			DataStructures::AABB* aabb = nullptr;
			if (mesh.hasAABB)
				aabb = new DataStructures::AABB(*reinterpret_cast<DataStructures::AABB*>(&mesh.aabb));
			m_meshes.emplace_back(RenderDevice::Get()->CreateMesh(vertices, indices, aabb, Rendering::DRAW_MODE::TRIANGLES));
		}

		RecalculateAABB();
		delete in_mudModel;
		in_mudModel = nullptr;
	}

	Model::Model(const std::initializer_list<Mesh*>& in_meshes)
		: m_aabb(nullptr)
	{
		for (auto mesh : in_meshes)
			m_meshes.emplace_back(mesh);
	}

	Model::Model(const std::vector<Vertex>& in_vertices, const std::vector<uint_32>& in_indices, const DRAW_MODE in_drawMode)
		: m_aabb(nullptr)
	{
		m_meshes.emplace_back(RenderDevice::Get()->CreateMesh(in_vertices, in_indices, nullptr, in_drawMode));
	}

	void Model::Draw() const
	{
		for (auto mesh : m_meshes)
		{
			// bind the mesh
			mesh->Bind();
			// work with mesh
			// draw the mesh
			mesh->Draw();
		}
	}

	void Model::LoadGPU()
	{
		for (auto mesh : m_meshes)
			mesh->LoadGPU();
	}

	void Model::SetTransform(const glm::mat4* in_transform)
	{
		m_transform = in_transform;
	}

	void Model::RecalculateAABB()
	{
		DataStructures::AABB aabb;
		bool has = false;

		for (auto mesh : m_meshes)
		{
			if (!mesh->HasAABB())
				continue;
			if (!has)
			{
				aabb = DataStructures::AABB(*mesh->GetAABB());
				has = true;
				continue;
			}
			aabb = DataStructures::AABB::Union(aabb, *mesh->GetAABB());
		}
		if (has)
			m_aabb = new DataStructures::AABB(aabb);
	}
}
