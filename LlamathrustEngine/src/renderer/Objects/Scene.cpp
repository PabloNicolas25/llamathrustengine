#include "ltpch.hpp"
#include "Scene.hpp"
#include "core/Application.hpp"
#include <glm/gtc/matrix_transform.hpp>

namespace Llamathrust::Rendering
{
	SceneNode::SceneNode(const glm::mat4& in_transform, void* in_user, Node* in_parent, ViewProperties* in_POVprops)
		: DataStructures::Node<SceneNode>(in_transform, in_parent), m_POVprops(in_POVprops), m_user(in_user)
	{
	}

	SceneNode::~SceneNode()
	{
		UnsetAsViewNode();
	}

	void SceneNode::SetUser(void * in_user)
	{
		m_user = in_user;
	}
	
	ViewProperties * SceneNode::GetViewProps()
	{
		return m_POVprops;
	}
	
	void SceneNode::SetAsViewNode(ViewProperties * in_viewProps)
	{
		if (m_POVprops)
			delete m_POVprops;
		m_POVprops = in_viewProps;
	}

	void SceneNode::UnsetAsViewNode()
	{
		if (!m_POVprops) return;

		delete m_POVprops;
		m_POVprops = nullptr;
	}

	bool SceneNode::IsViewNode() const
	{
		return m_POVprops != nullptr;
	}
	
	SceneGraph::SceneGraph()
		: ktree()
	{
	}
	
	SceneNode * SceneGraph::AddNode(const glm::mat4 & in_transform, void * in_user, SceneNode * in_parent)
	{
		SceneNode* node = ktree::AddNode(in_transform, in_parent);
		node->SetUser(in_user);
		return node;
	}
	
	Scene::Scene(Camera* in_camera, ViewProperties* in_defaultView) : m_activeCamera(in_camera)
	{
		// Set default view
		if (in_defaultView)
		{
			m_activeViewNode = m_sceneGraph.AddNode(glm::mat4(1.0f));
			m_activeViewNode->SetAsViewNode(in_defaultView);
			m_viewNodes.emplace_back(m_activeViewNode);
		}

		if (in_camera)
			m_cameras.emplace_back(in_camera);

		m_videoDriver = Llamathrust::Rendering::RenderDevice::Get();

		//  Reserve MATERIAL_TYPE::COUNT places for materials vectors
		m_materials.reserve(MATERIAL_TYPE::COUNT);
		for (size_t i = 0; i < m_materials.capacity(); i++)
		{
			m_materials.emplace_back(new MaterialGroup());
		}
	}
	
	void Scene::Update(SceneNode* in_node)
	{
		m_aabbTree.Update();
		m_sceneGraph.Update(in_node);
	}

	void Scene::Draw()
	{
		if (m_models.size() == 0)
			return;
		
		// Draw AABBs and branches AABBs
		m_aabbTree.Draw();

		glm::mat4 PVmat;
		
		if (m_activeViewNode)
			PVmat = m_activeCamera->GetProjectionMatrix() * m_activeViewNode->GetViewProps()->GetViewMatrix();
		else
			PVmat = m_activeCamera->GetProjectionMatrix();
		m_videoDriver->Draw(PVmat, m_materials, m_noMaterialModels);
	}

	void Scene::LoadGPU()
	{
		m_videoDriver->GenerateShaders(m_types);

		for (auto group : m_materials)
		{
			if (group->size())
				(*group)[0].first->LoadGPU();
		}

		for (auto model : m_models)
			model->LoadGPU();
	}

	// Add & replace nodes
	SceneNode* Scene::AddViewNode(ViewProperties* in_viewProps)
	{
		SceneNode* node = m_sceneGraph.AddNode(glm::mat4(1.0f));
		node->SetAsViewNode(in_viewProps);
		m_viewNodes.emplace_back(node);
		return node;
	}
	
	void Scene::ReplaceViewNode(SceneNode* in_viewNode, uint_32 in_index)
	{
		LT_ASSERT(in_viewNode->IsViewNode(), "The node isn't a view node!");
		auto index = m_viewNodes.begin() + in_index;
		if (m_viewNodes.size() > 0)
			m_viewNodes.erase(index);
		
		// if the index is not in the vector
		if (m_viewNodes.size() > in_index)
			m_viewNodes.insert(index, in_viewNode);
		// the add it to the back
		else
			m_viewNodes.emplace_back(in_viewNode);
	}
	
	void Scene::ReplaceViewNodeSetActive(SceneNode* in_viewNode, uint_32 in_index)
	{
		ReplaceViewNode(in_viewNode, in_index);
		m_activeViewNode = m_viewNodes.at(in_index);
	}
	
	// Node types
	SceneNode* Scene::AddModel(Model* in_model, const glm::mat4& in_transform, SceneNode* parent, const char* in_node_name)
	{
		// TODO
		m_models.emplace_back(in_model);
		for (auto mesh : in_model->GetMeshes())
			m_noMaterialModels.emplace_back(mesh);
		auto node = m_sceneGraph.AddNode(in_transform, (void*) in_model, parent);
		node->SetName(in_node_name);
		
		const glm::mat4* worldTransform = node->GetWorldTransformPtr();
		in_model->SetTransform(worldTransform);

		if (in_model->HasAABB())
			m_aabbTree.AddAABB(in_model->GetAABB(), node);
		return node;
	}
	
	SceneNode* Scene::AddEmpty(const glm::mat4& in_transform, SceneNode* parent)
	{
		return m_sceneGraph.AddNode(in_transform, nullptr, parent);
	}


	// Material
	Material* Scene::CreateMaterial(const MATERIAL_TYPE in_type)
	{
		m_types.emplace(in_type);
		// Create the material
		Material* material = m_videoDriver->CreateMaterial(in_type);
		Bind bind(material, new Meshes());
		MaterialGroup* group = m_materials[in_type];
		group->emplace_back(bind);
		return material;
	}
	
	Material* Scene::CreateMaterialAndBind(const MATERIAL_TYPE in_type, Model* in_model)
	{
		auto it_s = std::find(m_models.begin(), m_models.end(), in_model);
		// if not in scene return
		if (it_s == m_models.end())
		{
			LT_WARN("Model not in scene. Add it first!");
			return nullptr;
		}

		/* TODO
		auto it = std::find(m_noMaterialModels.begin(), m_noMaterialModels.end(), in_model);
		// if is an already binded model
		if (it == m_noMaterialModels.end())
		{
			size_t material_index, model_index;
			bool found = false;

			// find the material is binded to
			for (MaterialGroup* matType : m_materials)
			{
				// For every material
				material_index = 0;
				for (auto bind : *matType)
				{
					model_index = 0;
					// compare to their meshes to see
					// if it is binded to this material
					for (auto model : *bind.second)
					{
						if (model == in_model)
						{
							found = true;
							break;
						}
						model_index++;
					}

					if (found) break;
					
					material_index++;
				}
				if (found)
				{
					//unbind it
					auto models = (*matType)[material_index].second;
					models->erase(models->begin() + model_index);
					break;
				}
			}

		}
		*/

		// Create the material
		m_types.emplace(in_type);
		Material* material = m_videoDriver->CreateMaterial(in_type);
		// Create model binded
		Meshes meshes = in_model->GetMeshes();
		Meshes* modelsBinded = new Meshes();
		modelsBinded->insert(modelsBinded->end(), meshes.begin(), meshes.end());

		// Bind models
		Bind bind(material, modelsBinded);
		auto group = m_materials[in_type];
		group->emplace_back(bind);
		
		return material;
	}
	
	void Scene::BindMaterialToModel(Material* in_material, Model* in_model)
	{
		// Get material group { O(1) }
		auto type = in_material->GetType();
		MaterialGroup* group = m_materials[type];
		size_t index = 0;

		bool found = false;
		// Find index of material { O(n) }
		for (auto bind : *group)
		{
			if (bind.first == in_material)
			{
				found = true;
				break;
			}
			index++;
		}
		// if found bind model
		if (found)
		{
			auto bind = (*group)[index];
			auto models = bind.second;
			auto meshes = in_model->GetMeshes();
			models->insert(models->end(), meshes.begin(), meshes.end());
		}
	}

	void Scene::BindMaterialsToModel(Model* in_model, std::initializer_list<Material*> in_materials)
	{
		auto meshes = in_model->GetMeshes();
		uint_32 i = 0;
		if (meshes.size() == in_materials.size())
		{
			for (Material* material : in_materials)
			{
				BindMaterialToMesh(material, meshes[i++]);
			}
		}
		// TODO
		else
		{
			uint_32 size_loop = (meshes.size() <= in_materials.size()) ? (uint_32)meshes.size() : (uint_32)in_materials.size();
		}
	}

	void Scene::BindMaterialToMesh(Material* in_material, Mesh* in_mesh)
	{
		MaterialGroup* groups = m_materials[in_material->GetType()];
		for (auto bind : *groups)
		{
			if (bind.first == in_material)
			{
				bind.second->emplace_back(in_mesh);
				break;
			}
		}
	}
	
	// Ray
	void Scene::GetPickRay(glm::vec2& in_mousePosition, glm::vec3& out_pickRay)
	{
		Window& window = *Application::Get().GetWindow();
		
		// Find screen coordinates normalized to -1,1
		glm::vec3& coord = out_pickRay;
		coord.x = (((2.0f * in_mousePosition.x) / window.GetWidth()) - 1);
		coord.y = -(((2.0f * in_mousePosition.y) / window.GetHeight()) - 1);
		coord.z = -1.0f;

		// 4d Homogeneous Clip Coordinates
		glm::vec4 ray_clips(coord, 1.0f);

		// 4d Eye (Camera) Coordinates
		glm::vec4 ray_eye = glm::inverse(m_activeCamera->GetProjectionMatrix()) * ray_clips;
		ray_eye = glm::vec4(ray_eye.xy, -1.0f, 0.0f);

		// 4d World Coordinates
		coord = (glm::inverse(m_activeViewNode->GetViewProps()->GetViewMatrix()) * ray_eye).xyz;
		coord = glm::normalize(coord);
	}

	bool Scene::RayCast(glm::vec3& in_from, glm::vec3& in_to, RayCastHitInfo& out_hitInfo)
	{
		auto direction = in_to - in_from;
		return m_aabbTree.RayCast(in_from, direction, direction.length(), out_hitInfo.hit_position, (void**)&out_hitInfo.node);
	}

}
