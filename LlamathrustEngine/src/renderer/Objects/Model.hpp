#pragma once
#include <core/Core.hpp>
#include <vector>
#include <glm/glm.hpp>
#include "Mesh.hpp"
#include "core/loaders/mud_file/mud_importer.hpp"

namespace Llamathrust
{
	namespace Rendering
	{
#pragma warning(push)
#pragma warning(disable: 4251)
		class LLAMATHRUST_API Model
		{
			std::vector<Mesh*> m_meshes;
			DataStructures::AABB* m_aabb;
			const glm::mat4* m_transform;
		public:
			Model(MUDLoader::Model* in_mudModel);
			Model(const std::initializer_list<Mesh*>& in_meshes);
			Model(const std::vector<Vertex>& in_vertices,
				  const std::vector<uint_32>& in_indices, const DRAW_MODE in_drawMode);

			void Draw() const;

			void LoadGPU();

			void SetTransform(const glm::mat4* in_transform);
			const glm::mat4* GetTransform() { return m_transform; }

			bool HasAABB() { return m_aabb; }
			void RecalculateAABB();
			void SetAABB(DataStructures::AABB* in_aabb) { m_aabb = in_aabb; }
			DataStructures::AABB* GetAABB() { return m_aabb; }
			const DataStructures::AABB* GetAABB() const { return m_aabb; }

			std::vector<Mesh*> GetMeshes() const { return m_meshes; }
		};
#pragma warning(pop)
	}
}