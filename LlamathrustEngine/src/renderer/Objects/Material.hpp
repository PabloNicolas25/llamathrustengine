#pragma once

#include "core/Core.hpp"
#include "core/graphics/ShaderProgram.hpp"
#include "core/graphics/GraphicsAPI.hpp"
#include "core/listeners/DrawCallListener.hpp"
#include "Texture.hpp"
#include "Cubemap.hpp"
#include "Camera.hpp"

namespace Llamathrust
{
	namespace Rendering
	{
		// Materials types from lighter to heaviest GPU usage
		enum LLAMATHRUST_API MATERIAL_TYPE : uint_32
		{
			UNLIT = 1,
			LIT = UNLIT << 1,
			TEXTURED = LIT << 1,
			REFLECTIVE = TEXTURED << 1,
			TEXTURED_LIT = LIT | TEXTURED,
			TEXTURED_LIT_REFLECTIVE = TEXTURED_LIT | REFLECTIVE,
			TEXTURED_UNLIT = UNLIT | TEXTURED,
			REFLECTIVE_UNLIT = UNLIT | REFLECTIVE,

			COUNT = 15
		};

		class LLAMATHRUST_API Material
		{
			Graphics::ShaderProgram** m_program;
			MATERIAL_TYPE m_type;

			DrawCallListener* m_drawCallListener = nullptr;

			// Properties
			Graphics::Color m_diffuseColor;
			Texture* m_diffuseTex;

			float m_reflection_factor = 0.0f;
			Cubemap* m_skyboxTex;
			ViewProperties* m_viewProps;

		public:
			Material(Graphics::ShaderProgram** in_shaderProgram, const MATERIAL_TYPE in_type);
			~Material() = default;

			MATERIAL_TYPE GetType() { return m_type; }

			void LoadGPU()
			{
				if (m_type & MATERIAL_TYPE::TEXTURED)
				{
					// bind the program first
					Use();
					(*m_program)->SetInt("diffuse", 0);
					m_diffuseTex->LoadGPU();
				}
				if (m_type & MATERIAL_TYPE::REFLECTIVE)
				{
					Use();
					(*m_program)->SetInt("skybox", 1);
					m_skyboxTex->LoadGPU();
				}
			}

			// Prepare Material to use
			void Use();

			// Send data to the GPU
			void Draw();

			//Send PVM mat to GPU
			void SetPVM(const glm::mat4& in_mat) { (*m_program)->SetMat4("pvm_transform", in_mat); }

			// Set diffuse color
			void SetDiffuse(Graphics::Color& in_color) { m_diffuseColor = in_color; }
			void SetDiffuse(Texture* in_tex) { m_diffuseTex = in_tex; }

			// Set Reflective Cubemap
			void SetReflection(Cubemap* in_cubemap, ViewProperties* in_viewProps);
			// Set Reflection amount
			void SetReflectionFactor(const float in_reflectionFactor = 0.0f) { m_reflection_factor = in_reflectionFactor; }
		};
	}
}