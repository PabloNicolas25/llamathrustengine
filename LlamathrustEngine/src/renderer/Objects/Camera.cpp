#include "ltpch.hpp"
#include "Camera.hpp"
#include <glm/gtc/matrix_transform.hpp>

namespace Llamathrust::Rendering
{
	Camera::Camera(float in_fov, float in_maxDist, float in_minDist, float in_aspect)
		: m_fov(in_fov), m_maxDistance(in_maxDist), m_minDistance(in_minDist), m_aspect(in_aspect)
	{
		m_projectionMatrix = glm::perspective(glm::radians(in_fov), in_aspect, in_minDist, in_maxDist);
	}
	Camera::Camera(float in_width, float in_height)
	{
		ortho = true;
		RecalculateProjectionMatrix(in_width, in_height);
	}
	glm::mat4& Camera::GetProjectionMatrix()
	{
		return m_projectionMatrix;
	}

	glm::mat4 & Camera::RecalculateProjectionMatrix(float in_fov, float in_maxDist, float in_minDist, float in_aspect)
	{
		m_fov = in_fov;
		m_aspect = in_aspect;
		m_maxDistance = in_maxDist;
		m_minDistance = in_minDist;

		m_projectionMatrix = glm::perspective(glm::radians(in_fov), in_aspect, in_minDist, in_maxDist);
		
		return m_projectionMatrix;
	}

	glm::mat4& Camera::RecalculateProjectionMatrix(float in_width, float in_height)
	{
		float right = in_width / in_height;
		float top = 1;
		m_projectionMatrixOrtho = glm::ortho(-right, right, -top, top, -1.0f, 2.0f);
		return m_projectionMatrixOrtho;
	}

	float Camera::GetFieldOfView()
	{
		return m_fov;
	}

	float Camera::GetMaxDistance()
	{
		return m_maxDistance;
	}

	float Camera::GetMinDistance()
	{
		return m_minDistance;
	}

	float Camera::GetAspectRatio()
	{
		return m_aspect;
	}
	
	ViewProperties::ViewProperties(glm::vec3 & in_position, float in_pitch, float in_yaw, glm::vec3 & in_worldUp)
		: m_position(in_position), m_worldUp(in_worldUp), m_yaw(in_yaw), m_pitch(in_pitch)
	{
		updateCameraVectors();
	}

	glm::mat4 ViewProperties::GetViewMatrix() const
	{
		return m_viewMatrix;
	}

	void ViewProperties::SetRotation(float in_pitch, float in_yaw)
	{
		m_pitch = in_pitch;
		m_yaw = in_yaw;
		updateCameraVectors();
		m_viewMatrix = glm::lookAt(m_position, m_position + m_front, m_up);
	}

	void ViewProperties::SetPosition(glm::vec3& in_position)
	{
		m_position = in_position;
		m_viewMatrix = glm::lookAt(m_position, m_position + m_front, m_up);
	}

	void ViewProperties::updateCameraVectors()
	{
		// Calculate the new Front vector
		glm::vec3 front;
		front.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
		front.y = sin(glm::radians(m_pitch));
		front.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
		m_front = glm::normalize(front);
		// Also re-calculate the Right and Up vector
		m_right = glm::normalize(glm::cross(m_front, m_worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		m_up = glm::normalize(glm::cross(m_right, m_front));
	}
}
