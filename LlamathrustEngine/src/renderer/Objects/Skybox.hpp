#pragma once

#include "core/Core.hpp"
#include "Model.hpp"
#include "Cubemap.hpp"
#include "core/graphics/ShaderProgram.hpp"

namespace Llamathrust
{
	namespace Rendering
	{
		class LLAMATHRUST_API Skybox
		{
			Llamathrust::Rendering::Cubemap* m_cubemap;
			Llamathrust::Rendering::Model* m_model = nullptr;
			Llamathrust::Graphics::ShaderProgram* m_shader = nullptr;
		public:
			Skybox(Llamathrust::Rendering::Cubemap* in_cube);
			~Skybox();

			void Draw(const glm::mat4& in_projection, const glm::mat4& in_view);

			Llamathrust::Rendering::Cubemap* GetCubemap();
		};
	}
}