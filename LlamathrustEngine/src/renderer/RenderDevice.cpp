#include "ltpch.hpp"
#include "RenderDevice.hpp"
#include "core/data_structures/AABB.hpp"

namespace Llamathrust::Rendering
{
	RenderDevice* RenderDevice::s_instance = nullptr;

	RenderDevice::RenderDevice()
	{
		m_shaders.reserve(MATERIAL_TYPE::COUNT);
		for (size_t i = 0; i < m_shaders.capacity(); i++)
		{
			m_shaders.emplace_back(nullptr);
		}
		s_instance = this;
	}

	void RenderDevice::Draw(const glm::mat4& PVmat, std::vector<std::vector<std::pair<Material*, std::vector<Mesh*>*>>*>& in_materialsAndModels, const std::vector<Mesh*>& in_models)
	{
		for (auto materialType : in_materialsAndModels)
		{
			// if not material of this type then continue to next type
			if (!materialType->size())
				continue;

			// Use shader program ONCE per TYPE
			(*materialType)[0].first->Use();

			for (auto bind : *materialType)
			{
				Material* material = bind.first;

				material->Draw();
				
				for (Mesh* mesh : *bind.second)
				{
					auto PVM = PVmat;
					material->SetPVM(PVM);
					//material->SetModelMat()
					mesh->Bind();
					mesh->Draw();
				}
			}

		}

		// AABB draw doesnt work
		/*
		if (m_debugMat)
		{
			// Debug draw
			m_debugMat->Use();
			m_debugMat->SetPVM(PVmat);
			for (auto const& mesh : m_debugMeshes)
			{
				m_debugMat->Draw();
				mesh->Bind();
				mesh->Draw();
			}
		}
		*/

		// Clear the debug meshes since they are recreated each draw call
		m_debugMeshes.clear();
	}

	Material* RenderDevice::CreateMaterial(const MATERIAL_TYPE in_type)
	{
		if (!m_shaders[in_type])
		{
			Graphics::ShaderProgram* shader = nullptr;

			m_shaders.insert(m_shaders.begin() + in_type, shader);
			m_shaders.erase(m_shaders.begin() + in_type + 1);
		}

		return new Material(&m_shaders[in_type], in_type);
	}

	void RenderDevice::GenerateShaders(const std::set<MATERIAL_TYPE>& in_types)
	{
		for (auto type : in_types)
		{
			switch (type)
			{
			case MATERIAL_TYPE::UNLIT:
				m_shaders[type] = CreateShaderProgram("../res/shaders/unlit", "../res/shaders/unlit"); break;
			case MATERIAL_TYPE::TEXTURED_UNLIT:
			case MATERIAL_TYPE::TEXTURED:
				m_shaders[type] = CreateShaderProgram("../res/shaders/unlit", "../res/shaders/unlit_tex"); break;
			case MATERIAL_TYPE::LIT:
				m_shaders[type] = CreateShaderProgram("../res/shaders/lit", "../res/shaders/lit"); break;
			case MATERIAL_TYPE::TEXTURED_LIT:
				m_shaders[type] = CreateShaderProgram("../res/shaders/lit", "../res/shaders/lit_tex"); break;
			case MATERIAL_TYPE::REFLECTIVE:
			case MATERIAL_TYPE::REFLECTIVE_UNLIT:
				m_shaders[type] = CreateShaderProgram("../res/shaders/lit_reflective", "../res/shaders/lit_reflective"); break;
			}
			m_shaders[type]->Create();
		}
	}

	void RenderDevice::DrawAABB(const DataStructures::AABB& in_aabb)
	{
		std::vector<Vertex> vertices; vertices.reserve(8); vertices.resize(8);
		std::vector<uint_32> indices; indices.reserve(24); indices.resize(24);
		auto max = in_aabb.GetMaxExtent();
		auto min = in_aabb.GetMinExtent();

		//Vertices 
		vertices[0].position = { max };
		vertices[1].position = { min.x, max.y, max.z };
		vertices[2].position = { min.x, min.y, max.z };
		vertices[3].position = { max.x, min.y, max.z };

		vertices[4].position = { max.x, max.y, min.z };
		vertices[5].position = { min.x, max.y, min.z };
		vertices[6].position = { min };
		vertices[7].position = { max.x, min.y, min.z };


		// Up face
		indices = {
			0, 1,
			1, 2,
			2, 3,
			3, 0,
			// Down face
			4, 5,
			5, 6,
			6, 7,
			7, 4,
			// Side faces
			0, 4,
			1, 5,
			2, 6,
			3, 7 };


		
		Mesh* aabb_mesh = CreateMesh(vertices, indices, nullptr, DRAW_MODE::LINES);
		m_debugMeshes.emplace_back(aabb_mesh);

		if (!m_debugMat)
		{
			m_debugMat = CreateMaterial(MATERIAL_TYPE::UNLIT);
			m_debugMat->SetDiffuse(Graphics::Color(0.0f, 1.0f, 0.0f, 1.0f));
		}
	}

}
